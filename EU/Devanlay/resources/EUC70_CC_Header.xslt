<?xml version="1.0" encoding="utf-8"?>
<xsl:transform version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">  

	<xsl:output method="xml" indent="yes"/>

	<xsl:template match="/">
	
	<Orders>		
			
		<!-- Liste de toutes les factures non publiees -->
		<xsl:for-each select="/tXML/Message/Order/Invoices/InvoiceDetail[InvoiceType='Shipment' and Published='false']"> 
			<xsl:variable name="Invoice" select="."/> 	
			<xsl:variable name="Order" select="../.."/>		
			
			<!-- DEBUG -->		
			<!--
			<debugInvoice>
				<xsl:value-of select="$Invoice/InvoiceNbr"/>
			</debugInvoice>
			<debugOrder>
				<xsl:value-of select="$Order/OrderNumber"/>
			</debugOrder>
			-->
			
			<Order>		
			
				<alias>
					<xsl:value-of select="concat($Order/OrderNumber,'-',$Invoice/InvoiceNbr)"/>			
				</alias>
				<tracking_number>
					<xsl:value-of select="$Order/ShipmentDetails/ShipmentDetail/Cartons/Carton[ShipmentNbr=$Invoice/ShipmentNbr]/TrackingNbr"/> 						
				</tracking_number>			
				<civility>
					<xsl:value-of select="'1'"/>	
				</civility>
				<name>
					<xsl:choose>
						<xsl:when test="$Order/OrderType='FID'">
							<xsl:value-of select="$Order/CustomerInfo/CustomerLastName"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$Order/PaymentDetails/PaymentDetail[1]/BillToDetail/BillToLastName"/>	
						</xsl:otherwise>	
					</xsl:choose>					
				</name>
				<first_name>
					<xsl:choose>
						<xsl:when test="$Order/OrderType='FID'">
							<xsl:value-of select="$Order/CustomerInfo/CustomerFirstName"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$Order/PaymentDetails/PaymentDetail[1]/BillToDetail/BillToFirstName"/>		
						</xsl:otherwise>	
					</xsl:choose>					
				</first_name>
				<email>
					<xsl:choose>
						<xsl:when test="$Order/OrderType='FID'">
							<xsl:value-of select="$Order/CustomerInfo/CustomerEmail"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$Order/PaymentDetails/PaymentDetail[1]/BillToDetail/BillToEmail"/>	
						</xsl:otherwise>	
					</xsl:choose>					
				</email>
				<cell_phone>
					<xsl:choose>
						<xsl:when test="$Order/OrderType='FID'">
							<xsl:value-of select="$Order/CustomerInfo/CustomerPhone"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$Order/PaymentDetails/PaymentDetail[1]/BillToDetail/BillToPhone"/>	
						</xsl:otherwise>	
					</xsl:choose>	
				</cell_phone>
				<street1>
					<xsl:choose>
						<xsl:when test="$Order/OrderType='FID'">
							<xsl:value-of select="''"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$Order/PaymentDetails/PaymentDetail[1]/BillToDetail/BillToAddressLine1"/>		
						</xsl:otherwise>	
					</xsl:choose>	
				</street1>
				<street2>
					<xsl:choose>
						<xsl:when test="$Order/OrderType='FID'">
							<xsl:value-of select="''"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$Order/PaymentDetails/PaymentDetail[1]/BillToDetail/BillToAddressLine2"/>	
						</xsl:otherwise>	
					</xsl:choose>
				</street2>
				<zipcode>
					<xsl:choose>
						<xsl:when test="$Order/OrderType='FID'">
							<xsl:value-of select="''"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$Order/PaymentDetails/PaymentDetail[1]/BillToDetail/BillToPostalCode"/>	
						</xsl:otherwise>	
					</xsl:choose>	
				</zipcode>
				<city>
					<xsl:choose>
						<xsl:when test="$Order/OrderType='FID'">
							<xsl:value-of select="''"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$Order/PaymentDetails/PaymentDetail[1]/BillToDetail/BillToCity"/>	
						</xsl:otherwise>	
					</xsl:choose>
				</city>
				<country>
					<xsl:choose>
						<xsl:when test="$Order/OrderType='FID'">
							<xsl:value-of select="''"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$Order/PaymentDetails/PaymentDetail[1]/BillToDetail/BillToCountry"/>		
						</xsl:otherwise>	
					</xsl:choose>	
				</country>
				<store>
					<xsl:value-of select="$Order/OrderLines/OrderLine[1]/ShippingInfo/ShippingAddress/ShipToFax"/>	
				</store>
				<products_price>
					<xsl:value-of select="$Invoice/InvoiceAmount"/>	
				</products_price>
				<currency>
					<xsl:value-of select="$Order/ReferenceFields/ReferenceField5"/>	
				</currency>			
				<xsl:variable name="DateCreatedAt">
					<xsl:call-template name="convertDOMDateFormat1">
						<xsl:with-param name="DOMDateTime" select="$Invoice/InvoiceCreationDTTM"/>
					</xsl:call-template>
				</xsl:variable>
				<created_at>
					<xsl:value-of select="$DateCreatedAt"/>
				</created_at>
				<status>
					<xsl:value-of select="'1'"/>	
				</status>
				<comments>
					<xsl:value-of select="''"/>	
				</comments>
			
			</Order>
			
		</xsl:for-each>	
        
	</Orders>		
	
	</xsl:template>
	
	<xsl:template name="convertDOMDateFormat1">
				<xsl:param name="DOMDateTime"/>
				<xsl:if test="string-length($DOMDateTime) &gt; 15">
						   <xsl:variable name="varDate">
									   <xsl:value-of select="substring-before($DOMDateTime, ' ')"/>
						   </xsl:variable>
						   <xsl:variable name="varHour">
									   <xsl:value-of select="substring-after($DOMDateTime, ' ')"/>
						   </xsl:variable>
						   <xsl:variable name="varMonth">
									   <xsl:if test="string-length(substring-before($varDate, '/')) &lt; 2">
												   <xsl:value-of select="concat('0',substring-before($varDate, '/'))"/>
									   </xsl:if>
									   <xsl:if test="string-length(substring-before($varDate, '/')) = 2">
												   <xsl:value-of select="substring-before($varDate, '/')"/>
									   </xsl:if>
						   </xsl:variable>
						   <xsl:variable name="varDayYear">
									   <xsl:value-of select="substring-after($varDate,'/')"/>
						   </xsl:variable>
						   <xsl:variable name="varDay">
									   <xsl:if test="string-length(substring-before($varDayYear, '/')) &lt; 2">
												   <xsl:value-of select="concat('0',substring-before($varDayYear, '/') )"/>
									   </xsl:if>
									   <xsl:if test="string-length(substring-before($varDayYear, '/')) = 2">
												   <xsl:value-of select="substring-before($varDayYear, '/')"/>
									   </xsl:if>
						   </xsl:variable>
						   <xsl:variable name="varYear">
									   <xsl:choose>
												   <xsl:when test="string-length(substring-after($varDayYear, '/')) = 2">
															   <xsl:value-of select="concat('20',substring-after($varDayYear, '/'))"/>
												   </xsl:when>
												   <xsl:when test="string-length(substring-after($varDayYear, '/')) = 4">
															   <xsl:value-of select="substring-after($varDayYear, '/')"/>
												   </xsl:when>
									   </xsl:choose>
						   </xsl:variable>
						   <xsl:value-of select="concat($varDay,'/',$varMonth,'/',$varYear,' ',$varHour)"/>
				</xsl:if>
	</xsl:template>
	
</xsl:transform>
          