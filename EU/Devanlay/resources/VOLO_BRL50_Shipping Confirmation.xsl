<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="@* | node()">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ActualShippedDTTM">
		<DeliveryEnd>
			<xsl:call-template name="ConvertToDateTimeDOM">
				<xsl:with-param name="datetime" select="."/>
			</xsl:call-template>
		</DeliveryEnd>
	</xsl:template>
	<xsl:template match="DeliveryStart">
		<DeliveryStart>
			<!--<xsl:value-of select="format-date(.,'[M01]/[D01]/[Y0001]')"/>-->
			<xsl:call-template name="ConvertToDateTimeDOM">
				<xsl:with-param name="datetime" select="."/>
			</xsl:call-template>
		</DeliveryStart>
	</xsl:template>
		<xsl:template match="LPNStatusDate">
		<DeliveryStart>
			<xsl:call-template name="ConvertToDateTimeDOM">
				<xsl:with-param name="datetime" select="."/>
			</xsl:call-template>
		</DeliveryStart>
	</xsl:template>
	<xsl:template match="ASNID">
		<ASNID>
			<xsl:value-of select="concat(.,'_',/tXML/Header/Company_ID)" />
		</ASNID>
	</xsl:template>
	<xsl:template name="ConvertToDateTimeDOM">
		<xsl:param name="datetime"/>
		<xsl:variable name="date" select="tokenize(tokenize($datetime,'\s')[1],'-')"/>
		<xsl:variable name="year" select="$date[1]"/>
		<xsl:variable name="month" select="$date[2]"/>
		<xsl:variable name="day" select="$date[3]"/>
		<xsl:value-of select="concat($month,'/',$day,'/',$year,' ',tokenize($datetime,'\s')[2])"/>
	</xsl:template>
</xsl:stylesheet>
