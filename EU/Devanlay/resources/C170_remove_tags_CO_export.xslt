<!-- Rajouter Action Type avant import -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output omit-xml-declaration="yes" indent="yes"/>
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="Sequence_Number|Batch_ID|Reference_ID|User_ID|Password|Version|OrderCreatedDate|OrderSubtotal|OrderTotal|OrderStatus|OrderEvent|LastUpdatedDTTM|OrderLineCreatedDate|OrderLineCaptureDate|OrderLineStatus|LineTotal|CancelledQty|TotalAllocatedQty|TotalCancelledQty|DesignatedCarrierCode|DesignatedModeCode|DesignatedServiceLevelCode|AllocationDetails|SplitLines|DistributionOrders|ShipmentDetails|Invoices|OrderedTotals"/>
</xsl:stylesheet>