<?xml version="1.0" encoding="utf-8"?>
<xsl:transform version="2.0"
               xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
               xmlns:src="http://www.demandware.com/xml/impex/catalog/2006-10-31"
               xmlns:msxsl="urn:schemas-microsoft-com:xslt"
               xmlns:local="urn:local" extension-element-prefixes="msxsl"
               exclude-result-prefixes="src local">  

	<xsl:output method="xml" indent="yes" cdata-section-elements="ItemImageFilename"/>

	<xsl:template match="/">
		<xsl:param name="current-date"/>
		<tXML>
			<Header>
				<Source>
					<xsl:value-of select="'Orliweb'"/>
				</Source>
				<Action_Type>
					<xsl:value-of select="'update'"/>
				</Action_Type>
				<Reference_ID>
					<xsl:value-of select="substring-before(substring-after(substring-after(substring-after(substring-after(tokenize(base-uri(.), '/')[last()],'_'),'_'),'_'),'_'),'_')"/>          
				</Reference_ID>
				<Message_Type>
					<xsl:value-of select="'Item'"/>
				</Message_Type>
				<Company_ID>
					<xsl:value-of select="'1'"/>
				</Company_ID>
				<Msg_Locale>
					<xsl:value-of select="'English (United States)'"/>
				</Msg_Locale>
				<Msg_Time_Zone>
					<xsl:value-of select="'Europe/Paris'"/>
				</Msg_Time_Zone>
				<Internal_Reference_ID>
					<xsl:value-of select="concat('IM_',format-dateTime(current-dateTime(),'[Y0001][M01][D01][H01][m01]'))"/>          
				</Internal_Reference_ID>
			</Header>
			<Message>

				<!-- Liste de tous les styles -->
				<xsl:for-each select="//src:product[src:min-order-quantity]">
					<xsl:variable name="style" select="."/>
					<!-- Liste de tous les sku -->
					<xsl:for-each select="src:variations/src:variants/src:variant">
						<xsl:variable name="sku-id" select="@product-id"/>
						<xsl:variable name="sku" select="//src:product[(src:ean or src:upc) and @product-id = $sku-id]"/>

						<Item>  

							<!-- DEBUG -->
							<!--
              <xsl:attribute name="style">
                <xsl:value-of select="$style/@product-id"/>
              </xsl:attribute>
              <xsl:attribute name="sku-id">
                <xsl:value-of select="$sku/@product-id"/>
              </xsl:attribute>
              <xsl:attribute name="sku_id">
                <xsl:value-of select="$sku-id"/>
              </xsl:attribute>
              <xsl:attribute name="sku">
                <xsl:value-of select="$sku/src:online-flag"/>
              </xsl:attribute>
              -->

							<ItemName>
								<xsl:value-of select="$sku/src:ean"/>                                   
							</ItemName>
							<BusinessUnit>
								<xsl:value-of select="'1'"/>
							</BusinessUnit>

							<Description>
								<xsl:variable name="DescriptionValue">
									<xsl:choose>               
										<xsl:when test="string-length($style/src:display-name[@xml:lang = 'en']) != 0">
											<xsl:value-of select="substring(concat(concat($style/src:display-name[@xml:lang =  'en'] , ' - ') , $style/src:variations/src:attributes/src:variation-attribute[@attribute-id = 'color' and @variation-attribute-id = 'color']/src:variation-attribute-values/src:variation-attribute-value[@value = $sku/src:custom-attributes/src:custom-attribute[@attribute-id = 'colorCode']]/src:display-value[@xml:lang = 'en']), 1, 100)"/>
										</xsl:when>
										<xsl:otherwise>                   
											<xsl:value-of select="$style/@product-id"/>                   
										</xsl:otherwise>
									</xsl:choose>     
								</xsl:variable>

								<xsl:value-of select="replace($DescriptionValue, '&amp;', '')"/>  
							</Description>

							<LongDescription>
								<xsl:value-of select="$style/src:long-description[@xml:lang = 'en']"/>
							</LongDescription>
							<ProtectionLevel>
								<xsl:value-of select="substring($style/src:custom-attributes/src:custom-attribute[@attribute-id = 'networkID'], 1, 1)"/>
							</ProtectionLevel>
							<ProductClass>
								<xsl:value-of select="substring($style/src:custom-attributes/src:custom-attribute[@attribute-id = 'categoryID'], 1, 3)"/>
							</ProductClass>
							<CommodityCode>
								<!--<xsl:value-of select="$style/src:custom-attributes/src:custom-attribute[@attribute-id = 'customCode']"/>-->
							</CommodityCode>
							<BaseStorageUOM>
								<xsl:value-of select="'Units'"/>
							</BaseStorageUOM>
							<ItemSeason>
								<xsl:variable name="season" select="$sku/src:custom-attributes/src:custom-attribute[@attribute-id = 'season']"/>                
								<xsl:value-of select="substring($season, string-length($season), 1)"/>
							</ItemSeason>
							<ItemSeasonYear>
								<xsl:value-of select="substring($sku/src:custom-attributes/src:custom-attribute[@attribute-id = 'season'], 0, 3)"/>
							</ItemSeasonYear>
							<ItemStyle>
								<xsl:choose>
									<xsl:when test="contains($style/@product-id,'-')">
										<xsl:value-of select="substring($style/@product-id, 0, string-length($style/@product-id) - 2)"/>
									</xsl:when>				
									<xsl:otherwise>                   
										<xsl:value-of select="substring($style/@product-id, 0, 9)"/>
									</xsl:otherwise>
								</xsl:choose>
							</ItemStyle>
							<ItemStyleSfx>
								<xsl:choose>
									<xsl:when test="contains($style/@product-id,'-')">
										<xsl:value-of select="substring($style/@product-id, string-length($style/@product-id) - 2, 3)"/>
									</xsl:when>
									<xsl:otherwise>                   
										<xsl:value-of select="substring($style/@product-id, 9)"/>                   
									</xsl:otherwise>
								</xsl:choose>
							</ItemStyleSfx>
							<ItemColor>
								<xsl:value-of select="$sku/src:custom-attributes/src:custom-attribute[@attribute-id = 'colorCode']"/>
							</ItemColor>
							<ItemColorSfx>
								<xsl:value-of select="''"/>
							</ItemColorSfx>
							<ItemQuality>
								<xsl:value-of select="''"/>
							</ItemQuality>              
							<ItemSizeDesc>
								<xsl:value-of select="substring($style/src:variations/src:attributes/src:variation-attribute[@attribute-id = 'size' and @variation-attribute-id = 'size']/src:variation-attribute-values/src:variation-attribute-value[@value = $sku/src:custom-attributes/src:custom-attribute[@attribute-id = 'size']] /src:display-value[@xml:lang = 'en'],1,4)"/>
							</ItemSizeDesc>
							<ItemUpcGtin>               
								<xsl:value-of select="$sku/src:ean"/>
							</ItemUpcGtin>
							<QtyUOM>
								<xsl:value-of select="'Units'"/>
							</QtyUOM>
							<SoldOnline>
								<xsl:value-of select="'1'"/>
							</SoldOnline>
							<AvailableForShipToStore>
								<xsl:value-of select="'true'"/>
							</AvailableForShipToStore>
							<ItemImageFilename>
								<xsl:value-of >
									<xsl:value-of select="'style='"/>
									<xsl:choose>
										<xsl:when test="contains($style/@product-id,'-')">
											<xsl:value-of select="substring($style/@product-id, 0, string-length($style/@product-id) - 2)"/>
										</xsl:when>				
										<xsl:otherwise>                   
											<xsl:value-of select="substring($style/@product-id, 0, 9)"/>
										</xsl:otherwise>
									</xsl:choose>
									<xsl:value-of>
										<xsl:text disable-output-escaping="yes">&amp;</xsl:text>
									</xsl:value-of>
									<xsl:value-of select="concat('color=',$sku/src:custom-attributes/src:custom-attribute[@attribute-id = 'colorCode'])"/>
									<xsl:value-of>
										<xsl:text disable-output-escaping="yes">&amp;platform=EU1</xsl:text>
									</xsl:value-of>
								</xsl:value-of>
							</ItemImageFilename>
							<ItemURLPath>
								<xsl:value-of select="concat($style/@product-id,'.html?dwvar_',$style/@product-id,'_color=',$sku/src:custom-attributes/src:custom-attribute[@attribute-id = 'colorCode'])"/>
							</ItemURLPath>
							<ReferenceField1>
								<xsl:value-of select="$sku/src:tax-class-id"/>
							</ReferenceField1>
							<ColorDesc>
								<xsl:value-of select="$sku/src:custom-attributes/src:custom-attribute[@attribute-id = 'color']"/>
							</ColorDesc>
							<ItemBarCode>
								<xsl:value-of select="$sku/src:ean"/>
							</ItemBarCode>
							<ProductType>
								<xsl:value-of select="'0'"/>
							</ProductType>
							<IsReturnable>
								<xsl:value-of select="'true'"/>
							</IsReturnable>
							<IsExchangeable>
								<xsl:value-of select="'true'"/>
							</IsExchangeable>
							<MarkForDeletion>
								<xsl:value-of select="''"/>
							</MarkForDeletion>
							<ItemSizeList>
								<ItemSize>
									<Uom>
										<xsl:value-of select="'Units'"/>                    
									</Uom>
									<UomValue>
										<xsl:value-of select="'1'"/>
									</UomValue>
								</ItemSize>
							</ItemSizeList>
							<ItemPackageList>
								<ItemPackage>
									<PkgUOM>
										<xsl:value-of select="'Units'"/>
									</PkgUOM>
									<Quantity>
										<xsl:value-of select="'1'"/>
									</Quantity>
								</ItemPackage>
							</ItemPackageList>

						</Item>            

					</xsl:for-each>
				</xsl:for-each>

			</Message>
		</tXML>
	</xsl:template>
</xsl:transform>