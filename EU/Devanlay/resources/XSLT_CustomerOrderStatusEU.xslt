<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xsl:output method="text" indent="yes"/>
	<xsl:param name="separateur" select="'/'"/>
	<!-- This function transforms the date format from '9/7/07 13:13'  OR 9/9/2009 09:00  7/10/2008 10:27 to YYYYMMDDHH24:MI -->
	<!-- RDN 25/04/2014 Maintenant la fonction retourne une date au format XML : YYYY-MM-DDTHH:MI:SS / 2008-10-07T10:27:00 -->
	<!-- CBS RDN 08/07/2014 Renommage en Old de la fonction date de Manhattan qui etait erronee sur le découpage (FSE)
                                        Remplacement parune nouvelle fonction du meme nom avec du tokenize
                                        ATTENTION on force 20 devant annee pour avoir annee sur 4 digits !!!!  -->
	<!-- CBS RDN 08/07/2014 Si pas de balise ALLOCATION_DETAIL alors il faut interpreter comme 0 shipped et 0 allocated (FSE) -->
	<!-- CBS RDN 28/10/2014 Gestion de la SplitLine potentielle (FSE) -->
	<!-- RDN 28/04/2015 Alimentation de la variable "vbatch_id" ==> Modification de Current_Date vers le noeud Sequence_Number (FSE tâche 32390) -->	
	<xsl:template name="convertDOMDateFormat1Old">
		<xsl:param name="DOMDateTime"/>
		<xsl:if test="string-length($DOMDateTime) &gt; 15">
			<xsl:variable name="varDate">
				<xsl:value-of select="substring-before($DOMDateTime, ' ')"/>
			</xsl:variable>
			<xsl:variable name="varMonth">
				<xsl:if test="string-length(substring-before($varDate, '/')) &lt; 2">
					<xsl:value-of select="concat('0',substring-before($varDate, '/'))"/>
				</xsl:if>
				<xsl:if test="string-length(substring-before($varDate, '/')) = 2">
					<xsl:value-of select="substring-before($varDate, '/')"/>
				</xsl:if>
			</xsl:variable>
			<xsl:variable name="varDayYear">
				<xsl:value-of select="substring-after($varDate,'/')"/>
			</xsl:variable>
			<xsl:variable name="varDay">
				<xsl:if test="string-length(substring-before($varDayYear, '/')) &lt; 2">
					<xsl:value-of select="concat('0',substring-before($varDayYear, '/') )"/>
				</xsl:if>
				<xsl:if test="string-length(substring-before($varDayYear, '/')) = 2">
					<xsl:value-of select="substring-before($varDayYear, '/')"/>
				</xsl:if>
			</xsl:variable>
			<xsl:variable name="varYear">
				<xsl:choose>
					<xsl:when test="string-length(substring-after($varDayYear, '/')) = 2">
						<xsl:value-of select="concat('20',substring-after($varDayYear, '/'))"/>
					</xsl:when>
					<xsl:when test="string-length(substring-after($varDayYear, '/')) = 4">
						<xsl:value-of select="substring-after($varDayYear, '/')"/>
					</xsl:when>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="vartime">
				<xsl:value-of select="substring($DOMDateTime,string-length($DOMDateTime)-8,5)"/>
			</xsl:variable>
			<xsl:value-of select="concat($varYear,'-',$varMonth,'-',$varDay,'T',$vartime,':00')"/>
		</xsl:if>
	</xsl:template>
	<xsl:template name="convertDOMDateFormat1">
		<xsl:param name="DOMDateTime"/>
		<xsl:variable name="splitdatecomplete" select="tokenize($DOMDateTime, ' ')"/>
		<xsl:variable name="splitdate" select="tokenize($splitdatecomplete[1], $separateur)"/>
		<xsl:variable name="varMonth">
			<!--        <xsl:value-of select="normalize-space($splitdate[1])"/>     -->
			<xsl:if test="string-length(normalize-space($splitdate[1])) &lt; 2">
				<xsl:value-of select="concat('0',normalize-space($splitdate[1]))"/>
			</xsl:if>
			<xsl:if test="string-length(normalize-space($splitdate[1])) = 2">
				<xsl:value-of select="normalize-space($splitdate[1])"/>
			</xsl:if>
		</xsl:variable>
		<xsl:variable name="varDay">
			<!--        <xsl:value-of select="normalize-space($splitdate[2])"/>     -->
			<xsl:if test="string-length(normalize-space($splitdate[2])) &lt; 2">
				<xsl:value-of select="concat('0',normalize-space($splitdate[2]))"/>
			</xsl:if>
			<xsl:if test="string-length(normalize-space($splitdate[2])) = 2">
				<xsl:value-of select="normalize-space($splitdate[2])"/>
			</xsl:if>
		</xsl:variable>
		<xsl:variable name="varYear">
			<xsl:value-of select="normalize-space($splitdate[3])"/>
		</xsl:variable>
		<xsl:variable name="vartime">
			<xsl:value-of select="normalize-space($splitdatecomplete[2])"/>
		</xsl:variable>
		<xsl:value-of select="concat('20', $varYear,'-',$varMonth,'-',$varDay,'T',$vartime,':00')"/>
	</xsl:template>
	<xsl:template match="/">
		<!--		<xsl:variable name="vbatch_id" select="tXML/Header/Batch_ID"/>-->
		<xsl:variable name="vBatchId">
			<xsl:call-template name="convertDOMDateFormat1">
				<xsl:with-param name="DOMDateTime" select="tXML/Message/Order/LastUpdatedDTTM"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="vdateBatchId">
			<xsl:value-of select="format-dateTime($vBatchId,'[Y0001][M01][D01][H01][m01][s01]')"/>
		</xsl:variable>
		<!--		<xsl:variable name="vbatch_id" select="$vdateBatchId"/>-->
		<!--		<xsl:variable name="vbatch_id" select="format-dateTime(current-dateTime(),'[Y0001][M01][D01][H01][m01][s01][f01]')"/>-->
		<xsl:variable name="vbatch_id" select="tXML/Header/Sequence_Number"/>
		<xsl:for-each select="tXML/Message/Order">
			<xsl:variable name="vordernumber" select="OrderNumber"/>
			<xsl:for-each select="OrderLines/OrderLine">
				<xsl:variable name="vorderlinenumber" select="LineNumber"/>
				<xsl:variable name="vdateDDL">
					<xsl:call-template name="convertDOMDateFormat1">
						<xsl:with-param name="DOMDateTime" select="LastUpdatedDTTM"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="vlastupdatedbttm">
					<!--				<xsl:variable name="vadjustedDateTime" select="adjust-dateTime-to-timezone($vdateDDL,xs:dayTimeDuration('CEST'))"/>-->
					<xsl:value-of select="format-dateTime($vdateDDL,'[Y0001][M01][D01][H01][m01][s01]')"/>
				</xsl:variable>
				<xsl:choose>
					<xsl:when test="exists(AllocationDetails/AllocationDetail)">
						<xsl:for-each select="AllocationDetails/AllocationDetail">
							<xsl:variable name="vitemid" select="ItemID"/>
							<xsl:variable name="vallocatedquantity">
								<xsl:variable name="vconditionallocqty" select="AllocationStatus != 'Shipped' and AllocationStatus != 'Partially Shipped'"/>
								<xsl:if test="$vconditionallocqty">
									<xsl:value-of select="AllocatedQuantity"/>
								</xsl:if>
								<xsl:if test="not($vconditionallocqty)">
									<xsl:value-of select="0"/>
								</xsl:if>
							</xsl:variable>
							<xsl:variable name="vshippedquantity">
								<xsl:variable name="vconditionshipqty" select="AllocationStatus = 'Shipped'"/>
								<xsl:if test="$vconditionshipqty">
									<xsl:value-of select="AllocatedQuantity"/>
								</xsl:if>
								<xsl:if test="not($vconditionshipqty)">
									<xsl:value-of select="0"/>
								</xsl:if>
							</xsl:variable>
							<xsl:variable name="vinventorysegmentname" select="InventorySegmentName"/>
							<!--Creation de la ligne csv avec separateur-->
							<xsl:value-of select="concat($vbatch_id,';',$vordernumber,';',$vorderlinenumber,';',$vlastupdatedbttm,';',$vitemid,';',$vallocatedquantity,';',$vshippedquantity,';',$vinventorysegmentname,';')"/>
							<xsl:text>&#13;&#10;</xsl:text>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<xsl:variable name="vitemidbis" select="ItemID"/>
						<!--Creation de la ligne csv avec separateur-->
						<xsl:value-of select="concat($vbatch_id,';',$vordernumber,';',$vorderlinenumber,';',$vlastupdatedbttm,';',$vitemidbis,';0;0;0;')"/>
						<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
				<!--AllocationDetail-->
				<!--SplitLine-->
				<xsl:choose>
					<xsl:when test="exists(SplitLines/SplitLine)">
						<xsl:for-each select="SplitLines/SplitLine">
							<xsl:variable name="vordersplitlinenumber" select="replace(LineNumber,':','.')"/>
							<xsl:choose>
								<xsl:when test="exists(AllocationDetails/AllocationDetail)">
									<xsl:for-each select="AllocationDetails/AllocationDetail">
										<xsl:variable name="vitemid" select="ItemID"/>
										<xsl:variable name="vallocatedquantity">
											<xsl:variable name="vconditionallocqty" select="AllocationStatus != 'Shipped' and AllocationStatus != 'Partially Shipped'"/>
											<xsl:if test="$vconditionallocqty">
												<xsl:value-of select="AllocatedQuantity"/>
											</xsl:if>
											<xsl:if test="not($vconditionallocqty)">
												<xsl:value-of select="0"/>
											</xsl:if>
										</xsl:variable>
										<xsl:variable name="vshippedquantity">
											<xsl:variable name="vconditionshipqty" select="AllocationStatus = 'Shipped'"/>
											<xsl:if test="$vconditionshipqty">
												<xsl:value-of select="AllocatedQuantity"/>
											</xsl:if>
											<xsl:if test="not($vconditionshipqty)">
												<xsl:value-of select="0"/>
											</xsl:if>
										</xsl:variable>
										<xsl:variable name="vinventorysegmentname" select="InventorySegmentName"/>
										<!--Creation de la ligne csv avec separateur-->
										<xsl:value-of select="concat($vbatch_id,';',$vordernumber,';',$vordersplitlinenumber,';',$vlastupdatedbttm,';',$vitemid,';',$vallocatedquantity,';',$vshippedquantity,';',$vinventorysegmentname,';')"/>
										<xsl:text>&#13;&#10;</xsl:text>
									</xsl:for-each>
								</xsl:when>
								<xsl:otherwise>
									<xsl:variable name="vitemidbis" select="ItemID"/>
									<!--Creation de la ligne csv avec separateur-->
									<xsl:value-of select="concat($vbatch_id,';',$vordernumber,';',$vordersplitlinenumber,';',$vlastupdatedbttm,';',$vitemidbis,';0;0;0;')"/>
									<xsl:text>&#13;&#10;</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
				</xsl:choose>
				<!--SplitLine-->
			</xsl:for-each>
			<!--OrderLine-->
		</xsl:for-each>
		<!--Order-->
	</xsl:template>
</xsl:transform>
