<!-- Fichier de transformation L030 STORE DO Auteur      Date      	Commentaire QMT         08/2016   	Création -->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema"				 exclude-result-prefixes="xs">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template name="convertDOMDateFormat">
		<xsl:param name="input"/>
		<xsl:analyze-string select="$input" regex="^(\d+)/(\d+)/(\d+) (\d+):(\d+)$">
			<xsl:matching-substring>
				<xsl:variable name="yearReg" select="xs:string(regex-group(3))"/>
				<xsl:variable name="year">
					<xsl:choose>
						<xsl:when test="string-length($yearReg) = 2">
							<xsl:value-of select="concat('20', $yearReg)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$yearReg"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="month_Reg" select="xs:string(regex-group(1))"/>
				<xsl:variable name="month">
					<xsl:choose>
						<xsl:when test="string-length($month_Reg) = 1">
							<xsl:value-of select="concat('0', $month_Reg)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$month_Reg"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="dayReg" select="xs:string(regex-group(2))"/>
				<xsl:variable name="day">
					<xsl:choose>
						<xsl:when test="string-length($dayReg) = 1">
							<xsl:value-of select="concat('0', $dayReg)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$dayReg"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="hour" select="xs:integer(regex-group(4))"/>
				<xsl:variable name="min" select="xs:integer(regex-group(5))"/>
				<xsl:variable name="dateFormatted" select="concat($year, '-', $month, '-', $day, 'T00:00:00')"/>
				<!-- TCY -->
				<xsl:value-of select="$dateFormatted"/>
			</xsl:matching-substring>
			<xsl:non-matching-substring>
				<xsl:value-of select="$input"/>
			</xsl:non-matching-substring>
		</xsl:analyze-string>
	</xsl:template>
	<xsl:template match="/">
		<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://www.cegid.fr/Retail/1.0">
			<soapenv:Header/>
			<soapenv:Body>
				<!-- <impl:Create> -->
				<ns:Create>
					<ns:createRequest>
						<ns:DeliveryAddress>
							<ns:City>
								<xsl:value-of select="tXML/Message/DistributionOrder/DestinationCity"/>
							</ns:City>
							<ns:CountryId>
								<xsl:value-of select="tXML/Message/DistributionOrder/DestinationCountry"/>
							</ns:CountryId>
							<ns:FirstName>
								<xsl:value-of select="tXML/Message/DistributionOrder/CustomerName"/>
							</ns:FirstName>
							<ns:LastName>
								<xsl:value-of select="tXML/Message/DistributionOrder/CustomerName"/>
							</ns:LastName>
							<ns:Line1>
								<xsl:value-of select="tXML/Message/DistributionOrder/DestinationAddressLine1"/>
							</ns:Line1>
							<ns:Line2>
								<xsl:value-of select="tXML/Message/DistributionOrder/DestinationAddressLine2"/>
							</ns:Line2>
							<ns:Line3>
								<xsl:value-of select="tXML/Message/DistributionOrder/DestinationAddressLine3"/>
							</ns:Line3>
							<ns:PhoneNumber>
								<xsl:value-of select="tXML/Message/DistributionOrder/DestinationContactTelephoneNbr"/>
							</ns:PhoneNumber>
							<ns:ZipCode>
								<xsl:value-of select="tXML/Message/DistributionOrder/DestinationPostalCode"/>
							</ns:ZipCode>
						</ns:DeliveryAddress>
						<ns:Header>
							<ns:Active>true</ns:Active>
							<ns:CurrencyId>
								<xsl:value-of select="tXML/Message/DistributionOrder/ReferenceField5"/>
							</ns:CurrencyId>
							<!-- <ns:CustomerId><xsl:value-of select="tXML/Message/DistributionOrder/CustomerCode"/></ns:CustomerId> -->
							<!-- <ns:CustomerId>ECFRCD</ns:CustomerId> -->
							<ns:CustomerId>
								<xsl:value-of select="tXML/Message/DistributionOrder/ReferenceField8"/>
							</ns:CustomerId>
							<ns:Date>
								<xsl:variable name="OrderGenerationDate">
									<xsl:call-template name="convertDOMDateFormat">
										<xsl:with-param name="input" select="tXML/Message/DistributionOrder/OrderGenerationDate"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:value-of select="$OrderGenerationDate"/>
							</ns:Date>
							<ns:ExternalReference>
								<xsl:value-of select="tXML/Message/DistributionOrder/SalesOrderNbr"/>
							</ns:ExternalReference>
							<ns:InternalReference>
								<xsl:value-of select="tXML/Message/DistributionOrder/DistributionOrderId"/>
							</ns:InternalReference>
							<ns:LinesUnmodifiable>false</ns:LinesUnmodifiable>
							<ns:OmniChannel>
								<ns:BillingStatus>Pending</ns:BillingStatus>
								<ns:Comment>
									<xsl:variable name="FraudInfo">
										<xsl:for-each select="tXML/Message/DistributionOrder/Comment">
											<xsl:if test="contains(CommentText, 'Name on Card')">
												<xsl:value-of select="substring-after(CommentText,':')"/>
											</xsl:if>
										</xsl:for-each>
									</xsl:variable>
									<xsl:variable name="FraudInfo2">
										<xsl:for-each select="tXML/Message/DistributionOrder/Comment">
											<xsl:if test="contains(CommentText, 'Card Number')">												
												<xsl:value-of select="concat($FraudInfo,' - ', substring-after(CommentText,':'))"/>
											</xsl:if>
										</xsl:for-each>
									</xsl:variable>
									<xsl:variable name="FraudInfo3">
										<xsl:for-each select="tXML/Message/DistributionOrder/Comment">
											<xsl:if test="contains(CommentText, 'Expiration date')">
												<xsl:value-of select="concat($FraudInfo2,' - ', substring-after(CommentText,':'))"/>
											</xsl:if>
										</xsl:for-each>
									</xsl:variable>
									
									
									<xsl:value-of select="$FraudInfo3"/>
								</ns:Comment>
								<ns:DeliveryStoreId>
									<xsl:value-of select="tXML/Message/DistributionOrder/OriginFacilityAliasId"/>
								</ns:DeliveryStoreId>
								<ns:DeliveryType>BookedInStore</ns:DeliveryType>
								<ns:FollowUpStatus>ToBeProcessed</ns:FollowUpStatus>
								<ns:PaymentStatus>Pending</ns:PaymentStatus>
								<ns:ReturnStatus>NotReturned</ns:ReturnStatus>
								<ns:ShippingStatus>Pending</ns:ShippingStatus>
							</ns:OmniChannel>
							<ns:Origin>ECommerce</ns:Origin>
							<ns:StoreId>708</ns:StoreId>
							<ns:TaxExcluded>false</ns:TaxExcluded>
							<ns:Type>CustomerOrder</ns:Type>
							<ns:UserDefinedTables>
								<ns:UserDefinedTable>
									<ns:Id>1</ns:Id>
									<ns:Value>
										<xsl:choose>
											<xsl:when test="tXML/Message/DistributionOrder/PackAndHoldFlag = 1">HLD</xsl:when>
											<xsl:when test="tXML/Message/DistributionOrder/PackAndHoldFlag = 0">NOH</xsl:when>
											<!--A utiliser si on doit mettre une valeur par defaut-->
											<xsl:otherwise>NOH</xsl:otherwise>
										</xsl:choose>
									</ns:Value>
								</ns:UserDefinedTable>
								<ns:UserDefinedTable>
									<ns:Id>2</ns:Id>
									<ns:Value>
										<xsl:choose>
											<xsl:when test="tXML/Message/DistributionOrder/ReferenceField6 = ''">AUC</xsl:when>
											<!--A utiliser si on doit mettre une valeur par defaut-->
											<xsl:otherwise>
												<xsl:value-of select="/tXML/Message/DistributionOrder/ReferenceField6"/>
											</xsl:otherwise>
										</xsl:choose>
										<!-- <xsl:value-of select="/tXML/Message/DistributionOrder/ReferenceField6"/> -->
									</ns:Value>
								</ns:UserDefinedTable>
							</ns:UserDefinedTables>
						</ns:Header>
						<ns:InvoicingAddress>
							<ns:City>
								<xsl:value-of select="tXML/Message/DistributionOrder/BillToCity"/>
							</ns:City>
							<ns:CountryId>
								<xsl:value-of select="tXML/Message/DistributionOrder/BillToCountryCode"/>
							</ns:CountryId>
							<ns:FirstName>
								<xsl:value-of select="tXML/Message/DistributionOrder/BillToName"/>
							</ns:FirstName>
							<ns:LastName>
								<xsl:value-of select="tXML/Message/DistributionOrder/BillToName"/>
							</ns:LastName>
							<ns:Line1>
								<xsl:value-of select="tXML/Message/DistributionOrder/BillToAddressLine1"/>
							</ns:Line1>
							<ns:Line2>
								<xsl:value-of select="tXML/Message/DistributionOrder/BillToAddressLine2"/>
							</ns:Line2>
							<ns:Line3>
								<xsl:value-of select="tXML/Message/DistributionOrder/BillToAddressLine3"/>
							</ns:Line3>
							<ns:PhoneNumber>
								<xsl:value-of select="tXML/Message/DistributionOrder/BillToTelephoneNbr"/>
							</ns:PhoneNumber>
							<ns:TitleId>
								<xsl:value-of select="tXML/Message/DistributionOrder/BillToTitle"/>
							</ns:TitleId>
							<ns:ZipCode>
								<xsl:value-of select="tXML/Message/DistributionOrder/BillToPostalCode"/>
							</ns:ZipCode>
						</ns:InvoicingAddress>
						<ns:Lines>
							<xsl:for-each select="tXML/Message/DistributionOrder/LineItem">
								<ns:Create_Line>
									<ns:ExternalReference>
										<xsl:value-of select="DoLineNbr"/>
									</ns:ExternalReference>
									<ns:ItemIdentifier>
										<ns:Reference>
											<xsl:value-of select="ItemName"/>
										</ns:Reference>
									</ns:ItemIdentifier>
									<ns:NetUnitPrice>
										<xsl:value-of select="TotalMonetaryValue"/>
									</ns:NetUnitPrice>
									<ns:Origin>ECommerce</ns:Origin>
									<ns:Quantity>
										<xsl:value-of select="Quantity/OrderQty"/>
									</ns:Quantity>
									<ns:UnitPrice>
										<xsl:value-of select="RetailPrice"/>
									</ns:UnitPrice>
								</ns:Create_Line>
							</xsl:for-each>
							<xsl:for-each select="tXML/Message/DistributionOrder/Comment">
								<xsl:if test="contains(CommentText, '(Header) VAS Gift Message')">
									<ns:Create_Line>
										<ns:ExternalReference>99</ns:ExternalReference>
										<ns:ItemIdentifier>
											<ns:Reference>9990000162277</ns:Reference>
										</ns:ItemIdentifier>
										<!-- <ns:Label>99</ns:Label> -->
										<ns:NetUnitPrice>0</ns:NetUnitPrice>
										<ns:Origin>ECommerce</ns:Origin>
										<ns:Quantity>1</ns:Quantity>
										<ns:UnitPrice>0</ns:UnitPrice>
									</ns:Create_Line>
								</xsl:if>
							</xsl:for-each>
						</ns:Lines>
					</ns:createRequest>
					<ns:clientContext>
						<ns:DatabaseId>PGI</ns:DatabaseId>
					</ns:clientContext>
				</ns:Create>
				<!-- </impl:Create> -->
			</soapenv:Body>
		</soapenv:Envelope>
	</xsl:template>
</xsl:stylesheet>