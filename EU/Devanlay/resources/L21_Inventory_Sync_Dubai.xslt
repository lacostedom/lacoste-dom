<xsl:transform version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="/">
		<!--<xml>-->
		<inventory xmlns="http://www.demandware.com/xml/impex/inventory/2007-05-31">
			<inventory-list>
								<xsl:variable name="Facility" select="/tXML/Message/SupplyBalanceOutput/Line_Item[1]/Facility"/>
				<xsl:variable name="ListId">
					<xsl:choose>
						<xsl:when test="$Facility !='1000002'">
							<xsl:value-of select="$Facility"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="'lacoste-inventory-dubai'"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<header list-id="{$ListId}">
					<default-instock>false</default-instock>
				</header>
				<records>
				<xsl:for-each select="/tXML/Message/SupplyBalanceOutput/Line_Item">
					<xsl:variable name="Total_Supply_Balance">
						<xsl:choose>
							<xsl:when test="number(Total_Supply_Balance) &lt; 0">
								<xsl:value-of select="'0'"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="Total_Supply_Balance"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					
						<record product-id="{Item}">
							<allocation><xsl:value-of select="$Total_Supply_Balance"/></allocation>
							<perpetual>false</perpetual>
						</record>
					
				</xsl:for-each>
				</records>
			</inventory-list>
		</inventory>
		<!--</xml>-->
	</xsl:template>
</xsl:transform>
