<xsl:transform version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="/">
		<!--<xml>-->
		<inventory xmlns="http://www.demandware.com/xml/impex/inventory/2007-05-31">
			<xsl:for-each select="/tXML/Message/SupplyBalanceOutput/Line_Item">
				<inventory-list>					
					<header list-id="{Facility}">
						<default-instock>false</default-instock> 
					</header>
					<records>
						<record product-id="{Item}">
							<allocation>
								<xsl:value-of select="Total_Supply_Balance"/>
							</allocation>
							<perpetual>false</perpetual>
						</record>
					</records>
				</inventory-list>
			</xsl:for-each>
		</inventory>
		<!--</xml>--> 
	</xsl:template>
</xsl:transform>