<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="@*|node()">
		<xsl:if test="normalize-space(string(.)) != ''">
			<xsl:copy>
				<xsl:apply-templates select="node()|@*"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>
	<!-- recupération des éléments du discount sur l'order -->
	<xsl:template match="/tXML/Message/Order/Invoices/InvoiceDetail[InvoiceType='Adjustment']/DiscountDetails">
		<DiscountDetails>
			<xsl:for-each select="DiscountDetail">
				<xsl:variable name="ExtDiscountId" select="ExtDiscountId"/>
				<xsl:variable name="DiscountType" select="DiscountType"/>
				<xsl:variable name="DiscountAmount" select="DiscountAmount"/>
				<xsl:copy-of select="/tXML/Message/Order/DiscountDetails/DiscountDetail[ DiscountStatus = 'Applied' and MarkForDeletion='false' and ExtDiscountId=$ExtDiscountId and DiscountType=$DiscountType and DiscountAmount=$DiscountAmount]"/>
			</xsl:for-each>
		</DiscountDetails>
	</xsl:template>
	<!-- recalcul de la valorisation des retours -->
	<xsl:template match="/tXML/Message/Order/Invoices/InvoiceDetail[InvoiceType='Return']/InvoiceLines/InvoiceLineDetail/LineSubTotal">
		<xsl:variable name="ParentOrderLineNbr" select="../ReferenceOrderLineNbr"/>
		<xsl:variable name="OrderedItem" select="../OrderedItem"/>
		<xsl:variable name="InvoicedQuantity" select="../InvoicedQuantity"/>
		<xsl:variable name="LineInvoiceTotal">
			<xsl:number value="/tXML/Message/Order/Invoices/InvoiceDetail[InvoiceType='Shipment']/InvoiceLines/InvoiceLineDetail[ShippedItem=$OrderedItem][1]/LineTotal"/>
		</xsl:variable>
		<xsl:variable name="LineInvoiceQty">
			<xsl:number value="/tXML/Message/Order/Invoices/InvoiceDetail[InvoiceType='Shipment']/InvoiceLines/InvoiceLineDetail[ShippedItem=$OrderedItem][1]/InvoicedQuantity"/>
		</xsl:variable>
		<xsl:variable name="UnitPrice" select="$LineInvoiceTotal div $LineInvoiceQty"/>
		<LineSubTotal>
			<xsl:value-of select="format-number($UnitPrice * $InvoicedQuantity*-1,'0.00')"/>
		</LineSubTotal>
	</xsl:template>
	
	<xsl:template match="/tXML/Header/Message_Type">
		<Message_Type>SalesPosting_EURECA</Message_Type>
	</xsl:template>
	
	<!-- removing unusable infos -->
	<xsl:template match="TaxDetails"/>
	<xsl:template match="WMProcessInfo"/>
</xsl:stylesheet>
