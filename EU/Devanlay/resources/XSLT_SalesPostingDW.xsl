<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" exclude-result-prefixes="fn xs">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

	<xsl:template match="node()|@*">		
			<xsl:if test="normalize-space(string(.)) != '' or  (ancestor-or-self::ReturnOrder and ancestor-or-self::ReferenceFields)" >
				<xsl:choose>
					<xsl:when test="normalize-space(string(.)) = '' and self::ReferenceField1">
						<ReferenceField1>
							<xsl:value-of select="../../OrderNumber"/>
						</ReferenceField1>
					</xsl:when>
					<xsl:otherwise>
						<xsl:copy>
							<xsl:apply-templates select="node()|@*"/>
						</xsl:copy>
					</xsl:otherwise>
				</xsl:choose>		
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>