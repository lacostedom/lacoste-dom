<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema"				 exclude-result-prefixes="xs">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="/">
		<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://www.cegid.fr/Retail/1.0">
			<soapenv:Header/>
			<soapenv:Body>
				<ns:AddNewCustomer>
					<ns:customerData>
						<ns:EmailData>
							<ns:Email>
								<xsl:value-of select="/tXML/Message/Order/CustomerInfo/CustomerEmail"/>
							</ns:Email>
						</ns:EmailData>
						<ns:FirstName>
							<xsl:value-of select="/tXML/Message/Order/CustomerInfo/CustomerFirstName"/>
						</ns:FirstName>
						<ns:LastName>
							<xsl:value-of select="/tXML/Message/Order/CustomerInfo/CustomerLastName"/>
						</ns:LastName>
						<ns:PhoneData>
							<ns:CellularPhoneNumber>
								<xsl:value-of select="/tXML/Message/Order/CustomerInfo/CustomerPhone"/>
							</ns:CellularPhoneNumber>
						</ns:PhoneData>
						<ns:CustomerId>
							<xsl:value-of select="/tXML/Message/Order/CustomerInfo/CustomerId"/>
						</ns:CustomerId>
					</ns:customerData>
					<ns:clientContext>
						<ns:DatabaseId>PGI</ns:DatabaseId>
					</ns:clientContext>
				</ns:AddNewCustomer>
			</soapenv:Body>
		</soapenv:Envelope>
	</xsl:template>
</xsl:stylesheet>