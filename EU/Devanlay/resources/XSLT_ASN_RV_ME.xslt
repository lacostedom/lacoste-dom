<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:output method="xml" indent="yes" encoding="utf-8" omit-xml-declaration="no"/>

  <xsl:template name="wms-to-dom-date">
    <!--  09/10/2017 23:28:25 to 10/09/2017 23:28:25 -->
    <xsl:param name="dt"/>
    <xsl:variable name="day" select="substring-before($dt,'/')"/>
    <xsl:variable name="dd_yy_hh_mm_cet" select="substring-after($dt, '/')"/>
    <xsl:variable name="month" select="substring-before($dd_yy_hh_mm_cet,'/')"/>
    <xsl:variable name="yy_hh_mm_cet" select="substring-after($dd_yy_hh_mm_cet, '/')"/>
    <xsl:variable name="year" select="substring-before($yy_hh_mm_cet,' ')"/>
    <xsl:variable name="hh_mm_cet" select="substring-after($yy_hh_mm_cet, ' ')"/>

    <xsl:variable name="time">
      <xsl:value-of select="$hh_mm_cet"/>
    </xsl:variable>
    <xsl:value-of select="concat(format-number(number($month),'00'), '/', format-number(number($day),'00') , '/' , $year, ' ', $time)"/>
  </xsl:template>
  
<xsl:template name="asn">
	<xsl:param name="status"/>
	<xsl:param name="message"/>
	<ASN>
          <ASNID>
            <xsl:value-of select="$message/ReceivingStatus/ReturnTrackingNumber"/>
          </ASNID>
          <!-- valeur constante -->
          <ASNType>20</ASNType>
          <!-- valeur constante -->
          <ASNStatus><xsl:value-of select="$status"/></ASNStatus>
          <DestinationFacilityAliasID>
            <xsl:value-of select="$message/ReceivingStatus/ReceivingLoc"/>
          </DestinationFacilityAliasID>
          <DeliveryStart>
            <xsl:call-template name="wms-to-dom-date">
              <xsl:with-param name="dt" select="$message/ReceivingStatus/ReceiptDate"/>
            </xsl:call-template>
          </DeliveryStart>
          <!-- valeur constante -->
          <OriginType>R</OriginType>
          <ReturnReferenceNumber>
            <xsl:value-of select="$message/ReceivingStatus/OrigCustOrderNbr"/>
          </ReturnReferenceNumber>
          <!-- valeur constante -->
          <IsCancelled>0</IsCancelled>
          <!-- valeur constante -->
          <ReceiptType>2</ReceiptType>
          <xsl:variable name="rs" select="$message/ReceivingStatus"/>
          <xsl:variable name="seqs" select="distinct-values($message/ReceivingStatus/ReceivingDetail/LineSeqNumber)" />
          <xsl:for-each select="$seqs">
			    <xsl:variable name="seq" select="."/>
				<xsl:variable name="receivingDetail" select="$rs/ReceivingDetail[LineSeqNumber=$seq][position()=1]"/>
				<xsl:variable name="sumQty" select="sum($rs/ReceivingDetail[LineSeqNumber=$seq]/QtyReceived)"/>
				<ASNDetail>
              <SequenceNumber>
                <xsl:value-of select="number($seq)"/>
              </SequenceNumber>
              <ItemName>
                <xsl:value-of select="$receivingDetail/Item"/>
              </ItemName>
              <PurchaseOrderID></PurchaseOrderID>
              <PurchaseOrderLineItemID></PurchaseOrderLineItemID>
              <InventoryAttributes>
                <!-- valeur constante -->
                <InventoryType>F</InventoryType>
              </InventoryAttributes>
              <Quantity>
                <ReceivedQty><xsl:value-of select="$sumQty"/></ReceivedQty>
                <!-- valeur constante -->
                <QtyUOM>Units</QtyUOM>
              </Quantity>
              <UnitDetails>
                <ReferenceNumber>1</ReferenceNumber>
                <Quantity><xsl:value-of select="$sumQty"/></Quantity>
                <!-- valeur constante -->
                <QtyUOM>Units</QtyUOM>
                <ReceiveConditionCode>
                  <xsl:value-of select="$receivingDetail/ConditionCode"/>
                </ReceiveConditionCode>
                <!-- valeur constante -->
                <ReturnAction>01</ReturnAction>
              </UnitDetails>
            </ASNDetail>
          </xsl:for-each>
        </ASN>
</xsl:template>  
  
  <xsl:template match="/tXML">
    <tXML>
      <Header>
        <!-- vu avec Thierry, ils vont s'aligner sur le nom des balises -->
        <xsl:copy-of select="Header/Source" />
        <!-- valeur constante -->
        <Action_Type>Create</Action_Type>
        <Reference_ID>
          <xsl:value-of select="Message/ReceivingStatus/OrigCustOrderNbr"/>
        </Reference_ID>
        <xsl:copy-of select="Header/Message_Type" />
        <xsl:copy-of select="Header/Company_ID" />
      </Header>
      <Message>
      
       <xsl:variable name="status" select="(30, 40)"/>  
       <xsl:variable name="message" select="Message"/>  
      
      <xsl:for-each select="$status">
		  <xsl:call-template name="asn">
				  <xsl:with-param name="status" select="."/>
				  <xsl:with-param name="message" select="$message"/>
				</xsl:call-template>
      </xsl:for-each>      
      </Message>
    </tXML>
  </xsl:template>
</xsl:stylesheet>
