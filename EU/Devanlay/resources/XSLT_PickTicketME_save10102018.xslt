<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:output method="xml" indent="yes" encoding="utf-8" omit-xml-declaration="no"/>

  <xsl:template name="dom-to-wms-date">
    <!--  10/9/17 23:28 to 09/10/2017 23:28:00 -->
    <xsl:param name="dt"/>
    <xsl:variable name="month" select="substring-before($dt,'/')"/>
    <xsl:variable name="dd_yy_hh_mm_cet" select="substring-after($dt, '/')"/>
    <xsl:variable name="day" select="substring-before($dd_yy_hh_mm_cet,'/')"/>
    <xsl:variable name="yy_hh_mm_cet" select="substring-after($dd_yy_hh_mm_cet, '/')"/>
    <xsl:variable name="year" select="substring-before($yy_hh_mm_cet,' ')"/>
    <xsl:variable name="hh_mm_cet" select="substring-after($yy_hh_mm_cet, ' ')"/>

    <xsl:variable name="time">
      <xsl:choose>
        <xsl:when test="contains($hh_mm_cet, 'CET')">
          <xsl:value-of select="substring-before($hh_mm_cet, ' ')"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$hh_mm_cet"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:value-of select="concat(format-number($day,'00'), '/', format-number($month,'00') , '/' ,'20', $year, ' ', $time, ':00')"/>
  </xsl:template>

  <xsl:template match="/">
    <OrderList xmlns="http://www.oracle.com/retail/integration/ecom/OrderList">

      <xsl:for-each select="/tXML/Message/DistributionOrder">
        <Order>
          <!-- Constante -->
          <OperationType>CustomerOrder</OperationType>
          <CustomerDetail>
            <CustomerNumber>
              <xsl:value-of select="ProcessInfo/RefTextField6"/>
            </CustomerNumber>
            <CustomerShippingAddrLine1>
              <xsl:value-of select='translate(DestinationAddressLine1, "&apos;", " ")'/>
            </CustomerShippingAddrLine1>
            <CustomerShippingAddrLine2>
              <xsl:value-of select='translate(DestinationAddressLine2, "&apos;", " ")'/>
            </CustomerShippingAddrLine2>
			
			<xsl:variable name="area" select="Comment/CommentText[starts-with(.,'Area:')]" />
			<xsl:variable name="area2" select="substring-after($area, 'Area:')" />
            
            <CustomerShippingAddrLine3>
               <xsl:value-of select='translate($area2, "&apos;", " ")'/>
            </CustomerShippingAddrLine3>
            <CustomerCity>
              <xsl:value-of select='translate(DestinationCity, "&apos;", " ")'/>
            </CustomerCity>
            <CustomerState>
              <xsl:value-of select='translate(DestinationStateOrProvince, "&apos;", " ")'/>
            </CustomerState>
            <CustomerCntryCode>
              <xsl:value-of select='translate(DestinationCountry, "&apos;", " ")'/>
            </CustomerCntryCode>
            <CustomerPostalCode>
              <xsl:value-of select="DestinationPostalCode"/>
            </CustomerPostalCode>
            <CustomerContactName>
              <xsl:value-of select='translate(DestinationContactName, "&apos;", " ")'/>
            </CustomerContactName>
            <CustomerContactPhoneNumber>
              <xsl:value-of select="DestinationContactTelephoneNbr"/>
            </CustomerContactPhoneNumber>
            <CustomerContactFaxNumber>
              <xsl:value-of select="DestinationContactFaxNbr"/>
            </CustomerContactFaxNumber>
            <CustomerContactEmailAddress>
              <xsl:value-of select="DestinationContactEmailAddress"/>
            </CustomerContactEmailAddress>
            <!-- Vide vu avec Thierry -->
            <RefNumberField1></RefNumberField1>
            <!-- Vide vu avec Thierry -->
            <RefTextField1></RefTextField1>
          </CustomerDetail>
          <CustomerOrder>
            <CustomerOrderNumber>
              <xsl:value-of select="SalesOrderNbr"/>
            </CustomerOrderNumber>
            <PickTicketNumber>
              <xsl:value-of select="DistributionOrderId"/>
            </PickTicketNumber>
            <!-- Constante -->
            <ShipmentType>CO</ShipmentType>
            <ShipmentDate>
              <xsl:call-template name="dom-to-wms-date">
                <xsl:with-param name="dt" select="OrderedDttm"/>
              </xsl:call-template>
            </ShipmentDate>
            <PaymentMode>
              <xsl:value-of select="ProcessInfo/RefTextField5"/>
            </PaymentMode>
            <!--
              "Si le Reference Field est égal à ""COD"" alors valeur du champs <MonetaryValue>
                Si valeur <> de COD (ou vide) alors =0"
            -->
			<xsl:variable name="TotalLinePrice">
				<xsl:value-of select="sum(/tXML/Message/DistributionOrder/LineItem/RetailPrice)"/>
			</xsl:variable>
			<xsl:variable name="ShippingCharges">
				<xsl:value-of select="/tXML/Message/DistributionOrder/ShippingCharges"/>
			</xsl:variable>
            <OrderValue>
				<xsl:choose>
					<xsl:when test="$ShippingCharges = ''">
					<xsl:value-of select="$TotalLinePrice"/>
					</xsl:when>
					<xsl:otherwise>
					<xsl:value-of select="$TotalLinePrice + $ShippingCharges"/>
					</xsl:otherwise>
				  <!--<xsl:choose>
                <xsl:when test="ProcessInfo/RefTextField5 = 'COD'">-->
                <!--</xsl:when>
                <xsl:otherwise>0</xsl:otherwise>
              </xsl:choose>-->
				</xsl:choose>
            </OrderValue>
            <!--
            "Si champs <DestinationCountry> = AE alors CurrencyCode=AED
              Si champs <DestinationCountry> = SA alors CurrencyCode=SR"
            -->
            <CurrencyCode>
              <xsl:choose>
                <xsl:when test="DestinationCountry = 'AE'">AED</xsl:when>
                <xsl:when test="DestinationCountry = 'SA'">SAR</xsl:when>
              </xsl:choose>
            </CurrencyCode>
            <ShipmentPriority>
              <xsl:value-of select="DsgShipVia"/>
            </ShipmentPriority>
            <SourceLoc>
              <xsl:value-of select="OriginFacilityAliasId"/>
            </SourceLoc>
            <!-- Constante -->
            <StockStatus>001</StockStatus>
            <eStore>
              <xsl:value-of select="ReferenceField3"/>
            </eStore>
            <CustomerNumber>
              <xsl:value-of select="ProcessInfo/RefTextField6"/>
            </CustomerNumber>

            <xsl:variable name="gift" select="Comment/CommentText[starts-with(.,'(Header) VAS Gift Message:')]" />
            
            <GiftWrapIndicator>
              <xsl:choose>
                <xsl:when test="$gift">Y</xsl:when>
                <xsl:otherwise>N</xsl:otherwise>
              </xsl:choose>
            </GiftWrapIndicator>
            <GiftMessage>
              <xsl:value-of select="substring-after($gift, '(Header) VAS Gift Message:')"/>
            </GiftMessage>

            <!-- Vide vu avec Thierry -->
            <RefNumberField1></RefNumberField1>
            <!-- Vide vu avec Thierry -->
            <RefTextField1></RefTextField1>

            <xsl:for-each select="LineItem">
              <CustomerOrderDetail>
                <OrderLineNumber>
                  <xsl:value-of select="DoLineNbr"/>
                </OrderLineNumber>
                <ItemNumber>
                  <xsl:value-of select="ItemName"/>
                </ItemNumber>
                <Quantity>
                  <xsl:value-of select="Quantity/OrderQty"/>
                </Quantity>
                <UnitPrice>
                  <xsl:value-of select="UnitMonetaryValue"/>
                </UnitPrice>
                <!-- Vide vu avec Thierry -->
                <RefNumberField1></RefNumberField1>
                <!-- Vide vu avec Thierry -->
                <RefTextField1></RefTextField1>
              </CustomerOrderDetail>
            </xsl:for-each>
          </CustomerOrder>
        </Order>
      </xsl:for-each>
    </OrderList>
  </xsl:template>
</xsl:stylesheet>
