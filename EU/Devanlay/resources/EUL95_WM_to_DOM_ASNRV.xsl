<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ext="http://exslt.org/common" version="2.0" exclude-result-prefixes="ext">
	<xsl:include href="CommonFunctions.xsl"/>
	<xsl:output method="xml" omit-xml-declaration="no" indent="yes"/>
	<xsl:template match="/">
		<xsl:variable name="CompanyID" select="1"/>
		<xsl:variable name="ASNID" select="/tXML/Message/PIX[1]/PIXFields/Reference1"/>
		<tXML>
			<Header>
				<Source>WMOS</Source>
				<Action_Type>Create</Action_Type>
				<Reference_ID>
					<xsl:value-of select="$ASNID"/>
				</Reference_ID>
				<Message_Type>ASN RV</Message_Type>
				<Company_ID>
					<xsl:value-of select="$CompanyID"/>
				</Company_ID>
				<Msg_Locale>English (United States)</Msg_Locale>
				<Msg_Time_Zone>Europe/Paris</Msg_Time_Zone>
				<User_ID/>
				<Password/>
				<Company_ID>
					<xsl:value-of select="$CompanyID"/>
				</Company_ID>
			</Header>
			<Message>
				<!-- ASN Received -->
				<xsl:for-each select="/tXML/Message/PIX[TransactionCode='20']">
					<xsl:if test="(count(preceding::PIX[TransactionCode='20']/PIXFields[Reference1=$ASNID]) = 0)">
						<ASN>
							<ASNID>
								<xsl:value-of select="$ASNID"/>
							</ASNID>
							<ASNType>20</ASNType>
							<ASNStatus>30</ASNStatus>
							<DestinationFacilityAliasID>EU1</DestinationFacilityAliasID>
							<DeliveryStart>
								<xsl:call-template name="convertWMDateToDOMDate">
									<xsl:with-param name="WMDateTime" select="PIXFields/DateCreated"/>
								</xsl:call-template>
							</DeliveryStart>
							<DeliveryEnd>
								<xsl:call-template name="convertWMDateToDOMDate">
									<xsl:with-param name="WMDateTime" select="PIXFields/DateCreated"/>
								</xsl:call-template>
							</DeliveryEnd>
							<OriginType>R</OriginType>
							<IsCancelled>0</IsCancelled>
							<ReceiptType>2</ReceiptType>
							<ReturnReferenceNumber>
								<xsl:value-of select="PIXFields/ReferenceField7"/>
							</ReturnReferenceNumber>
							<xsl:for-each select="/tXML/Message/PIX[TransactionCode='20'and PIXFields[Reference1=$ASNID and number(UnitsReceived) != 0]]">
								<xsl:variable name="ReceiveConditionCode" select="PIXFields/TransReasonCode"/>
								<!-- Si le PIX remonte un numéro de boutique dans le referencefield8 alors on annule l'asn -->
								<xsl:variable name="IsConnectReturned"> 
									<xsl:choose>
										<xsl:when test="PIXFields/ReferenceField8=''" >
											<xsl:value-of select="'0'"/>
										</xsl:when>
										<xsl:when test="PIXFields/ReferenceField8 = /tXML/Message/PIX/PIXFields/Warehouse">
											<xsl:value-of select="'0'"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="'1'"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								
								<ASNDetail>
									<IsCancelled>
										<xsl:value-of select="$IsConnectReturned"/>
									</IsCancelled>
									<SequenceNumber>
										<xsl:value-of select="PIXFields/ReferenceField9"/>
									</SequenceNumber>
									<ItemName>
										<xsl:value-of select="PIXFields/CustomReference"/>
									</ItemName>
									<InventoryAttributes>
										<InventoryType>
											<xsl:value-of select="SubSKUFields/InventoryType"/>
										</InventoryType>
									</InventoryAttributes>
									<Quantity>
										<ReceivedQty>
											<xsl:value-of select="number(PIXFields/UnitsReceived)"/>
										</ReceivedQty>
										<QtyUOM>Units</QtyUOM>
									</Quantity>
									<UnitDetails>
										<ReferenceNumber>1</ReferenceNumber>
										<Quantity>
											<xsl:value-of select="number(PIXFields/UnitsReceived)"/>
										</Quantity>
										<QtyUOM>Units</QtyUOM>
										<ReceiveConditionCode>
											<xsl:value-of select="PIXFields/TransReasonCode"/>
										</ReceiveConditionCode>
										<!-- B 028 Syscode value-->
										<ReturnAction>01</ReturnAction>
										<!--  NEW FIELD FOR 2012 : credit(Default) = 01   replace = 02  -->
									</UnitDetails>
									<Notes>
										<Note>
											<xsl:value-of select="concat('Return Reason:', PIXFields/ReferenceField8)" />
										</Note>
										<Visibility>0</Visibility>
									</Notes>
									<Notes>
										<Note>
											<xsl:choose>
												<xsl:when test="$ReceiveConditionCode='1' or $ReceiveConditionCode='01'">
													<xsl:value-of select="concat('Return Condition: ', $ReceiveConditionCode, ' - ', 'Contrefacon')" />
												</xsl:when>
												<xsl:when test="$ReceiveConditionCode='2' or $ReceiveConditionCode='02'">
													<xsl:value-of select="concat('Return Condition: ', $ReceiveConditionCode, ' - ', 'Abime, Lave, Porte' )" />
												</xsl:when>
												<xsl:when test="$ReceiveConditionCode='3' or $ReceiveConditionCode='03'">
													<xsl:value-of select="concat('Return Condition: ', $ReceiveConditionCode, ' - ', 'Defectueux')" />
												</xsl:when>
												<xsl:when test="$ReceiveConditionCode='4' or $ReceiveConditionCode='04'">
													<xsl:value-of select="concat('Return Condition: ', $ReceiveConditionCode, ' - ', 'RAS')" />
												</xsl:when>
												<xsl:when test="$ReceiveConditionCode='5' or $ReceiveConditionCode='05'">
													<xsl:value-of select="concat('Return Condition: ', $ReceiveConditionCode, ' - ', 'Reconditionnable SOLODI')" />
												</xsl:when>
												<xsl:when test="$ReceiveConditionCode='6' or $ReceiveConditionCode='06'">
													<xsl:value-of select="concat('Return Condition: ', $ReceiveConditionCode, ' - ', 'Plastique manquant')" />
												</xsl:when>
												<xsl:when test="$ReceiveConditionCode='7' or $ReceiveConditionCode='07'">
													<xsl:value-of select="concat('Return Condition: ', $ReceiveConditionCode, ' - ', 'Produit retourne par erreur')" />
												</xsl:when>
												<xsl:when test="$ReceiveConditionCode='8' or $ReceiveConditionCode='08'">
													<xsl:value-of select="concat('Return Condition: ', $ReceiveConditionCode, ' - ', 'Non reconditionnable par SOLODI')" />
												</xsl:when>
												<xsl:when test="$ReceiveConditionCode='9' or $ReceiveConditionCode='09'">
													<xsl:value-of select="concat('Return Condition: ', $ReceiveConditionCode, ' - ', 'Produit LAB')" />
												</xsl:when>
												<xsl:when test="$ReceiveConditionCode='10' or $ReceiveConditionCode='010'">
													<xsl:value-of select="concat('Return Condition: ', $ReceiveConditionCode, ' - ', 'NPAI')" />
												</xsl:when>
												<xsl:otherwise>
													<xsl:value-of select="concat('Return Condition: ', $ReceiveConditionCode)" />
												</xsl:otherwise>
											</xsl:choose>
										</Note>
										<Visibility>0</Visibility>
									</Notes>
								</ASNDetail>
							</xsl:for-each>
						</ASN>
					</xsl:if>
				</xsl:for-each>
				<!-- ASN Verified -->
				<xsl:for-each select="/tXML/Message/PIX[TransactionCode='20']">
					<xsl:if test="(count(preceding::PIX[TransactionCode='20']/PIXFields[Reference1=$ASNID]) = 0)">
						<ASN>
							<ASNID>
								<xsl:value-of select="$ASNID"/>
							</ASNID>
							<ASNType>20</ASNType>
							<ASNStatus>40</ASNStatus>
							<DestinationFacilityAliasID>EU1</DestinationFacilityAliasID>
							<DeliveryStart>
								<xsl:call-template name="convertWMDateToDOMDate">
									<xsl:with-param name="WMDateTime" select="PIXFields/DateCreated"/>
								</xsl:call-template>
							</DeliveryStart>
							<ReferenceField2>
								<xsl:value-of select="PIXFields/ReferenceField8"/>
							</ReferenceField2>
							<OriginType>R</OriginType>
							<IsCancelled>0</IsCancelled>
							<ReceiptType>2</ReceiptType>
							<ReturnReferenceNumber>
								<xsl:value-of select="PIXFields/ReferenceField7"/>
							</ReturnReferenceNumber>

							<xsl:for-each select="/tXML/Message/PIX[TransactionCode='20' and PIXFields[Reference1=$ASNID and number(UnitsReceived) != 0]]">
								<xsl:variable name="Sequence" select="PIXFields/ReferenceField9"/>
								<xsl:variable name="Item" select="PIXFields/CustomReference"/>
								<!-- Si le PIX remonte un numéro de boutique dans le referencefield8 alors on annule l'asn -->
								<xsl:variable name="IsConnectReturned"> 
									<xsl:choose>
										<xsl:when test="PIXFields/ReferenceField8=''" >
											<xsl:value-of select="'0'"/>
										</xsl:when>
										<xsl:when test="PIXFields/ReferenceField8 = /tXML/Message/PIX/PIXFields/Warehouse">
											<xsl:value-of select="'0'"/>
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="'1'"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>
								<xsl:if test="(count(preceding::PIX[TransactionCode='20']/PIXFields[Reference1=$ASNID and ReferenceField9=$Sequence and CustomReference=$Item]) = 0)">
									<xsl:variable name="ReceiveConditionCode" select="PIXFields/TransReasonCode"/>
									<ASNDetail>
										<IsCancelled>
											<xsl:value-of select="$IsConnectReturned"/>
										</IsCancelled>
										<SequenceNumber>
											<xsl:value-of select="$Sequence"/>
										</SequenceNumber>
										<ItemName>
											<xsl:value-of select="$Item"/>
										</ItemName>
										<InventoryAttributes>
											<InventoryType>
												<xsl:value-of select="SubSKUFields/InventoryType"/>
											</InventoryType>
										</InventoryAttributes>
										<Quantity>
											<ShippedQty>
												<xsl:value-of select="sum(/tXML/Message/PIX[TransactionCode='20' and PIXFields[Reference1=$ASNID and ReferenceField9=$Sequence and CustomReference=$Item]]/PIXFields/UnitsReceived)"/>
											</ShippedQty>
											<ReceivedQty>
												<xsl:value-of select="sum(/tXML/Message/PIX[TransactionCode='20' and PIXFields[Reference1=$ASNID and ReferenceField9=$Sequence and CustomReference=$Item]]/PIXFields/UnitsReceived)"/>
											</ReceivedQty>
											<QtyUOM>Units</QtyUOM>
											<!-- configured qty uom value -->
										</Quantity>
										<Notes>
											<Note>
												<xsl:value-of select="concat('Return Reason:', PIXFields/ReferenceField8)" />
											</Note>
											<Visibility>0</Visibility>
										</Notes>
										<Notes>
											<Note>
												<xsl:choose>
													<xsl:when test="$ReceiveConditionCode='1' or $ReceiveConditionCode='01'">
														<xsl:value-of select="concat('Return Condition: ', $ReceiveConditionCode, ' - ', 'Contrefacon')" />
													</xsl:when>
													<xsl:when test="$ReceiveConditionCode='2' or $ReceiveConditionCode='02'">
														<xsl:value-of select="concat('Return Condition: ', $ReceiveConditionCode, ' - ', 'Abime, Lave, Porte' )" />
													</xsl:when>
													<xsl:when test="$ReceiveConditionCode='3' or $ReceiveConditionCode='03'">
														<xsl:value-of select="concat('Return Condition: ', $ReceiveConditionCode, ' - ', 'Defectueux')" />
													</xsl:when>
													<xsl:when test="$ReceiveConditionCode='4' or $ReceiveConditionCode='04'">
														<xsl:value-of select="concat('Return Condition: ', $ReceiveConditionCode, ' - ', 'RAS')" />
													</xsl:when>
													<xsl:when test="$ReceiveConditionCode='5' or $ReceiveConditionCode='05'">
														<xsl:value-of select="concat('Return Condition: ', $ReceiveConditionCode, ' - ', 'Reconditionnable SOLODI')" />
													</xsl:when>
													<xsl:when test="$ReceiveConditionCode='6' or $ReceiveConditionCode='06'">
														<xsl:value-of select="concat('Return Condition: ', $ReceiveConditionCode, ' - ', 'Plastique manquant')" />
													</xsl:when>
													<xsl:when test="$ReceiveConditionCode='7' or $ReceiveConditionCode='07'">
														<xsl:value-of select="concat('Return Condition: ', $ReceiveConditionCode, ' - ', 'Produit retourne par erreur')" />
													</xsl:when>
													<xsl:when test="$ReceiveConditionCode='8' or $ReceiveConditionCode='08'">
														<xsl:value-of select="concat('Return Condition: ', $ReceiveConditionCode, ' - ', 'Non reconditionnable par SOLODI')" />
													</xsl:when>
													<xsl:when test="$ReceiveConditionCode='9' or $ReceiveConditionCode='09'">
														<xsl:value-of select="concat('Return Condition: ', $ReceiveConditionCode, ' - ', 'Produit LAB')" />
													</xsl:when>
													<xsl:when test="$ReceiveConditionCode='10' or $ReceiveConditionCode='010'">
														<xsl:value-of select="concat('Return Condition: ', $ReceiveConditionCode, ' - ', 'NPAI')" />
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="concat('Return Condition: ', $ReceiveConditionCode)" />
													</xsl:otherwise>
												</xsl:choose>
											</Note>
											<Visibility>0</Visibility>
										</Notes>
									</ASNDetail>
								</xsl:if>
							</xsl:for-each>
						</ASN>
					</xsl:if>
				</xsl:for-each>
			</Message>
		</tXML>
	</xsl:template>
</xsl:stylesheet>