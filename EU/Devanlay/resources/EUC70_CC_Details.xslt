<?xml version="1.0" encoding="utf-8"?>
<xsl:transform version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">  

	<xsl:output method="xml" indent="yes"/>

	<xsl:template match="/">
	
	<Order_Lines>
	
		<xsl:variable name="Order" select="/tXML/Message/Order"/>
			
		<!-- Liste de toutes les lignes de factures -->
		<xsl:for-each select="$Order/Invoices/InvoiceDetail/InvoiceLines/InvoiceLineDetail[ParentOrderLineNbr=$Order/OrderLines/OrderLine[ShippingInfo/ShipVia='CLNC']/LineNumber]"> 
			<xsl:variable name="InvoiceLine" select="."/> 	
			<xsl:variable name="OrderLine" select="$Order/OrderLines/OrderLine[ShippingInfo/ShipVia='CLNC' and LineNumber=$InvoiceLine/ParentOrderLineNbr and ItemID=$InvoiceLine/ShippedItem]"/>		
			
			<!-- DEBUG -->		
			
			<!--<debugInvoiceLine>
				<xsl:value-of select="$InvoiceLine/InvoiceLineNbr"/>
			</debugInvoiceLine>
			<debugOrderLine>
				<xsl:value-of select="$OrderLine/LineNumber"/>
			</debugOrderLine>-->
			
			
			<Order_Line>				
				
				<alias>
					<xsl:value-of select="concat($Order/OrderNumber,'-',$InvoiceLine/../../InvoiceNbr)"/>			
				</alias>
				<reference>
					<xsl:value-of select="$InvoiceLine/ShippedItem"/>			
				</reference>
				<model>
					<xsl:value-of select="''"/>			
				</model>
				<ean>
					<xsl:value-of select="$InvoiceLine/ShippedItem"/>			
				</ean>
				<name>
					<xsl:value-of select="substring($OrderLine/ItemDescription,1,255)"/>			
				</name>
				<libelle_sku>
					<xsl:value-of select="''"/>			
				</libelle_sku>
				<libelle_couleur>
					<xsl:value-of select="''"/>			
				</libelle_couleur>
				<product_image>
					<xsl:value-of select="concat('http://www.lacoste.com/', $InvoiceLine/ShippedItem, '.html')"/>			
				</product_image>
				<quantity>
					<xsl:value-of select="$InvoiceLine/OrderedQty"/>			
				</quantity>
				<amount>
					<xsl:value-of select="$InvoiceLine/LineTotal"/>			
				</amount>	
				<currency>
					<xsl:value-of select="$Order/ReferenceFields/ReferenceField5"/>			
				</currency>	
				<quantity_prepared>
					<xsl:value-of select="$InvoiceLine/InvoicedQuantity"/>			
				</quantity_prepared>				
				
			</Order_Line>
			
		</xsl:for-each>	
        
	</Order_Lines>		
	
	</xsl:template>	
	
</xsl:transform>
          