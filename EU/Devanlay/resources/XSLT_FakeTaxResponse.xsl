﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    version = "1.0"
    xmlns:xsl = "http://www.w3.org/1999/XSL/Transform">

	<xsl:output
        indent = "yes"/>

	<xsl:template match = "tXML">
<tXML xmlns:c="urn:schemas-cybersource-com:transaction-data-1.59" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
   <Header>
      <Source/>
      <Action_Type/>
      <Sequence_Number/>
      <Batch_ID/>
      <Reference_ID/>
      <User_ID/>
      <Password/>
      <Message_Type/>
      <Company_ID/>
      <Msg_Locale/>
      <Msg_Time_Zone>GMT</Msg_Time_Zone>
      <Version/>
   </Header>
   <Message>
      <taxReply>
	           <reasonCode>100</reasonCode>
			   <grandTotalAmount>000.00</grandTotalAmount>
         <totalCityTaxAmount>0.00</totalCityTaxAmount>
         <city>MONTREAL</city>
         <totalCountyTaxAmount>00.00</totalCountyTaxAmount>
         <totalDistrictTaxAmount>0.00</totalDistrictTaxAmount>
         <totalStateTaxAmount>0.00</totalStateTaxAmount>
         <state>QC</state>
         <totalTaxAmount>00.00</totalTaxAmount>
         <postalCode>H2S 2K6</postalCode>
         <item id="0">
            <cityTaxAmount>0.00</cityTaxAmount>
            <countyTaxAmount>0.00</countyTaxAmount>
            <districtTaxAmount>0.00</districtTaxAmount>
            <stateTaxAmount>0.00</stateTaxAmount>
            <totalTaxAmount>00.00</totalTaxAmount>
         </item>
      </taxReply>
   </Message>
</tXML>
		
	</xsl:template>
</xsl:stylesheet>