<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="/">
	<xsl:variable name="threedays" select="xs:dayTimeDuration('P3D')"/>
		<tXML>
			<Header>
				<Source>LACOSTE</Source>
				<xsl:copy-of select="/tXML/Header/Action_Type"/>
				<xsl:copy-of select="/tXML/Header/Reference_ID"/>
				<Message_Type>Returns Feed</Message_Type>
				<xsl:copy-of select="/tXML/Header/Company_ID"/>
				<xsl:copy-of select="/tXML/Header/Msg_Locale"/>
			</Header>
			<Message>
				<ReturnTracking>
				<ReturnCenter>
					<xsl:choose>
					<xsl:when test="/tXML/Message/ReturnOrder/ReferenceFields/ReferenceField2!=''">
					<xsl:value-of select="/tXML/Message/ReturnOrder/ReferenceFields/ReferenceField2"/>
					
					
					
					</xsl:when>
					<xsl:otherwise>
					<xsl:if test="/tXML/Header/Company_ID='5'"> 
					<xsl:value-of select="'BR1'"/>
					</xsl:if>
					
					<xsl:if test="/tXML/Header/Company_ID='1'"> 
					<xsl:value-of select="'EU1'"/>
					</xsl:if>
					</xsl:otherwise>
					</xsl:choose>
				</ReturnCenter>
					<CreateDate><xsl:value-of select="format-dateTime(current-dateTime(), '[M01]/[D01]/[Y0001] [h1]:[m01] [P01]')"/></CreateDate>
					<TrackingDetails>
						<ReturnTrackingNumber><xsl:value-of select="/tXML/Message/ReturnOrder/ShippingInfo/ReturnLabel/ReturnTrackingNumber"/></ReturnTrackingNumber>
						<DeliveryStartDate><xsl:value-of select="format-dateTime(current-dateTime() + $threedays, '[M01]/[D01]/[Y0001] [h1]:[m01] [P01]')"/></DeliveryStartDate>
					</TrackingDetails>
				</ReturnTracking>
			</Message>
		</tXML>
	</xsl:template>
</xsl:stylesheet>
