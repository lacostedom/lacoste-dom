﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:output method="xml" indent="yes" encoding="utf-8" omit-xml-declaration="no"/>
  <xsl:template match="/">
    <xsl:variable name="LineSequenceNumber" select="1"/>
    <OrderList xmlns="http://www.oracle.com/retail/integration/ecom/OrderList">
      <xsl:for-each select="/tXML/Message/ASN">
        <Order>
          <OperationType>ReturnASN</OperationType>
		  <CustomerDetail>
            <CustomerNumber>1234</CustomerNumber>
            <CustomerShippingAddrLine1>Dummy Address Line 1</CustomerShippingAddrLine1>
            <CustomerShippingAddrLine2>Dummy Address Line 2</CustomerShippingAddrLine2>
            <CustomerShippingAddrLine3>Dummy Address Line 3</CustomerShippingAddrLine3>
            <CustomerCity>Dummy</CustomerCity>
            <CustomerState>Dummy</CustomerState>
            <CustomerCntryCode>Dummy</CustomerCntryCode>
            <CustomerPostalCode>1111</CustomerPostalCode>
            <CustomerContactName>Dummy</CustomerContactName>
            <CustomerContactPhoneNumber>12341234</CustomerContactPhoneNumber>
            <CustomerContactFaxNumber/>
            <CustomerContactEmailAddress>Dummy</CustomerContactEmailAddress>
            <RefNumberField1/>
            <RefTextField1/>
          </CustomerDetail>
          <ASN>
            <ReturnTrackingNumber>
              <xsl:value-of select="ASNID"/>
            </ReturnTrackingNumber>
            <OrigCustOrderNbr>
              <xsl:value-of select="ReturnReferenceNumber"/>
            </OrigCustOrderNbr>
            <xsl:variable name="ecom" select="substring-before(RefField1,'-')"/>
            <eStore>
              <xsl:choose>
                <xsl:when test="DestinationFacilityAliasID = '1000002'">52052</xsl:when>
                <xsl:when test="DestinationFacilityAliasID = '1000022'">52053</xsl:when>
              </xsl:choose>
            </eStore>
            <PhysicalWH>
              <xsl:value-of select="DestinationFacilityAliasID"/>
            </PhysicalWH>
            <!-- Constante -->
            <ASNType>CR</ASNType>
            <!-- Constante -->
            <CustomerID>1234</CustomerID>
            <!--
          Si champs estore vu plus haut = "52052" alors ReturnASNCurrency="AED"
          Si champs estore vu plus haut = "52053" alors ReturnASNCurrency="SAR"
          -->
            <ReturnASNCurrency>
              <xsl:choose>
                <xsl:when test="DestinationFacilityAliasID = '1000002'">AED</xsl:when>
                <xsl:when test="DestinationFacilityAliasID = '1000022'">SAR</xsl:when>
              </xsl:choose>
            </ReturnASNCurrency>
            <Note></Note>
            <AdditionalField1></AdditionalField1>
            <AdditionalField2></AdditionalField2>
            <AdditionalField3></AdditionalField3>
            <xsl:for-each select="ASNDetail">
              <ASNDetail>
                <!-- Constante -->
                <Conditioncode>001</Conditioncode>
                <LineSequenceNumber>
                  <xsl:value-of select="position()"/>
                </LineSequenceNumber>
                <Item>
                  <xsl:value-of select="ItemName"/>
                </Item>
                <QtyExpected>
					<xsl:value-of select="number(substring-before(Quantity/ShippedQty, '.'))"/>
                </QtyExpected>
                <ExtendedPrice>0.0</ExtendedPrice>
                <UnitPrice>0.0</UnitPrice>
                <AdditionalField4></AdditionalField4>
                <AdditionalField5></AdditionalField5>
                <AdditionalField6></AdditionalField6>
              </ASNDetail>
            </xsl:for-each>
          </ASN>
        </Order>
      </xsl:for-each>
    </OrderList>
  </xsl:template>
</xsl:stylesheet>
