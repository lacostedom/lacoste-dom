<?xml version="1.0"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="text"/>
	<xsl:template name="formatDate">
		<xsl:param name="inputDate"/>
		<xsl:analyze-string select="$inputDate" regex="^(\d+)/(\d+)/(\d+) (\d+):(\d+) (\w+)">
			<xsl:matching-substring>
				<!-- Traitement du mois -->
				<xsl:variable name="monthRegex" select="regex-group(1)"/>
				<xsl:variable name="month">
					<xsl:choose>
						<xsl:when test="string-length($monthRegex) = 1">
							<xsl:value-of select="concat('0', $monthRegex)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$monthRegex"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<!-- Traitement du jour -->
				<xsl:variable name="dayRegex" select="regex-group(2)"/>
				<xsl:variable name="day">
					<xsl:choose>
						<xsl:when test="string-length($dayRegex) = 1">
							<xsl:value-of select="concat('0', $dayRegex)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$dayRegex"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<!-- Traitement de l'année -->
				<xsl:variable name="yearRegex" select="regex-group(3)"/>
				<xsl:variable name="year">
					<xsl:choose>
						<xsl:when test="string-length($yearRegex) = 2">
							<xsl:value-of select="concat('20', $yearRegex)"/>
						</xsl:when>
						<xsl:when test="string-length($yearRegex) = 1">
							<xsl:value-of select="concat('200', $yearRegex)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$yearRegex"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<!-- Traitement de l'heure -->
				<xsl:variable name="hourRegex" select="regex-group(4)"/>
				<xsl:variable name="hour">
					<xsl:choose>
						<xsl:when test="string-length($hourRegex) = 1">
							<xsl:value-of select="'0', $hourRegex"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$hourRegex"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<!-- Traitement des minutes -->
				<xsl:variable name="minuteRegex" select="regex-group(5)"/>
				<xsl:variable name="minute">
					<xsl:choose>
						<xsl:when test="string-length($minuteRegex) = 1">
							<xsl:value-of select="concat('0', $minuteRegex)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$minuteRegex"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<!-- Traitement des secondes -->
				<xsl:variable name="second">
					<xsl:value-of select="'00'"/>
				</xsl:variable>
				<xsl:value-of select="concat($year, $month, $day)"/>
			</xsl:matching-substring>
			<xsl:non-matching-substring>
				<xsl:value-of select="$inputDate"/>
			</xsl:non-matching-substring>
		</xsl:analyze-string>
	</xsl:template>
	<xsl:template match="/">
		<!-- static -->
		
		<xsl:variable name="Usuario" select="'SA'"/>
		<xsl:variable name="Codigo_Filial_Origem" select="'1000'"/>
		<xsl:variable name="Codigo_Filial_Retirada" select="'1000'"/>
		<xsl:variable name="Vendedor" select="'B2C'"/>
		<xsl:variable name="codigo_tab_preco" select="'R6'"/>
		<xsl:variable name="operacao_venda" select="'001'"/>
		<xsl:variable name="Cod_Forma_Pgto" select="'03'"/>
		<xsl:variable name="tipo_pedido" select="'4'"/>
		<xsl:variable name="Status_Clearsale" select="'APA'"/>
		<xsl:variable name="principal" select="'1'"/>
		<xsl:variable name="Pais" select="'BRASIL'"/>
		<xsl:variable name="Fillial" select="'LOJA E-COMMERCE'"/>
		<!-- global -->
		<xsl:variable name="CustomerInfo" select="/tXML/Message/Order/CustomerInfo"/>
		<xsl:variable name="OrderLines" select="/tXML/Message/Order/OrderLines"/>
		<xsl:variable name="BillingInfo" select="/tXML/Message/Order/PaymentDetails/PaymentDetail[1]/BillToDetail"/>
		<xsl:variable name="ShippingInfo" select="/tXML/Message/Order/OrderLines/OrderLine[1]/ShippingInfo/ShippingAddress"/>
		<xsl:variable name="OrderLineDCAllocated" select="/tXML/Message/Order/OrderLines/OrderLine[OrderLineStatus='DC Allocated']"/>
		<xsl:variable name="PaymentDetails" select="/tXML/Message/Order/PaymentDetails"/>
		<xsl:variable name="OrderNumber" select="/tXML/Message/Order/OrderNumber"/>
		<xsl:variable name="Total_Discount" select="/tXML/Message/Order/OrderedTotals/TotalDiscounts"/>
		
		<xsl:variable name="DONbr" select="/tXML/Message/Order/DistributionOrders/DistributionOrder[DOFulfillmentStatus='DC Allocated' or DOFulfillmentStatus='Partially DC Allocated'] /DONbr"/>
		<xsl:variable name="qtde_total" select="sum($OrderLineDCAllocated/Quantity/TotalAllocatedQty)"/>
		<xsl:variable name="volume" select="substring-before(/tXML/Message/Order/DistributionOrders/DistributionOrder[DONbr=$DONbr]/ReferenceField1,'/')"/>
		<xsl:variable name="peso" select="substring-before(substring-after(/tXML/Message/Order/DistributionOrders/DistributionOrder[DONbr=$DONbr]/ReferenceField1,'/'),'/')"/>
		<xsl:variable name="frete" select="sum(/tXML/Message/Order/ChargeDetails/ChargeDetail/ChargeAmount)"/>
		<xsl:variable name="data">
			<xsl:call-template name="formatDate">
				<xsl:with-param name="inputDate" select="/tXML/Message/Order/OrderCaptureDate"/>
			</xsl:call-template>
		</xsl:variable>
		
		<xsl:variable name="nome_entrega" select="concat($ShippingInfo/ShipToFirstName,' ',$ShippingInfo/ShipToLastName)"/>
		<xsl:variable name="Total_Charges" select="/tXML/Message/Order/OrderedTotals/TotalCharges"/>
		
		<xsl:variable name="valor_total" select="sum($OrderLineDCAllocated/PriceInfo/ExtendedPrice)"/>
		<xsl:variable name="desconto" select="0"/>
		<xsl:variable name="Fullcellular" select="$CustomerInfo/CustomerPhone"/>
		<xsl:variable name="CellularnumsOnly" select="translate($Fullcellular, translate($Fullcellular,'0123456789',''), '')"/>
		<xsl:variable name="Cpf" select="translate($CustomerInfo/CustomerId, translate($CustomerInfo/CustomerId,'0123456789',''), '')"/>
		<xsl:variable name="BillToCep" select="translate($BillingInfo/BillToPostalCode, translate($BillingInfo/BillToPostalCode,'0123456789',''), '')"/>
		<xsl:variable name="ShipToCep" select="translate($ShippingInfo/ShipToPostalCode, translate($ShippingInfo/ShipToPostalCode,'0123456789',''), '')"/>
		<xsl:variable name="cliente_varejo" select="concat($ShippingInfo/ShipToFirstName,' ',$ShippingInfo/ShipToLastName)"/>
		<xsl:variable name="transportadora" select="'13655202000628'"/>
		<xsl:variable name="forma_envio" select="'RODOVIARIO'"/>
		<xsl:variable name="cod_forma_pgto" select="'2'"/>
		<xsl:variable name="conferido" select="'0'"/>
		<xsl:variable name="tipo_logradouro" select="''"/>
		<xsl:variable name="Tipo_Varejo" select="'PESSOA FISICA'"/>
		<xsl:variable name="SellerMpToken" select="concat($OrderNumber,'-',$DONbr)"/>
		<xsl:variable name="MpSellerToken" select="concat($OrderNumber,'-',$DONbr)"/>
		<xsl:variable name="Ddd_Celular" select="substring($CellularnumsOnly,1,2)"/> 
		<xsl:variable name="Celular" select="substring($CellularnumsOnly,3,9)"/> 
		<xsl:variable name="Telefone" select="substring($CellularnumsOnly,3,9)"/> 
		<xsl:variable name="Endereco" select="substring-after($BillingInfo/BillToAddressLine1,',')"/> 
		<xsl:variable name="Numero" select="substring-before($BillingInfo/BillToAddressLine1,',')"/>
		<xsl:variable name="Logradouro" select="substring-after($ShippingInfo/ShipToAddressLine1, ',')"/>
		
{
  "SellerMpToken": "<xsl:value-of select="$SellerMpToken"/>",
  "LojaPedido": [
    {
      "MpSellerToken": "<xsl:value-of select="$MpSellerToken"/>",
      "Pedido": "",
      "Uid_Atendimento": "",
      "Uid_Row": "",
      "Qtde_Total": "<xsl:value-of select="$qtde_total"/>",
      "Id_Endereco_Entrega": "",
      "Id_Loja_Pedido_Lote": "0",
      "Ccf_Venda": "",
      "Coo_Dav": "",
      "Coo_Venda": "",
      "Id_Acao_Campanha": "",
      "Codigo_Campanha": "",
      "Volume": "<xsl:value-of select="$volume"/>",
      "Codigo_Cliente": "<xsl:value-of select="$Cpf"/>",
       "Usuario": "<xsl:value-of select="$Usuario"/>",
      "Nome_Entrega": "<xsl:value-of select="$nome_entrega"/>",
      "Mensagem_Cartao": "0",
      "Cpf_Cgc_Ecf": "",
      "Id_Equipamento_Dav": "",
      "Id_Pedido_Origem": "",
      "Transportadora": "<xsl:value-of select="$transportadora"/>",
       "Forma_Envio": "<xsl:value-of select="$forma_envio"/>",
      "Periodo_Agendamento": "",
      "Pedido_Site": "<xsl:value-of select="$OrderNumber"/>",
      "Identificacao_Cliente": "",
      "Marca": "",
      "Modelo": "",
      "Serie_Nf_Venda": "",
      "Titulo_Dav": "",
      "Id_Entrega_Origem": "",
      "Lx_Hash": "",
      "Obs": "",
      "Observacoes": "0",
      "Codigo_Filial_Origem": "<xsl:value-of select="$Codigo_Filial_Origem"/>",
      "Codigo_Filial_Retirada": "<xsl:value-of select="$Codigo_Filial_Retirada"/>",
      "Vendedor": "<xsl:value-of select="$Vendedor"/>",
      "Codigo_Tab_Preco": "<xsl:value-of select="$codigo_tab_preco"/>",
      "Operacao_Venda": "<xsl:value-of select="$operacao_venda"/>",
      "Cod_Forma_Pgto": "<xsl:value-of select="$Cod_Forma_Pgto"/>",
      "Mf_Adicional": "",
      "Nf_Numero_Venda": "",
      "Sequencial_Pre_Venda": "",
      "Tipo_Ecf": "ecf-if",
      "Codigo_Site": "",
      "Codigo_Filial_Venda": "",
      "Ticket_Venda": "",
      "Status_Lj_Pedido": "",
      "Codigo_Desconto": "",
      "Status_Clearsale": "<xsl:value-of select="$Status_Clearsale"/>",
      "Cnpj_Estabelecimento": "",
      "Tipo_Pedido": "<xsl:value-of select="$tipo_pedido"/>",
      "Motivo_Cancelamento": "",
      "Seq_Ecf_Cf_Dav": "",
      "Cancelado": "0",
      "Entregue": "1",
      "Digitacao_Encerrada": "1",
      "Dav_Impresso": "0",
      "Nao_Grava_Cpf_Ecf": "0",
      "Pf_Pj_Ecf": "0",
      "B2C_Loja": "0",
      "Conferido": "0",
      "Entrega_Agendada": "0",
      "Valor_Total": "<xsl:value-of select="$valor_total"/>",
      "Desconto": "<xsl:value-of select="$Total_Discount"/>",
      "Desconto_Pgto": "0.00",
      "Frete": "<xsl:value-of select="$frete"/>",
      "Peso": "<xsl:value-of select="$peso"/>",
      "Lx_Status_Pedido": "1",
      "Lx_Tipo_Pre_Venda": "0",
      "Status_B2c": "0",
      "Lx_Pedido_Origem": "",
      "Situacao_Oms": "",
      "Tipo_Entrega": "",
      "Data_Venda": "",
      "Data": "<xsl:value-of select="$data"/>",
      "Previsao_Retorno": "",
      "Data_Entrega": "",
      "Data_Faturamento": "",
      "Data_Agendamento": "",
	  "Tracking": "",
	      "ClientesVarejo": {
			"Pedido_Site": "<xsl:value-of select="$OrderNumber"/>",
			"Profissao": "",
			"Apelido": "",
			"Ddd_Celular": "<xsl:value-of select="$Ddd_Celular"/>",
			"Celular": "<xsl:value-of select="$Celular"/>",
			"Nome_Mae": "",
			"Nome_Familia": "",
			"Logradouro": "",
			"Senha": "",
			"Lembrete_Senha": "",
			"Pais": "<xsl:value-of select="$Pais"/>",
			"Tipo_Logradouro": "<xsl:value-of select="$tipo_logradouro"/>",
			"Cep": "<xsl:value-of select="$BillToCep"/>",
			"Telefone": "<xsl:value-of select="$Telefone"/>",
			"Fax": "",
			"Email": "<xsl:value-of select="$BillingInfo/BillToEmail"/>",
			"Bairro": "<xsl:value-of select="$BillingInfo/BillToCounty"/>",
			"Cartao_Afinidade": "",
			"Tipo_Bloqueio": "",
			"Endereco": "<xsl:value-of select="$Endereco"/>",
			"Rg_Ie": "",
			"Cpf_Cgc": "<xsl:value-of select="$Cpf"/>",
			"Cidade": "<xsl:value-of select="$BillingInfo/BillToCity"/>",
			"Complemento": "",
			"Obs": "",
			"Uf": "<xsl:value-of select="$BillingInfo/BillToState"/>",
			"Ddd": "",
			"Sexo": "I",
			"Tipo_Telefone": "",
			"Vendedor": "",
			"Codigo_Contato": "",
			"Codigo_Site": "",
			"Status": "1",
			"Nao_Consulta_Cheque": "0",
			"Numero": "<xsl:value-of select="$Numero"/>",
			"Pontualidade": "",
			"Filial": "<xsl:value-of select="$Fillial"/>",
			"Cliente_Varejo": "<xsl:value-of select="$cliente_varejo"/>",
			"Tipo_Varejo": "<xsl:value-of select="$Tipo_Varejo"/>",
			"Conceito": "",
			"Codigo_Cliente": "<xsl:value-of select="$cliente_varejo"/>",
			"Id": "",
			"Id_Crm_Pfj": "",
			"Id_Acao_Campanha": "",
			"Lx_Status_Cliente": "0",
			"Checksum_Origem": "",
			"Enviado_Serasa": "0",
			"Pf_Pj": "1",
			"Sem_Credito": "0",
			"Enviado_Spc": "0",
			"Inativo_para_Crm": "0",
			"B2C_Bloqueado": "0",
			"Limite_Credito": "0.00",
			"Limite_Credito_Total": "0.00",
			"Estado_Civil": "",
			"Aniversario": "",
			"Cadastramento": "<xsl:value-of select="$data"/>",
			"Ultima_Compra": "",
			"ContatoEndereco": [
			  {
				"Pedido_Site": "<xsl:value-of select="$OrderNumber"/>",
				"Id_Endereco": "",
				"Checksum_Origem": "",
				"Desc_Endereco": "Principal",
				"Pais": "<xsl:value-of select="$Pais"/>",
				"Cep": "<xsl:value-of select="$ShipToCep"/>",
				"Bairro": "<xsl:value-of select="$ShippingInfo/ShipToCounty"/>",
				"Cidade": "<xsl:value-of select="$ShippingInfo/ShipToCity"/>",
				"Complemento": "<xsl:value-of select="$ShippingInfo/ShipToAddressLine2"/>",
				"Numero": "<xsl:value-of select="$Numero"/>",
				"Logradouro": "<xsl:value-of select="$Logradouro"/>",
				"Cod_Municipio_Ibge": "",
				"Codigo_Contato": "",
				"Uf": "<xsl:value-of select="$ShippingInfo/ShipToState"/>",
				"Tipo_Endereco": "",
				"Principal": "1"
			  }
			]
		  },
      "LojaPedidoProduto": [
	   <xsl:for-each select="$OrderLineDCAllocated">
        {
          "Pedido_Site": "<xsl:value-of select="$OrderNumber"/>",
		  "Uid_Row": "",
          "Pedido": "",
          "Item": "",
          "Qtde": "<xsl:value-of select="sum(AllocationDetails/AllocationDetail/AllocatedQuantity)"/>,
          "Qtde_Devolvida": "0",
          "Qtde_Terceiro": "0",
          "Qtde_Venda": "0",
          "Qtde_Conferida": "0",
          "Data_Entrega": "",
          "Data_Inclusao": "<xsl:value-of select="$data"/>",
          "Codigo_Barra": "<xsl:value-of select="ItemID"/>",
          "Descr_Produto": "",
          "Dados_Adicionais": "",
          "B2C_Camisa_Nome": "",
          "B2C_Camisa_Numero": "",
          "B2C_Nome_Personalizacao": "",
          "B2C_Numero_Personalizacao": "",
          "Tracking": "",
          "Lx_Hash": "",
          "Mensagem_Cartao": "",
          "Codigo_Filial_Origem": "<xsl:value-of select="$Codigo_Filial_Origem"/>",
          "Produto": "",
          "Cor_Produto": "",
          "Situacao_Tributaria": "",
          "Unidade": "",
          "Tamanho": "",
          "Id_Vendedor": "1",
          "Cancelado": "0",
          "Indica_Entrega_Futura": "0",
          "Indica_Item_Ecommerce": "0",
          "Embrulha_Presente": "0",
          "Conferido": "1",
          "Preco_Liquido": "<xsl:value-of select="PriceInfo/Price"/>",
          "Desconto_Item": "<xsl:value-of select="DiscountDetails/DiscountDetail/DiscountAmount"/>",
          "Aliquota": "0.00",
          "Rateio_Desconto": "",
          "Rateio_Frete": "",
          "Decimais_Prec_Unit": "0",
          "Decimais_Qtde": "0",
          "Seq_Entrega": "",
        },
		</xsl:for-each>
      ],
      	<!-- payments -->
	  "LojaPedidoPgto": [
			<xsl:for-each select="$PaymentDetails/PaymentDetail">
			{
				<xsl:variable name="Data_Pagamento">
				<xsl:call-template name="formatDate">
					<xsl:with-param name="inputDate" select="PaymentTransactionDetails/PaymentTransactionDetail/RequestedDTTM"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:variable name="Parcelas_Cartao" select="'1'"/><!-- todo -->
			
			  "Pedido_Site": "<xsl:value-of select="$OrderNumber"/>",
			  "Pedido": "",
			  "Parcelas_Cartao": "<xsl:value-of select="$Parcelas_Cartao"/>",
			  "Numero_Titulo": "<xsl:value-of select="substring(ExternalPaymentDetailId,15)"/>",
			  "Conta_Corrente": "",
			  "Numero_Aprovacao_Cartao": "<xsl:value-of select="substring(ExternalPaymentDetailId,15,7)"/>",
			  "Codigo_Filial_Origem": "<xsl:value-of select="$Codigo_Filial_Origem"/>",
			  "Parcela": "01",
			  "Tipo_Pgto": "I", <!-- paypal  ? -->
			  "Agencia": "",
			  "Banco": "",
			  "Moeda": "R$",
			  "Codigo_Administradora": <xsl:value-of select="'02'"/>,
			  "Cheque_Cartao": "",
			  "Cheque_Digito": "",
			  "Id_Plano_Financiamento": "",
			  "Capturado": "1",
			  "Valor": "<xsl:value-of select="sum($OrderLineDCAllocated/LineTotal) + sum(/tXML/Message/Order/ChargeDetails/ChargeDetail/ChargeAmount)"/>",
			  "Valor_Cancelado": "0.00",
			  "Valor_Moeda": "1",
			  "Cotacao": "",
			  "Troco": "",
			  "Valor_Parcela_Financ": "",
			  "Valor_Tac": "0.00",
			  "Vencimento": "",
			  "Data_Pagamento": "<xsl:value-of select="$Data_Pagamento"/>",
			  "Data_Conciliacao": "",
			},
			</xsl:for-each>
		]
    }
  ]
}
		</xsl:template>
</xsl:stylesheet>