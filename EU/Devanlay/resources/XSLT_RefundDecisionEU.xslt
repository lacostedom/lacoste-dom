<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" indent="yes"/>

  <xsl:key name="group" match="tXML/Message/ASN/ASNDetail" use="ItemName"/>
  
<xsl:template match="/">
	<tXML>
		<Header>
			<Source><xsl:value-of select="tXML/Header/Source"/></Source>
			<Action_Type><xsl:value-of select="tXML/Header/Action_Type"/></Action_Type>
			<Reference_ID><xsl:value-of select="tXML/Header/Reference_ID"/></Reference_ID>
			<Message_Type><xsl:value-of select="tXML/Header/Message_Type"/></Message_Type>			
			<Company_ID><xsl:value-of select="tXML/Header/Company_ID"/></Company_ID>
			<Msg_Locale><xsl:value-of select="tXML/Header/Msg_Locale"/></Msg_Locale>
			<Msg_Time_Zone><xsl:value-of select="tXML/Header/Msg_Time_Zone"/></Msg_Time_Zone>
		</Header>
		<Message>
			<ASN>
				<ASNID><xsl:value-of select="tXML/Message/ASN/ASNID"/></ASNID>
				<ASNType>20</ASNType>
				<ASNStatus>30</ASNStatus>
				<DestinationFacilityAliasID>EU1</DestinationFacilityAliasID>
				<DeliveryStart><xsl:value-of select="tXML/Message/ASN/DeliveryStart"/></DeliveryStart>
				<DeliveryEnd><xsl:value-of select="tXML/Message/ASN/DeliveryEnd"/></DeliveryEnd>
				<OriginType>R</OriginType>
				<IsCancelled>0</IsCancelled>
				<ReceiptType>2</ReceiptType>
				<ReturnReferenceNumber><xsl:value-of select="tXML/Message/ASN/ReturnReferenceNumber"/></ReturnReferenceNumber>
        <xsl:for-each select="tXML/Message/ASN/ASNDetail">
          <ASNDetail>
            <ItemName>
              <xsl:value-of select="ItemName"/>
            </ItemName>
            <InventoryAttributes>
              <InventoryType>F</InventoryType>
            </InventoryAttributes>
            <Quantity>
              <ReceivedQty>
                <xsl:value-of select="UnitDetails/Quantity"/>
              </ReceivedQty>
              <QtyUOM>Units</QtyUOM>
            </Quantity>
            <UnitDetails>
              <ReferenceNumber>1</ReferenceNumber>
              <Quantity>
                <xsl:value-of select="UnitDetails/Quantity"/>
              </Quantity>
              <QtyUOM>Units</QtyUOM>
              <ReceiveConditionCode>
                <xsl:value-of select="UnitDetails/ReceiveConditionCode"/>
              </ReceiveConditionCode>
              <ReturnAction>01</ReturnAction>
            </UnitDetails>
          </ASNDetail>
        </xsl:for-each>
			</ASN>
      <ASN>
        <ASNID><xsl:value-of select="tXML/Message/ASN/ASNID"/></ASNID>
        <ASNType>20</ASNType>
        <ASNStatus>40</ASNStatus>
        <DestinationFacilityAliasID>EU1</DestinationFacilityAliasID>
        <DeliveryStart><xsl:value-of select="tXML/Message/ASN/DeliveryStart"/></DeliveryStart>
        <DeliveryEnd><xsl:value-of select="tXML/Message/ASN/DeliveryEnd"/></DeliveryEnd>
        <OriginType>R</OriginType>
        <IsCancelled>0</IsCancelled>
        <ReceiptType>2</ReceiptType>
        <ReturnReferenceNumber><xsl:value-of select="tXML/Message/ASN/ReturnReferenceNumber"/></ReturnReferenceNumber>
        <xsl:for-each select="tXML/Message/ASN/ASNDetail[generate-id()=generate-id(key('group', ItemName)[1])]">
          <ASNDetail>
            <ItemName>
              <xsl:value-of select="ItemName"/>
            </ItemName>
            <InventoryAttributes>
              <InventoryType>F</InventoryType>
            </InventoryAttributes>
            <Quantity>
              <ShippedQty>
                <xsl:value-of select="sum(key('group', ItemName)/UnitDetails/Quantity)"/>
              </ShippedQty>
              <ReceivedQty>
                <xsl:value-of select="sum(key('group', ItemName)/UnitDetails/Quantity)"/>
              </ReceivedQty>
              <QtyUOM>Units</QtyUOM>
            </Quantity>
          </ASNDetail>
        </xsl:for-each>
      </ASN>
		</Message>
	</tXML>
</xsl:template>

</xsl:stylesheet>