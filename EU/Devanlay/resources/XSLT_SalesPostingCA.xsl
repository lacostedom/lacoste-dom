<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" exclude-result-prefixes="fn xs">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="node()|@*">
		<xsl:if test="normalize-space(string(.)) != '' or  (ancestor-or-self::ReturnOrder and ancestor-or-self::ReferenceFields)">
			<xsl:choose>
				<xsl:when test="normalize-space(string(.)) = '' and self::ReferenceField1">
					<ReferenceField1>
						<xsl:value-of select="../../OrderNumber"/>
					</ReferenceField1>
				</xsl:when>
				<xsl:otherwise>
					<xsl:copy>
						<xsl:apply-templates select="node()|@*"/>
					</xsl:copy>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>	
		
	</xsl:template>
	
	<xsl:template match="/tXML/Message/Order/OrderLines/OrderLine/ShippingInfo/DesignatedCarrierCode">
		<DesignatedCarrierCode>
		
			<xsl:choose>
						<!-- DEBUT: EXPEDITO Change to the right carrier for Channel Advisor -->
						<xsl:when test="/tXML/Message/Order/EnteredLocation='822' and /tXML/Message/Order/OrderLines/OrderLine/ShippingInfo/ShipVia='STND'">
							<!--Zalando UK carrier for Standard shipping-->
							<xsl:value-of select="'hermes'"/>
						</xsl:when>
						<xsl:when test="/tXML/Message/Order/EnteredLocation='820' and /tXML/Message/Order/OrderLines/OrderLine/ShippingInfo/ShipVia='STND'">
							<!--Zalando FR carrier for Standard shipping-->
							<xsl:value-of select="'colissimo'"/>
						</xsl:when>
						<xsl:when test="/tXML/Message/Order/EnteredLocation='821' and /tXML/Message/Order/OrderLines/OrderLine/ShippingInfo/ShipVia='STND'">
							<!--Zalando DE carrier for Standard shipping-->
							<xsl:value-of select="'dhl paket'"/>
						</xsl:when>
						<xsl:when test="/tXML/Message/Order/EnteredLocation='829' and /tXML/Message/Order/OrderLines/OrderLine/ShippingInfo/ShipVia='STND'">
							<!--Zalando CH carrier for Standard shipping-->
							<xsl:value-of select="'swiss post'"/>
						</xsl:when>
						<xsl:when test="/tXML/Message/Order/EnteredLocation='823' and /tXML/Message/Order/OrderLines/OrderLine/ShippingInfo/ShipVia='STND'">
							<!--Amazon FR carrier for Standard shipping-->
							<xsl:value-of select="'colissimo'"/>
						</xsl:when>
						<xsl:when test="/tXML/Message/Order/EnteredLocation='823' and /tXML/Message/Order/OrderLines/OrderLine/ShippingInfo/ShipVia='EXPR'">
							<!--Amazon FR carrier for Express shipping-->
							<xsl:value-of select="'chronopost'"/>
						</xsl:when>
						<xsl:when test="/tXML/Message/Order/EnteredLocation='824' and /tXML/Message/Order/OrderLines/OrderLine/ShippingInfo/ShipVia='STND'">
							<!--Amazon DE carrier for Standard shipping-->
							<xsl:value-of select="'colissimo'"/>
						</xsl:when>
						<xsl:when test="/tXML/Message/Order/EnteredLocation='824' and /tXML/Message/Order/OrderLines/OrderLine/ShippingInfo/ShipVia='EXPR'">
							<!--Amazon DE carrier for Express shipping-->
							<xsl:value-of select="'dhl'"/>
						</xsl:when>
						<xsl:when test="/tXML/Message/Order/EnteredLocation='825' and /tXML/Message/Order/OrderLines/OrderLine/ShippingInfo/ShipVia='STND'">
							<!--Amazon GB carrier for Standard shipping-->
							<xsl:value-of select="'colissimo'"/>
						</xsl:when>
						<xsl:when test="/tXML/Message/Order/EnteredLocation='825' and /tXML/Message/Order/OrderLines/OrderLine/ShippingInfo/ShipVia='EXPR'">
							<!--Amazon GB carrier for Express shipping-->
							<xsl:value-of select="'dhl'"/>
						</xsl:when>
						<xsl:when test="/tXML/Message/Order/EnteredLocation='826' and /tXML/Message/Order/OrderLines/OrderLine/ShippingInfo/ShipVia='STND'">
							<!--Amazon IT carrier for Standard shipping-->
							<xsl:value-of select="'colissimo'"/>
						</xsl:when>
						<xsl:when test="/tXML/Message/Order/EnteredLocation='826' and /tXML/Message/Order/OrderLines/OrderLine/ShippingInfo/ShipVia='EXPR'">
							<!--Amazon IT carrier for Express shipping-->
							<xsl:value-of select="'dhl'"/>
						</xsl:when>
						<xsl:when test="/tXML/Message/Order/EnteredLocation='827' and /tXML/Message/Order/OrderLines/OrderLine/ShippingInfo/ShipVia='STND'">
							<!--Amazon ES carrier for Standard shipping-->
							<xsl:value-of select="'colissimo'"/>
						</xsl:when>
						<xsl:when test="/tXML/Message/Order/EnteredLocation='827' and /tXML/Message/Order/OrderLines/OrderLine/ShippingInfo/ShipVia='EXPR'">
							<!--Amazon ES carrier for Express shipping-->
							<xsl:value-of select="'dhl'"/>
						</xsl:when>
						<xsl:when test="/tXML/Message/Order/EnteredLocation='828' and /tXML/Message/Order/OrderLines/OrderLine/ShippingInfo/ShipVia='STND'">
							<!--Harvey Nichols UK carrier for Standard shipping-->
							<xsl:value-of select="'colissimo'"/>
						</xsl:when>
						<xsl:when test="/tXML/Message/Order/EnteredLocation='832' and /tXML/Message/Order/OrderLines/OrderLine/ShippingInfo/ShipVia='STND'">
							<!--OTTO DE carrier for Standard shipping-->
							<xsl:value-of select="'dhl paket'"/>
						</xsl:when>
					</xsl:choose>
			<!-- FIN: EXPEDITO Change to the right carrier for Channel Advisor -->
		
		</DesignatedCarrierCode>
	</xsl:template>
	
	
</xsl:stylesheet>
