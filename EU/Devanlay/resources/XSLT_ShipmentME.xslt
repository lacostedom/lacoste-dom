﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:output method="xml" indent="yes" encoding="utf-8" omit-xml-declaration="no"/>

  <xsl:template name="wms-to-dom-date">
    <!--  09/10/2017 23:28:25 to 10/09/2017 23:28:25 -->
    <xsl:param name="dt"/>
    <xsl:variable name="day" select="substring-before($dt,'/')"/>
    <xsl:variable name="dd_yy_hh_mm_cet" select="substring-after($dt, '/')"/>
    <xsl:variable name="month" select="substring-before($dd_yy_hh_mm_cet,'/')"/>
    <xsl:variable name="yy_hh_mm_cet" select="substring-after($dd_yy_hh_mm_cet, '/')"/>
    <xsl:variable name="year" select="substring-before($yy_hh_mm_cet,' ')"/>
    <xsl:variable name="hh_mm_cet" select="substring-after($yy_hh_mm_cet, ' ')"/>

    <xsl:variable name="time">
      <xsl:value-of select="$hh_mm_cet"/>
    </xsl:variable>
    <xsl:value-of select="concat(format-number($month,'00'), '/', format-number($day,'00') , '/' , $year, ' ', $time)"/>
  </xsl:template>

  <xsl:template name="fqty">
    <xsl:param name="qty"/>
    <xsl:choose>
      <xsl:when test="contains($qty, '.')">
        <xsl:value-of select="number(substring-before($qty, '.'))"></xsl:value-of>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$qty"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="/tXML">
    <tXML>
      <Header>
        <xsl:copy-of select="Header/Source" />
        <!-- valeur constante -->
        <Action_Type>Create</Action_Type>
        <xsl:copy-of select="Header/ShipmentStatus" />
        <xsl:copy-of select="Header/MessageStatus" />
        <xsl:copy-of select="Header/Reference_ID" />
        <xsl:copy-of select="Header/Message_Type" />
        <xsl:copy-of select="Header/Company_ID" />
      </Header>
      <Message>
        <ASN>
          <ASNID>ASN_<xsl:value-of select="Header/Reference_ID"/></ASNID>
          <!-- valeur constante -->
          <ASNType>20</ASNType>
          <!-- valeur constante -->
          <ASNStatus>20</ASNStatus>
          <BusinessUnit>
            <xsl:value-of select="Header/Company_ID"/>
          </BusinessUnit>
          <xsl:copy-of select="Message/ShipmentStatus/OriginFacilityAliasID" />
          <ActualShippedDTTM>
            <xsl:call-template name="wms-to-dom-date">
              <xsl:with-param name="dt" select="Message/ShipmentStatus/ActualShippedDTTM"/>
            </xsl:call-template>
          </ActualShippedDTTM>
          <DeliveryStart>
            <xsl:call-template name="wms-to-dom-date">
              <xsl:with-param name="dt" select="Message/ShipmentStatus/ActualShippedDTTM"/>
            </xsl:call-template>
          </DeliveryStart>
          <!-- valeur constante -->
          <RepresentativeName>CHALHOUB</RepresentativeName>
          <!-- valeur constante -->
          <ContactAddress1>Jebel Ali Freezone,Near Gate 4</ContactAddress1>
          <!-- valeur constante -->
          <ContactCity>Dubaï</ContactCity>
          <!-- valeur constante -->
          <ContactPostalCode>36312</ContactPostalCode>
          <!-- valeur constante -->
          <ContactCountry>PC</ContactCountry>
          <!-- valeur constante -->
          <OriginType>W</OriginType>
          <LPN>
            <LPNID>LPN_<xsl:value-of select="Header/Reference_ID"/></LPNID>
            <BusinessUnit>
              <xsl:value-of select="Header/Company_ID"/>
            </BusinessUnit>
            <!-- valeur constante -->
            <LPNType>LPN</LPNType>
            <!-- valeur constante -->
            <LPNStatus>Built</LPNStatus>
            <LPNStatusDate>
              <xsl:call-template name="wms-to-dom-date">
                <xsl:with-param name="dt" select="Message/ShipmentStatus/ActualShippedDTTM"/>
              </xsl:call-template>
            </LPNStatusDate>
            <xsl:copy-of select="Message/ShipmentStatus/TrackingNbr" />
            <!--<ReturnTrackingNumber></ReturnTrackingNumber>-->
            <ReturnTrackingNumber2>
              <xsl:value-of select="Message/ShipmentStatus/TrackingNbr"/>
            </ReturnTrackingNumber2>
            <ReturnReferenceNumber>
              <xsl:value-of select="Message/ShipmentStatus/CustomerOrderNumber"/>
            </ReturnReferenceNumber>
            <DistributionOrderID>
              <xsl:value-of select="Header/Reference_ID"/>
            </DistributionOrderID>
            <xsl:for-each select="Message/ShipmentStatus/ShipmentStatusDetail">
              <LPNDetail>
                <ItemName>
                  <xsl:value-of select="ItemNumber"/>
                </ItemName>
                <DistributionOrderLineItemID>
                  <xsl:value-of select="number(OrderLineNumber)"/>
                </DistributionOrderLineItemID>
                <LPNDetailQuantity>
                  <Quantity>
                    <xsl:call-template name="fqty">
                      <xsl:with-param name="qty" select="OriginalQuantity"/>
                    </xsl:call-template>
                  </Quantity>
                  <QuantityUOM>Units</QuantityUOM>
                  <ShippedAsnQuantity>
                    <xsl:call-template name="fqty">
                      <xsl:with-param name="qty" select="ShippedQuantity"/>
                    </xsl:call-template>
                  </ShippedAsnQuantity>
                  <!-- Not specified -->
                  <CancelledQuantity></CancelledQuantity>
                  <CancelReasonCode></CancelReasonCode>
                </LPNDetailQuantity>
              </LPNDetail>
            </xsl:for-each>
          </LPN>
        </ASN>
      </Message>
    </tXML>
  </xsl:template>
</xsl:stylesheet>
