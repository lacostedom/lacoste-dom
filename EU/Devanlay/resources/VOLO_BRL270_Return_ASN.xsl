<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/">
		<tXML>
			<!-- Header -->
			<Header>
				<xsl:copy-of select="/tXML/Header/Source"/>
				<xsl:copy-of select="/tXML/Header/Action_Type"/>
				<xsl:copy-of select="/tXML/Header/Reference_ID"/>
				<Message_Type>BRL270_RETURN_ASN</Message_Type>
				<xsl:copy-of select="/tXML/Header/Company_ID"/>
			</Header>
			<!-- Message -->
			<Message>
				<ASN>
					<xsl:copy-of select="/tXML/Message/ASN/ASNID"/>
					<xsl:copy-of select="/tXML/Message/ASN/ASNType"/>
					<xsl:copy-of select="/tXML/Message/ASN/DestinationFacilityAliasID"/>
					<xsl:copy-of select="/tXML/Message/ASN/ReturnReferenceNumber"/>
					<xsl:for-each select="/tXML/Message/ASN/ASNDetail">
						<ASNDetail>
							<xsl:copy-of select="ItemName"/>
							<ReasonCode><xsl:value-of select="ReferenceField2"/></ReasonCode>
							
							<DeclaredQty>
								
								<xsl:choose>
									 <xsl:when test="ReferenceField1 != ''">
									   <xsl:value-of select="round(ReferenceField1)"/>
									 </xsl:when>
									 <xsl:otherwise>
									  <xsl:value-of select="round(Quantity/ShippedQty)"/>
									 </xsl:otherwise>
								   </xsl:choose>
								
							</DeclaredQty>
							<Quantity>
								<ShippedQty><xsl:value-of select="round(Quantity/ShippedQty)"/></ShippedQty>
								<xsl:copy-of select="Quantity/QtyUOM"/>
							</Quantity>
						</ASNDetail>
					</xsl:for-each>
				</ASN>
			</Message>
		</tXML>
	</xsl:template>
	<!-- TEMPLATE - Get a YYYY-MM-DD date -->
	<xsl:template name="convertDOMDateFormat_ToDate">
		<xsl:param name="input"/>
		<xsl:analyze-string select="$input" regex="^(\d+)/(\d+)/(\d+) (.*?)$">
			<xsl:matching-substring>
				<xsl:variable name="yearReg" select="xs:string(regex-group(3))"/>
				<xsl:variable name="year">
					<xsl:choose>
						<xsl:when test="string-length($yearReg) = 2">
							<xsl:value-of select="concat('20', $yearReg)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$yearReg"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="month_Reg" select="xs:string(regex-group(1))"/>
				<xsl:variable name="month">
					<xsl:choose>
						<xsl:when test="string-length($month_Reg) = 1">
							<xsl:value-of select="concat('0', $month_Reg)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$month_Reg"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="dayReg" select="xs:string(regex-group(2))"/>
				<xsl:variable name="day">
					<xsl:choose>
						<xsl:when test="string-length($dayReg) = 1">
							<xsl:value-of select="concat('0', $dayReg)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$dayReg"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="dateFormatted" select="concat($year, '-', $month, '-', $day)"/>
				<xsl:value-of select="$dateFormatted"/>
			</xsl:matching-substring>
			<xsl:non-matching-substring>
				<xsl:value-of select="1900-01-01"/>
			</xsl:non-matching-substring>
		</xsl:analyze-string>
	</xsl:template>
	<!-- TEMPLATE - Get a HH24:MI time -->
	<xsl:template name="convertDOMDateFormat_ToTime">
		<xsl:param name="input"/>
		<xsl:analyze-string select="$input" regex="^(.+) (\d):(\d)(.*?)$">
			<xsl:matching-substring>
				<xsl:variable name="hoursReg" select="xs:string(regex-group(1))"/>
				<xsl:variable name="hours">
					<xsl:choose>
						<xsl:when test="string-length($hoursReg) = 1">
							<xsl:value-of select="concat('0', $hoursReg)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$hoursReg"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="minutesReg" select="xs:string(regex-group(2))"/>
				<xsl:variable name="minutes">
					<xsl:choose>
						<xsl:when test="string-length($hoursReg) = 1">
							<xsl:value-of select="concat('0', $hoursReg)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$hoursReg"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="timeFormatted" select="concat($hours, ':', $minutes)"/>
				<xsl:value-of select="$timeFormatted"/>
			</xsl:matching-substring>
			<xsl:non-matching-substring>
				<xsl:value-of select="'00:00'"/>
			</xsl:non-matching-substring>
		</xsl:analyze-string>
	</xsl:template>
</xsl:stylesheet>
