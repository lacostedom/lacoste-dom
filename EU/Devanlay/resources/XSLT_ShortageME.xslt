﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:output method="xml" indent="yes" encoding="utf-8" omit-xml-declaration="no"/>

  <xsl:template name="fqty">
    <xsl:param name="qty"/>
    <xsl:choose>
      <xsl:when test="contains($qty, '.')">
        <xsl:value-of select="number(substring-before($qty, '.'))"></xsl:value-of>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$qty"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="/tXML">
    <tXML>
      <Header>
        <xsl:copy-of select="Header/Source" />
        <!-- valeur constante -->
        <Action_Type>PARTIALUPDATE</Action_Type>
        <xsl:copy-of select="Header/ShipmentStatus" />
        <xsl:copy-of select="Header/MessageStatus" />
        <xsl:copy-of select="Header/Reference_ID" />
        <xsl:copy-of select="Header/Message_Type" />
        <xsl:copy-of select="Header/Company_ID" />
      </Header>
      <Message>
        <DistributionOrder>
          <DistributionOrderId>
            <xsl:value-of select="Header/Reference_ID"/>
          </DistributionOrderId>
          <BusinessUnit>
            <xsl:value-of select="Header/Company_ID"/>
          </BusinessUnit>
          <xsl:for-each select="Message/ShipmentStatus/ShipmentStatusDetail">
            <LineItem>
              <DoLineNbr>
                <xsl:value-of select="number(OrderLineNumber)"/>
              </DoLineNbr>
              <!-- valeur constante -->
              <UpdateActionType>SHORT</UpdateActionType>
              <Quantity>
                <OrderQty>
                    <xsl:call-template name="fqty">
                      <xsl:with-param name="qty" select="OriginalQuantity"/>
                    </xsl:call-template>
                </OrderQty>
                <QtyUOM>Units</QtyUOM>
              </Quantity>
            </LineItem>
          </xsl:for-each>
        </DistributionOrder>
      </Message>
    </tXML>
  </xsl:template>
</xsl:stylesheet>
