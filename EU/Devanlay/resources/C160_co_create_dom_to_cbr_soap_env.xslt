<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	
	<!-- DOM to CBR converter for Origin -->
	<xsl:template name="FormatOrigin">
		<xsl:param name="input" />
		<xsl:choose>
			<xsl:when test="$input = 'ECM' ">
				<xsl:value-of select=" 'ECommerce' " />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select=" 'Shop' " />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<!-- Date converter from "DD/MM/YY HH:mm TimeZone" to "YYYY-MM-DD'T'HH:MM:'00'" -->
	<xsl:template name="convertDOMDateFormat">
		<xsl:param name="input" />
		<xsl:analyze-string select="$input" regex="^(\d+)/(\d+)/(\d+) (\d+):(\d+) (\w+)$">
		
			<xsl:matching-substring>
				<xsl:variable name="yearReg" select="xs:string(regex-group(3))" />
				<xsl:variable name="year">
					<xsl:choose>
						<xsl:when test="string-length($yearReg) = 2">
							<xsl:value-of select="concat(('20'), $yearReg)" />
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$yearReg" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				
				<xsl:variable name="monthReg" select="xs:string(regex-group(1))" />
				<xsl:variable name="month">
					<xsl:choose>
						<xsl:when test="string-length($monthReg) = 1">
							<xsl:value-of select="concat('0', $monthReg)" />
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$monthReg" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				
				<xsl:variable name="dayReg" select="xs:string(regex-group(2))" />
				<xsl:variable name="day">
					<xsl:choose>
						<xsl:when test="string-length($dayReg) = 1">
							<xsl:value-of select="concat('0', $dayReg)" />
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$dayReg" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				
				<xsl:variable name="hourReg" select="xs:string(regex-group(4))" />
				<xsl:variable name="hour">
					<xsl:choose>
						<xsl:when test="string-length($hourReg) = 1">
							<xsl:value-of select="concat('0', $hourReg)" />
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$hourReg" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				
				<xsl:variable name="minReg" select="xs:string(regex-group(5))" />
				<xsl:variable name="min">
					<xsl:choose>
						<xsl:when test="string-length($minReg) = 1">
							<xsl:value-of select="concat('0', $minReg)" />
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$minReg" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				
				<xsl:variable name="formattedDate" select="concat($year, '-', $month, '-', $day)" />
				<xsl:value-of select="$formattedDate" />
			</xsl:matching-substring>
			
			<xsl:non-matching-substring>
				<xsl:value-of select="$input" />
			</xsl:non-matching-substring>
			
		</xsl:analyze-string>
	</xsl:template>
	
	<xsl:template name="escape-xml">
		<xsl:param name="text" />
		<xsl:if test="$text != ''">
			<xsl:variable name="head" select="fn:substring($text, 1, 1)" />
			<xsl:variable name="tail" select="substring($text, 2)" />
			<xsl:choose>
				<xsl:when test="$head = '&amp;'"> </xsl:when>
				<xsl:when test="$head = '&lt;'">&amp;lt;</xsl:when>
				<xsl:when test="$head = '&gt;'">&amp;&gt;</xsl:when>
				<xsl:when test="$head = '&quot;'">&amp;&quot;</xsl:when>
				<xsl:when test="$head = &quot;&apos;&quot;">&amp;apos;</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$head" />
				</xsl:otherwise>
			</xsl:choose>
			<xsl:call-template name="escape-xml">
				<xsl:with-param name="text" select="$tail" />
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="/">
		<soapenv:Envelope 
			xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" 
			xmlns:ns="http://cbrspec.lacoste.com/ConnectV3/1.0">
		
			<soapenv:Header/>
			
			<soapenv:Body>
				
				<!-- Body/AddCustomerOrder -->
				<ns:AddCustomerOrder>
					<!-- Body/AddCustomerOrder/DataCustomer -->
					<ns:DataCustomer>
					
						<!-- Body/AddCustomerOrder/DataCustomer/ClientContext -->
						<ns:ClientContext>
							<!-- Body/AddCustomerOrder/DataCustomer/ClientContext/DatabaseId -->
							<!-- Constante : PGITSTB -->
							<ns:DatabaseId>
								<!--<xsl:value-of select=" 'PGITSTB' " />-->
								<xsl:value-of select=" 'PGI' " />
							</ns:DatabaseId>
						</ns:ClientContext>
						
						<!-- Body/AddCustomerOrder/DataCustomer/CustomerData -->
						<ns:CustomerData>
							<!-- Body/AddCustomerOrder/DataCustomer/CustomerData/AddressData -->
							<ns:AddressData>
								<!-- Body/AddCustomerOrder/DataCustomer/CustomerData/AddressData/AddressLine1 -->
								<ns:AddressLine1>
									<xsl:value-of select="tXML/Message/Order/PaymentDetails/PaymentDetail[1]/BillToDetail/BillToAddressLine1" />
								</ns:AddressLine1>
								<!-- Body/AddCustomerOrder/DataCustomer/CustomerData/AddressData/AddressLine2 -->
								<ns:AddressLine2>
									<xsl:value-of select="tXML/Message/Order/PaymentDetails/PaymentDetail[1]/BillToDetail/BillToAddressLine2" />
								</ns:AddressLine2>
								<!-- Body/AddCustomerOrder/DataCustomer/CustomerData/AddressData/City -->
								<ns:City>
									<xsl:value-of select="tXML/Message/Order/PaymentDetails/PaymentDetail[1]/BillToDetail/BillToCity" />
								</ns:City>
								<!-- Body/AddCustomerOrder/DataCustomer/CustomerData/AddressData/ZipCode -->
								<ns:ZipCode>
									<xsl:value-of select="tXML/Message/Order/PaymentDetails/PaymentDetail[1]/BillToDetail/BillToPostalCode" />
								</ns:ZipCode>
							</ns:AddressData>
							<!-- Body/AddCustomerOrder/DataCustomer/CustomerData/CustomerId -->
							<ns:CustomerId>
								<xsl:value-of select="tXML/Message/Order/CustomerInfo/CustomerId" />
							</ns:CustomerId>
							<!-- Body/AddCustomerOrder/DataCustomer/CustomerData/EmailData -->
							<ns:EmailData>
								<!-- Body/AddCustomerOrder/DataCustomer/CustomerData/EmailData/Email -->
								<ns:Email>
									<xsl:value-of select="tXML/Message/Order/CustomerInfo/CustomerEmail" />
								</ns:Email>
							</ns:EmailData>
							<!-- Body/AddCustomerOrder/DataCustomer/CustomerData/FirstName -->
							<ns:FirstName>
								<xsl:value-of select="tXML/Message/Order/CustomerInfo/CustomerFirstName" />
							</ns:FirstName>
							<!-- Body/AddCustomerOrder/DataCustomer/CustomerData/IsCompany -->
							<!-- Valeur constante, toujours à 'false' -->
							<ns:IsCompany>
								<xsl:value-of select=" 'false' " />
							</ns:IsCompany>
							<!-- Body/AddCustomerOrder/DataCustomer/CustomerData/LastName -->
							<ns:LastName>
								<xsl:value-of select="tXML/Message/Order/CustomerInfo/CustomerLastName" />
							</ns:LastName>
							<!-- Body/AddCustomerOrder/DataCustomer/CustomerData/PhoneData -->
							<ns:PhoneData>
								<!-- Body/AddCustomerOrder/DataCustomer/CustomerData/PhoneData/CellularPhoneNumber -->
								<ns:CellularPhoneNumber>
									<xsl:value-of select="tXML/Message/Order/CustomerInfo/CustomerPhone" />
								</ns:CellularPhoneNumber>
							</ns:PhoneData>
							<!-- Body/AddCustomerOrder/DataCustomer/CustomerData/TitleId -->
							<!-- TODO: A définir -->
							<ns:TitleId>
							</ns:TitleId>
							<!-- Body/AddCustomerOrder/DataCustomer/CustomerData/UsualStoreId -->
							<ns:UsualStoreId>
								<xsl:value-of select="tXML/Message/Order/EnteredLocation" />
							</ns:UsualStoreId>
						</ns:CustomerData>
						
					</ns:DataCustomer>
					
				
					<!-- Body/AddCustomerOrder/DataOrder -->
					<ns:DataOrder>
					
						<!-- Body/AddCustomerOrder/DataOrder/ClientContext -->
						<ns:ClientContext>
							<!-- Body/AddCustomerOrder/DataOrder/ClientContext/DatabaseId -->
							<!-- Constante : PGITSTB -->
							<ns:DatabaseId>
								<!--<xsl:value-of select=" 'PGITSTB' " />-->
								<xsl:value-of select=" 'PGI' " />
							</ns:DatabaseId>
						</ns:ClientContext>
					
						<!-- Body/AddCustomerOrder/DataOrder/CreateRequest -->
						<ns:CreateRequest>
						
							<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Lines -->
							<ns:Create_Lines>
								<xsl:for-each select="tXML/Message/Order/OrderLines/OrderLine">
									<xsl:variable name="CanceledSource" select="OrderLineStatus" />
									<xsl:if test="$CanceledSource != 'Canceled'">
										<xsl:variable name="LineNumber" select="LineNumber" />
										<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Lines/Create_Line -->	
										<ns:Create_Line>
											<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Lines/Create_Line/ExternalReference -->
											<ns:ExternalReference>
												<xsl:variable name="OrderExternalReference" select="/tXML/Message/Order/OrderNumber" />
												
												<xsl:variable name="ExternalReferenceText" select="concat($OrderExternalReference, '-', $LineNumber)" />
												<xsl:variable name="ExternalReferenceValue" select="fn:concat('CONNECT', $ExternalReferenceText)" />
												<xsl:value-of select="$ExternalReferenceValue" />
											</ns:ExternalReference>
											<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Lines/Create_Line/ItemIdentifier -->
											<ns:ItemIdentifier>
												<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Lines/Create_Line/ItemIdentifier/Reference -->
												<ns:Reference>
													<xsl:value-of select="ItemID" />
												</ns:Reference>
											</ns:ItemIdentifier>
											<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Lines/Create_Line/NetUnitPrice -->
											<!-- Valeur : PriceInfo/Price - (DiscountAmout div OrderedQty) -->
											<ns:NetUnitPrice>
												<xsl:variable name="Price" select="PriceInfo/Price" /> 
												<xsl:variable name="OrderedQty" select="Quantity/OrderedQty" />
												<xsl:variable name="ExtendedPrice" select="PriceInfo/ExtendedPrice" />
												<xsl:variable name="PurchasePrice" select="PriceInfo/PurchasePrice" />
												<xsl:variable name="DiscountAmount" select="$ExtendedPrice - $PurchasePrice" />
												<xsl:variable name="UnitDiscountAmout" select="$DiscountAmount div $OrderedQty" />
												<xsl:variable name="NetUnitPriceValue" select="$Price - $UnitDiscountAmout" />
												<xsl:value-of select="$NetUnitPriceValue" />
											</ns:NetUnitPrice>
											<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Lines/Create_Line/Origin -->
											<!-- Constante : Shop -->
											<ns:Origin>
												<xsl:value-of select=" 'Shop' " />
											</ns:Origin>
											<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Lines/Create_Line/Quantity -->
											<ns:Quantity>
												<xsl:value-of select="Quantity/OrderedQty" />
											</ns:Quantity>
											<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Lines/Create_Line/SalesPersonId -->
											<ns:SalesPersonId>
												<xsl:value-of select="/tXML/Message/Order/EnteredBy" />
											</ns:SalesPersonId>
											
											<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Lines/Create_Line/TaxAmount -->
											<xsl:variable name="varTaxAmount">
												<xsl:if test="not(TaxDetails/TaxDetail)">
													<xsl:value-of select="-1" />
												</xsl:if>
												<xsl:if test="TaxDetails/TaxDetail/TaxAmount">
													<xsl:value-of select="fn:sum(TaxDetails/TaxDetail/TaxAmount)" />
												</xsl:if>
											</xsl:variable>
											<ns:TaxAmount>
												<xsl:value-of select="$varTaxAmount" />
											</ns:TaxAmount>
											
											<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Lines/Create_Line/UnitPrice -->
											<ns:UnitPrice>
												<xsl:value-of select="PriceInfo/Price" />
											</ns:UnitPrice>
											
										</ns:Create_Line>
										<!-- leather goods personalisation charge -->
										<xsl:for-each select="ChargeDetails/ChargeDetail[ChargeName='Personalized Leather Goods']">
											<ns:Create_Line>
												<ns:ExternalReference>
													<xsl:variable name="OrderNumberValue" select="/tXML/Message/Order/OrderNumber" />
													<xsl:value-of select="fn:concat('CONNECT', $OrderNumberValue,'-9',$LineNumber)" />
												</ns:ExternalReference>
												<ns:ItemIdentifier>
													<ns:Reference>
														<xsl:value-of select="'9990000162291'" />  <!-- article CUSMA -->
													</ns:Reference>
												</ns:ItemIdentifier>
												<ns:NetUnitPrice>
													<xsl:value-of select="ChargeAmount" />
												</ns:NetUnitPrice>
												<ns:Origin>
													<xsl:value-of select=" 'Shop' " />
												</ns:Origin>
												<ns:Quantity>
													<xsl:value-of select=" '1' " />
												</ns:Quantity>
												<ns:SalesPersonId>
													<xsl:value-of select="/tXML/Message/Order/EnteredBy" />
												</ns:SalesPersonId>
												<ns:UnitPrice>
													<xsl:value-of select="ChargeAmount" />
												</ns:UnitPrice>
											</ns:Create_Line>
										</xsl:for-each>
									</xsl:if>
								</xsl:for-each>
								
								<!-- Create_Line des frais de ports -->
								<ns:Create_Line>
									<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Lines/Create_Line/ExternalReference -->
									<!-- Value : CONNECT+OrderNumber-0 -->
									<ns:ExternalReference>
										<xsl:variable name="OrderNumberValue" select="/tXML/Message/Order/OrderNumber" />
										<xsl:value-of select="fn:concat('CONNECT', $OrderNumberValue, '-0')" />
									</ns:ExternalReference>
									
									<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Lines/Create_Line/ItemIdentifier -->
									<ns:ItemIdentifier>
										<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Lines/Create_Line/ItemIdentifier/Reference -->
										<!-- Constant : FPSFST -->
										<ns:Reference>
											<xsl:value-of select="'FPSFST'" />
										</ns:Reference>
									</ns:ItemIdentifier>
									
									<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Lines/Create_Line/NetUnitPrice -->
									<ns:NetUnitPrice>
										<xsl:value-of select="/tXML/Message/Order/ChargeDetails/ChargeDetail[1]/ChargeAmount" />
									</ns:NetUnitPrice>
									
									<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Lines/Create_Line/Origin -->
									<ns:Origin>
										<xsl:value-of select=" 'Shop' " />
									</ns:Origin>
									
									<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Lines/Create_Line/Quantity -->
									<ns:Quantity>
										<xsl:value-of select=" '1' " />
									</ns:Quantity>
									
									<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Lines/Create_Line/SalesPersonId -->
									<ns:SalesPersonId>
										<xsl:value-of select="/tXML/Message/Order/EnteredBy" />
									</ns:SalesPersonId>
									
									<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Lines/Create_Line/TaxAmount -->
									<xsl:variable name="varTaxAmountShipping">
										<xsl:if test="/tXML/Message/Order/TaxDetails/TaxDetail/ChargeCategory[. = 'Shipping']">
											<xsl:value-of select="fn:sum(/tXML/Message/Order/TaxDetails/TaxDetail/ChargeCategory[. = 'Shipping']/../TaxAmount)"/>
										</xsl:if>
										<xsl:if test="not(/tXML/Message/Order/TaxDetails/TaxDetail/ChargeCategory[. = 'Shipping'])">
											<xsl:value-of select="-1" />
										</xsl:if>
									</xsl:variable>
									<ns:TaxAmount>
										<xsl:value-of select="$varTaxAmountShipping" />
									</ns:TaxAmount>
										
									<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Lines/Create_Line/UnitPrice -->
									<ns:UnitPrice>
										<xsl:value-of select="/tXML/Message/Order/ChargeDetails/ChargeDetail[1]/ChargeAmount" />
									</ns:UnitPrice>
										
								</ns:Create_Line>
								
							</ns:Create_Lines>
						
							<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/DeliveryAddress -->
							<ns:DeliveryAddress>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/DeliveryAddress/City -->
								<ns:City>
									<xsl:value-of select="tXML/Message/Order/OrderLines/OrderLine[1]/ShippingInfo/ShippingAddress/ShipToCity" />
								</ns:City>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/DeliveryAddress/CountryId -->
								<ns:CountryId>
									<xsl:value-of select="tXML/Message/Order/OrderLines/OrderLine[1]/ShippingInfo/ShippingAddress/ShipToCountry" />
								</ns:CountryId>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/DeliveryAddress/FirstName -->
								<ns:FirstName>
									<xsl:value-of select="tXML/Message/Order/OrderLines/OrderLine[1]/ShippingInfo/ShippingAddress/ShipToFirstName" />
								</ns:FirstName>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/DeliveryAddress/LastName -->
								<ns:LastName>
									<xsl:value-of select="tXML/Message/Order/OrderLines/OrderLine[1]/ShippingInfo/ShippingAddress/ShipToLastName" />
								</ns:LastName>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/DeliveryAddress/Line1 -->
								<ns:Line1>
									<xsl:value-of select="tXML/Message/Order/OrderLines/OrderLine[1]/ShippingInfo/ShippingAddress/ShipToAddressLine1" />
								</ns:Line1>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/DeliveryAddress/Line2 -->
								<ns:Line2>
									<xsl:value-of select="tXML/Message/Order/OrderLines/OrderLine[1]/ShippingInfo/ShippingAddress/ShipToAddressLine2" />
								</ns:Line2>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/DeliveryAddress/Line3 -->
								<ns:Line3 />
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/DeliveryAddress/PhoneNumber -->
								<ns:PhoneNumber>
									<xsl:value-of select="tXML/Message/Order/OrderLines/OrderLine[1]/ShippingInfo/ShippingAddress/ShipToPhone" />
								</ns:PhoneNumber>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/DeliveryAddress/ZipCode -->
								<ns:ZipCode>
									<xsl:value-of select="tXML/Message/Order/OrderLines/OrderLine[1]/ShippingInfo/ShippingAddress/ShipToPostalCode" />
								</ns:ZipCode>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/DeliveryAddress/Civility -->
								<!-- TODO: A définir -->
								<ns:Civility>
								</ns:Civility>
							</ns:DeliveryAddress>
							
							<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Header -->
							<ns:Header>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Header/Active -->
								<!-- Constante : "true"-->
								<ns:Active>
									<xsl:value-of select=" 'true' " />
								</ns:Active>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Header/CurrencyId -->
								<ns:CurrencyId>
									<xsl:value-of select="tXML/Message/Order/ReferenceFields/ReferenceField5" />
								</ns:CurrencyId>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Header/CustomerId -->
								<ns:CustomerId>
									<xsl:value-of select="tXML/Message/Order/CustomerInfo/CustomerId" />
								</ns:CustomerId>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Header/Date -->
								<ns:Date>
									<xsl:call-template name="convertDOMDateFormat">
										<xsl:with-param name="input" select="tXML/Message/Order/OrderCreatedDate" />
									</xsl:call-template>
								</ns:Date>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Header/DeliveryDate -->
								<ns:DeliveryDate>
									<xsl:call-template name="convertDOMDateFormat">
										<xsl:with-param name="input" select="tXML/Message/Order/OrderLines/OrderLine[1]/ShippingInfo/RequestedDeliveryBy" />
									</xsl:call-template>
								</ns:DeliveryDate>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Header/ExternalReference -->
								<ns:ExternalReference>
									<xsl:variable name="ExternalReferenceText" select="tXML/Message/Order/OrderNumber" />
									<xsl:variable name="ExternalReferenceValue" select="fn:concat('CONNECT', $ExternalReferenceText)" />
									<xsl:value-of select="$ExternalReferenceValue" />
								</ns:ExternalReference>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Header/InternalReference -->
								<ns:InternalReference>
									<xsl:value-of select="tXML/Message/Order/OrderNumber" />
								</ns:InternalReference>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Header/IsSplittedOrder -->
								<!-- if tXML/Message/Order/ReferenceFields/ReferenceField7 contains "Split" then "true" else "false -->
								<ns:IsSplittedOrder>
										<xsl:variable name="OrderNumber" select="/tXML/Message/Order/OrderNumber" />
										<xsl:variable name="OrderNumberLastChar" select="fn:upper-case( fn:substring($OrderNumber, fn:string-length($OrderNumber))) " />
										<xsl:choose>
											<xsl:when test="$OrderNumberLastChar = 'A' or $OrderNumberLastChar = 'B'">
												<xsl:value-of select=" 'true' " />
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select=" 'false' " />
											</xsl:otherwise>
										</xsl:choose>
								</ns:IsSplittedOrder>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Header/LinesUnmodifiable -->
								<!-- Constante "false"-->
								<ns:LinesUnModifiable>
									<xsl:value-of select=" 'false' " />
								</ns:LinesUnModifiable>
								
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Header/OmniChannel -->
								<ns:OmniChannel>
									<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Header/OmniChannel/BillingStatus -->
									<!-- Constante : Pending -->
									<ns:BillingStatus>
										<xsl:value-of select=" 'Pending' " />
									</ns:BillingStatus>
									<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Header/OmniChannel/Comment -->
									<ns:Comment />
									<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Header/OmniChannel/DeliveryStoreId -->
									<ns:DeliveryStoreId>
										<xsl:value-of select="tXML/Message/Order/EnteredLocation" />
									</ns:DeliveryStoreId>
									<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Header/OmniChannel/DeliveryType -->
									<ns:DeliveryType>
										<xsl:value-of select=" 'ShipByCentral' " />
									</ns:DeliveryType>
									<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Header/OmniChannel/FollowUpStatus -->
									<!-- Constante : ToBeProcessed -->
									<ns:FollowUpStatus>
										<xsl:value-of select=" 'ToBeProcessed' " />
									</ns:FollowUpStatus>
									<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Header/OmniChannel/PaymentStatus -->
									<!-- Constante : Pending -->
									<ns:PaymentStatus>
										<xsl:value-of select=" 'Pending' " />
									</ns:PaymentStatus>
									<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Header/OmniChannel/ReturnStatus -->
									<!-- Constante : NotReturned -->
									<ns:ReturnStatus>
										<xsl:value-of select=" 'NotReturned' " />
									</ns:ReturnStatus>
									<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/header/OmniChannel/ShippingStatus -->
									<!-- Constante : Pending -->
									<ns:ShippingStatus>
										<xsl:value-of select=" 'Pending' " />
									</ns:ShippingStatus>
								</ns:OmniChannel>
								
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Header/Origin -->
								<!-- Constante : Shop -->
								<ns:Origin>
									<xsl:value-of select=" 'Shop' " />
								</ns:Origin>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Header/SalesPersonId -->
								<ns:SalesPersonId>
									<xsl:value-of select="tXML/Message/Order/EnteredBy" />
								</ns:SalesPersonId>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Header/ShipVia -->
								<ns:ShipVia>
									<xsl:value-of select="tXML/Message/Order/OrderLines/OrderLine[1]/ShippingInfo/ShipVia" />
								</ns:ShipVia>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Header/StoreId -->
								<ns:StoreId>
									<xsl:value-of select="tXML/Message/Order/EnteredLocation" />
								</ns:StoreId>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Header/TaxExcluded -->
								<ns:TaxExcluded>
									<xsl:variable name="varTaxExcluded">
										<xsl:if test="not(tXML/Message/Order/OrderLines/OrderLine/TaxDetails/TaxDetail)">
											<xsl:value-of select="'false'" />
										</xsl:if>
										<xsl:if test="tXML/Message/Order/OrderLines/OrderLine/TaxDetails/TaxDetail">
											<xsl:value-of select="'true'" />
										</xsl:if>
									</xsl:variable>
									<xsl:value-of select="$varTaxExcluded" />
								</ns:TaxExcluded>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Header/Type -->
								<!-- Constante : ReceiptOnHold -->
								<ns:Type>
									<xsl:value-of select=" 'ReceiptOnHold' " />
								</ns:Type>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Header/UserDefinedTables -->
								<!-- Vide ici -->
								<ns:UserDefinedTables />
							</ns:Header>
							
							<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/InvoicingAddress -->
							<ns:InvoicingAddress>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/InvoicingAddress/City -->
								<ns:City>
									<xsl:value-of select="tXML/Message/Order/PaymentDetails/PaymentDetail[1]/BillToDetail/BillToCity" />
								</ns:City>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/InvoicingAddress/CountryId -->
								<ns:CountryId>
									<xsl:value-of select="tXML/Message/Order/PaymentDetails/PaymentDetail[1]/BillToDetail/BillToCountry" />
								</ns:CountryId>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/InvoicingAddress/FirstName -->
								<ns:FirstName>
									<xsl:value-of select="tXML/Message/Order/PaymentDetails/PaymentDetail[1]/BillToDetail/BillToFirstName" />
								</ns:FirstName>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/InvoicingAddress/LastName -->
								<ns:LastName>
									<xsl:value-of select="tXML/Message/Order/PaymentDetails/PaymentDetail[1]/BillToDetail/BillToLastName" />
								</ns:LastName>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/InvoicingAddress/Line1 -->
								<ns:Line1>
									<xsl:value-of select="tXML/Message/Order/PaymentDetails/PaymentDetail[1]/BillToDetail/BillToAddressLine1" />
								</ns:Line1>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/InvoicingAddress/Line2 -->
								<ns:Line2>
									<xsl:value-of select="tXML/Message/Order/PaymentDetails/PaymentDetail[1]/BillToDetail/BillToAddressLine2" />
								</ns:Line2>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/InvoicingAddress/Line3 -->
								<ns:Line3 />
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/InvoicingAddress/PhoneNumber -->
								<ns:PhoneNumber>
									<xsl:value-of select="tXML/Message/Order/PaymentDetails/PaymentDetail[1]/BillToDetail/BillToPhone" />
								</ns:PhoneNumber>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/InvoicingAddress/TitleId -->
								<!-- TODO: à définir -->
								<ns:TitleId>
								</ns:TitleId>
								<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/InvoicingAddress/ZipCode -->
								<ns:ZipCode>
									<xsl:value-of select="tXML/Message/Order/PaymentDetails/PaymentDetail[1]/BillToDetail/BillToPostalCode" />
								</ns:ZipCode>
							</ns:InvoicingAddress>
							
						</ns:CreateRequest>
						
					</ns:DataOrder>
					
					<ns:OriginalXML>
						<xsl:apply-templates mode="escape" />
					</ns:OriginalXML>
					
				</ns:AddCustomerOrder>
			
			</soapenv:Body>
		
		</soapenv:Envelope>
	</xsl:template>
	
	<xsl:template match="*" mode="escape">
		<xsl:text>&lt;</xsl:text>
		<xsl:value-of select="fn:name()" />
		
		<!-- Namespaces -->
		<!--<xsl:for-each select="namespace::*">
			<xsl:text> xmlns</xsl:text>
			<xsl:if test="fn:name() != ''">
				<xsl:text>:</xsl:text>
				<xsl:value-of select="fn:name()"/>
			</xsl:if>
			<xsl:text>='</xsl:text>
			<xsl:call-template name="escape-xml">
				<xsl:with-param name="text" select="." />
			</xsl:call-template>
			<xsl:text>'</xsl:text>
		</xsl:for-each>-->
		
		<!-- Attributes -->
		<xsl:for-each select="@*">
			<xsl:text> </xsl:text>
			<xsl:value-of select="fn:name()"/>
			<xsl:text>='</xsl:text>
			<xsl:call-template name="escape-xml">
				<xsl:with-param name="text" select="."/>
			</xsl:call-template>
			<xsl:text>'</xsl:text>
		</xsl:for-each>
		
		<xsl:text>&gt;</xsl:text>
		
		<xsl:apply-templates select="node()" mode="escape" />
		
		<xsl:text>&lt;/</xsl:text>
		<xsl:value-of select="fn:name()" />
		<xsl:text>&gt;</xsl:text>
		
	</xsl:template>
	
	
	
	
</xsl:stylesheet>