<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="@*|node()">
		<xsl:if test="normalize-space(string(.)) != ''">
			<xsl:copy>
				<xsl:apply-templates select="node()|@*"/>
			</xsl:copy>
		</xsl:if>
	</xsl:template>
	<!-- recupération des éléments du discount sur l'order -->
	<xsl:template match="/tXML/Message/Order/PaymentDetails/PaymentDetail[ExternalPaymentDetailId='']">
	</xsl:template>
	

</xsl:stylesheet>
