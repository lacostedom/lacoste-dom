<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" omit-xml-declaration="no" indent="yes"/>
	<xsl:template match="/">
		<tXML>
			<xsl:copy-of select="tXML/Header"/>
			<Message>
				<SupplyBalanceOutput>
					<xsl:copy-of select="tXML/Message/SupplyBalanceOutput/Action_Type"/>
					<xsl:copy-of select="tXML/Message/SupplyBalanceOutput/View_Type"/>
					<xsl:for-each select="tXML/Message/SupplyBalanceOutput/Line_Item">
						<Line_Item>
							<xsl:copy-of select="Item"/>
							<xsl:copy-of select="Facility"/>
							<xsl:choose>
								<xsl:when test="Total_Supply_Balance>=0" >
									<xsl:copy-of select="Total_Supply_Balance"/>
								</xsl:when>
								<xsl:otherwise >
									<Total_Supply_Balance>0</Total_Supply_Balance>
								</xsl:otherwise>
							</xsl:choose>
						</Line_Item>
					</xsl:for-each>
				</SupplyBalanceOutput>
			</Message>
		</tXML>
	</xsl:template>
</xsl:stylesheet>
