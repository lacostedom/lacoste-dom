﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:output method="xml" indent="yes" encoding="utf-8" omit-xml-declaration="no"/>

  <xsl:template name="fdate1">
    <!--  10/12/17 23:28 CEST to 20171210 -->
    <xsl:param name="dt"/>
    <xsl:variable name="month" select="substring-before($dt,'/')"/>
    <xsl:variable name="dd_yy_hh_mm_cet" select="substring-after($dt, '/')"/>
    <xsl:variable name="day" select="substring-before($dd_yy_hh_mm_cet,'/')"/>
    <xsl:variable name="yy_hh_mm_cet" select="substring-after($dd_yy_hh_mm_cet, '/')"/>
    <xsl:variable name="year" select="substring-before($yy_hh_mm_cet,' ')"/>
    <xsl:variable name="hh_mm_cet" select="substring-after($yy_hh_mm_cet, ' ')"/>
    <xsl:variable name="time" select="substring-before($hh_mm_cet, ' ')"/>
    <xsl:value-of select="concat('20', $year, $month, $day)"/>
  </xsl:template>

  <xsl:template name="fdate2">
    <!--  10/12/17 23:28 CEST to 2017-10-12T23:28:00 -->
    <xsl:param name="dt"/>
    <xsl:variable name="month" select="substring-before($dt,'/')"/>
    <xsl:variable name="dd_yy_hh_mm_cet" select="substring-after($dt, '/')"/>
    <xsl:variable name="day" select="substring-before($dd_yy_hh_mm_cet,'/')"/>
    <xsl:variable name="yy_hh_mm_cet" select="substring-after($dd_yy_hh_mm_cet, '/')"/>
    <xsl:variable name="year" select="substring-before($yy_hh_mm_cet,' ')"/>
    <xsl:variable name="hh_mm_cet" select="substring-after($yy_hh_mm_cet, ' ')"/>
    <xsl:variable name="time" select="substring-before($hh_mm_cet, ' ')"/>
    <xsl:value-of select="concat('20', $year, '-', $month, '-', $day , 'T' , $time, ':00')"/>
  </xsl:template>

  <xsl:template match="/">
    <xsl:for-each select="/tXML/Message/Order">
      <xsl:variable name="lastInvoice" select="Invoices/InvoiceDetail[last()]" />
      <SaleMessage>
        <Head>
          <Business_Date>
            <xsl:call-template name="fdate1">
              <xsl:with-param name="dt" select="PaymentDetails/PaymentDetail/PaymentTransactionDetails/PaymentTransactionDetail[TransactionType='Settlement']/TransactionDTTM"/>
            </xsl:call-template>
          </Business_Date>
          <Transaction_Time>
            <xsl:call-template name="fdate2">
              <xsl:with-param name="dt" select="PaymentDetails/PaymentDetail/PaymentTransactionDetails/PaymentTransactionDetail[TransactionType='Settlement']/TransactionDTTM"/>
            </xsl:call-template>
          </Transaction_Time>
          <Location>
            <xsl:value-of select="PaymentDetails/PaymentDetail/ReferenceFields/ReferenceField4"/>
          </Location>
          <!--
            Si InvoiceType = Shipment alors valeur ""P""
            Si InvoiceType= Return alors valeur ""N""
            Point d'attention, étant donné que le sales posting reprend l'image complète de la commande, en cas de retour, il y aura d'abord 'invoice de Shipment, puis l'invoice de Retour, s'il y a InvoiceType de type Return, il faut considérer que c'est un retour."
          -->
          <Transaction_Type>
            <xsl:choose>
              <xsl:when test="$lastInvoice/InvoiceType = 'Shipment'">P</xsl:when>
              <xsl:when test="$lastInvoice/InvoiceType = 'Return'">N</xsl:when>
            </xsl:choose>
          </Transaction_Type>
          <Customer_Order_ID>
            <xsl:value-of select="OrderNumber"/>
          </Customer_Order_ID>
          <Transaction_Number>
            <xsl:value-of select="$lastInvoice/InvoiceNbr"/>
          </Transaction_Number>
          <Customer_ID/>
          <BP_Number>
            <xsl:value-of select="CustomerInfo/CustomerId"/>
          </BP_Number>
          <Loyalty_ID/>
          <xsl:for-each select="$lastInvoice/InvoiceLines/InvoiceLineDetail">
            <xsl:variable name="barcode" select="ShippedItem" />
            <SaleDetail>
              <Item>
                <xsl:value-of select="ShippedItem"/>
              </Item>
              <ItemComments>
                <xsl:value-of select="/tXML/Message/Order/OrderLines/OrderLine[ItemID=$barcode]/ItemDescription"/>
              </ItemComments>
              <Quantity_Sign>
                <xsl:choose>
                  <xsl:when test="$lastInvoice/InvoiceType = 'Shipment'">P</xsl:when>
                  <xsl:when test="$lastInvoice/InvoiceType = 'Return'">N</xsl:when>
                </xsl:choose>
              </Quantity_Sign>
              <Quantity>
                <xsl:value-of select="ShippedQuantity"/>
              </Quantity>
              <Unit_Retail>
                <xsl:choose>
                  <xsl:when test="contains(UnitPrice, '-')">substring-after(UnitPrice, '-')</xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="UnitPrice"/>
                  </xsl:otherwise>
                </xsl:choose>
              </Unit_Retail>
              <Discount_Amount>
                <xsl:choose>
                  <xsl:when test="(LineDiscounts = 0) and ($lastInvoice/TotalDiscounts != 0)">
                    <xsl:variable name="totalDiscounts" select="$lastInvoice/TotalDiscounts"/>
                    <xsl:variable name="totalAmount" select="$lastInvoice/InvoiceAmount"/>
                    <xsl:variable name="lineAmount" select="LineTotal"/>
                    <xsl:value-of select="$lineAmount div $totalAmount * $totalDiscounts"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="LineDiscounts"/>
                  </xsl:otherwise>
                </xsl:choose>
              </Discount_Amount>
              <Promotion_ID/>
            </SaleDetail>
          </xsl:for-each>

          <xsl:variable name="refTextField5" select="WMProcessInfo/RefTextField5"/>

          <xsl:for-each select="PaymentDetails/PaymentDetail">
            <xsl:variable name="cardType" select="CardType"/>
            <PaymentDetail>
              <Tender_Type_Group>
                <xsl:choose>
                  <xsl:when test="$refTextField5 = 'Credit Card' and $cardType = 'Master Card'">
                    <xsl:value-of select="$refTextField5"/>
                  </xsl:when>
                  <xsl:when test="$refTextField5 = 'Credit Card' and $cardType = 'American Express'">
                    <xsl:value-of select="$refTextField5"/>
                  </xsl:when>
                  <xsl:when test="$refTextField5 = 'Credit Card' and $cardType = 'Visa'">
                    <xsl:value-of select="$refTextField5"/>
                  </xsl:when>
                  <xsl:when test="$refTextField5 = 'COD'">
                    <xsl:value-of select="$refTextField5"/>
                  </xsl:when>
                  <xsl:when test="$refTextField5 = 'Paypal'">
                    <xsl:value-of select="$refTextField5"/>
                  </xsl:when>
                  <xsl:otherwise>VOUCH</xsl:otherwise>
                </xsl:choose>
              </Tender_Type_Group>
              <!--          
              Si <Message><Order><WMProcessInfo><RefTextField5> = à "Credit Card" et <Message><Order><PaymentDetails><PaymentDetail><CardType> = à "Master Card'
              Alors valeur = à "3010"

              Si <Message><Order><WMProcessInfo><RefTextField5> = à "Credit Card" et <Message><Order><PaymentDetails><PaymentDetail><CardType> = à "American Express'
              Alors valeur = à "3020"

              Si <Message><Order><WMProcessInfo><RefTextField5> = à "Credit Card" et <Message><Order><PaymentDetails><PaymentDetail><CardType> = à "Visa"
              Alors valeur = à "3000" 

              Si <Message><Order><WMProcessInfo><RefTextField5> = à "COD" alors valeur = à "1000"

              Si <Message><Order><WMProcessInfo><RefTextField5> = à "Paypal" alors valeur = à "3130"             
              -->
              <Tender_Type_ID>
                <xsl:choose>
                  <xsl:when test="$refTextField5 = 'Credit Card' and $cardType = 'Master Card'">3010</xsl:when>
                  <xsl:when test="$refTextField5 = 'Credit Card' and $cardType = 'American Express'">3020</xsl:when>
                  <xsl:when test="$refTextField5 = 'Credit Card' and $cardType = 'Visa'">3000</xsl:when>
                  <xsl:when test="$refTextField5 = 'COD'">1000</xsl:when>
                  <xsl:when test="$refTextField5 = 'Paypal'">3130</xsl:when>
                  <xsl:otherwise>4050</xsl:otherwise>
                </xsl:choose>
              </Tender_Type_ID>
              <Tender_Type_Amount>
                <xsl:value-of select="ReqSettlementAmount" />
              </Tender_Type_Amount>
              <!--
              Si <Message><Order><PaymentDetails><PaymentDetail><BillToDetail><BillToState> = à "AE" alors inscrire la valeur "AED" 
              -->
              <Original_Currency>
                <xsl:choose>
                  <xsl:when test="BillToDetail/BillToState = 'AE'">AED</xsl:when>
                </xsl:choose>
              </Original_Currency>
              <Original_Currency_Amount>
                <xsl:value-of select="$lastInvoice/InvoiceAmount" />
              </Original_Currency_Amount>
              <CC_NO>
                <xsl:value-of select="$lastInvoice/AccountDisplayNumber" />
              </CC_NO>

              <!--
              Si nous sommes dans la situation suivante:

              Si Champ <Message><Order><WMProcessInfo><RefTextField5> = à "COD"
              Et si <Message><Order><Invoices><InvoiceDetail><InvoiceType> = à "Return".

              Uniquement si les 2 conditions sont respectés, il faut que ce champ soit:

              "CPN+N° commande (valeur dans OrderNumber"+n° de seq: 001, 002...
              -->

              <VOUCH_NO>
                <xsl:choose>
                  <xsl:when test="$refTextField5 = 'COD' and $lastInvoice/InvoiceType = 'Return'">
                    <xsl:value-of select="concat('CPN',../../OrderNumber)" />
                  </xsl:when>
                </xsl:choose>
              </VOUCH_NO>
            </PaymentDetail>
          </xsl:for-each>
        </Head>
      </SaleMessage>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>
