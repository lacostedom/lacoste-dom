<?xml version="1.0" encoding="utf-8"?>  
<xsl:transform version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"                             
                             xmlns:dw="http://www.demandware.com/xml/impex/pricebook/2006-10-31"
                             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                             xsi:noNamespaceSchemaLocation="D:\2011.2\Regression Smoke Interface\From AIM\Item Price\Import Item Price.xsd"
                             xmlns:xs="http://www.w3.org/2001/XMLSchema"
                             xmlns:fn="http://www.w3.org/2005/xpath-functions"
                             exclude-result-prefixes="xsl dw xs fn"> 
                             
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
<xsl:template match="/">	  
  
<tXML>
	<Header>
		<Source>CBR</Source>
		<Action_Type>update</Action_Type>
		<Reference_ID>
			<xsl:value-of select="substring-before(substring-after(substring-after(substring-after(substring-after(substring-after(tokenize(base-uri(.), '/')[last()],'_'),'_'),'_'),'_'),'_'),'_')"/>
		</Reference_ID>
		<Message_Type>Item_Pricing</Message_Type>
		<Company_ID>1</Company_ID>
		<Msg_Locale>
			<xsl:value-of select="format-dateTime(current-dateTime(),'[Y0001][M01][D01][H01][m01]')"/>
		</Msg_Locale>
		<Msg_Time_Zone>Europe/Paris</Msg_Time_Zone>
		<Internal_Reference_ID>
			<xsl:value-of select="concat('IP_',format-dateTime(current-dateTime(),'[Y0001][M01][D01][H01][m01]'))"/>   
		</Internal_Reference_ID>
	</Header>
	<Message>
		<!-- liste de tous les pricebook -->
		<ItemPriceList>	
		<xsl:for-each select="//dw:pricebooks/dw:pricebook">
			<!-- <xsl:variable name="v_pricebook_id" select="dw:header/@pricebook-id"/> -->
			<xsl:variable name="v_pricebook" select="."/>
			<xsl:variable name="v_pricebook_pays" select="substring(substring-after($v_pricebook/dw:header/@pricebook-id, '-'), 0, 3)"/>
			<xsl:variable name="v_pricebook_typeprix" select="substring-after(substring-after($v_pricebook/dw:header/@pricebook-id, '-'), '-')"/>
			
			<!-- DEBUG -->
			<!--
			<pricebook>
                <xsl:value-of select="$v_pricebook/dw:header/@pricebook-id"/>                
            </pricebook>
            <pricebook_pays>
				<xsl:value-of select="$v_pricebook_pays"/>                
            </pricebook_pays>
            <pricebook_typeprix>
				<xsl:value-of select="$v_pricebook_typeprix"/>
            </pricebook_typeprix>
			-->
			
			<!-- liste de tous les EAN d un pricebook -->
		
			<xsl:for-each select="dw:price-tables/dw:price-table">
				<xsl:variable name="v_product" select="."/>
							
					<ItemPrice>
						<PriceID>
							<xsl:value-of select="concat(concat(concat(concat($v_pricebook/dw:header/dw:description,'_'),$v_product/@product-id),'_'),$v_pricebook_pays)"/>
						</PriceID>
						<Item>
							<xsl:value-of select="$v_product/@product-id"/>
						</Item>
						<Price>
							<xsl:value-of select="$v_product/dw:amount"/>
						</Price>
						<CurrencyCode>ZWL</CurrencyCode>
						<Rank>
							<xsl:choose>
								<xsl:when test="string-length($v_pricebook_typeprix)=0">
									<xsl:value-of select="format-number(999999999 - number(translate(substring-before($v_product/dw:online-from,'T'),'-','')),'#')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="format-number(99999999 - number(translate(substring-before($v_product/dw:online-from,'T'),'-','')),'#')"/>
								</xsl:otherwise>	
							</xsl:choose>							
						</Rank>
						<EffectiveDTTM>
							<!--<xsl:value-of select="translate(translate($v_product/dw:online-from,'T',''),'-','')"/> -->
							<xsl:value-of select="concat(substring($v_product/dw:online-from,6,2),'/',substring($v_product/dw:online-from,9,2),'/',substring($v_product/dw:online-from,1,4),' ',substring($v_product/dw:online-from,12,8))"/>							
						</EffectiveDTTM>
						<ExpirationDTTM>
							<xsl:choose>
								<xsl:when test="string-length($v_product/dw:online-to)!=0">
									<!--<xsl:value-of select="translate(translate($v_product/dw:online-to,'T',''),'-','')"/>-->
									<xsl:value-of select="concat(substring($v_product/dw:online-to,6,2),'/',substring($v_product/dw:online-to,9,2),'/',substring($v_product/dw:online-to,1,4),' ',substring($v_product/dw:online-to,12,8))"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="'12/31/2999 00:00:00'"/>
								</xsl:otherwise>	
							</xsl:choose>
						</ExpirationDTTM>
						<BehaviorAfterEndDate>Ignore</BehaviorAfterEndDate>
						<StoreID>
							<xsl:if test="$v_pricebook_pays='at'">
								<xsl:value-of select="705"/>
							</xsl:if>
							<xsl:if test="$v_pricebook_pays='ch'">
								<xsl:value-of select="706"/>
							</xsl:if>
							<xsl:if test="$v_pricebook_pays='de'">
								<xsl:value-of select="703"/>
							</xsl:if>
							<xsl:if test="$v_pricebook_pays='dk'">
								<xsl:value-of select="709"/>
							</xsl:if>
							<xsl:if test="$v_pricebook_pays='fr'">
								<xsl:value-of select="702"/>
							</xsl:if>
							<xsl:if test="$v_pricebook_pays='gb'">
								<xsl:value-of select="704"/>
							</xsl:if>
							<xsl:if test="$v_pricebook_pays='ie'">
								<xsl:value-of select="716"/>
							</xsl:if>
							<xsl:if test="$v_pricebook_pays='pt'">
								<xsl:value-of select="717"/>
							</xsl:if>
							<xsl:if test="$v_pricebook_pays='se'">
								<xsl:value-of select="718"/>
							</xsl:if>
							<xsl:if test="$v_pricebook_pays='it'">
								<xsl:value-of select="760"/>
							</xsl:if>
							<xsl:if test="$v_pricebook_pays='be'">
								<xsl:value-of select="763"/>
							</xsl:if>
							<xsl:if test="$v_pricebook_pays='nl'">
								<xsl:value-of select="764"/>
							</xsl:if>
						</StoreID>
						<StoreID/>
					</ItemPrice>				
				</xsl:for-each>
			</xsl:for-each>
		</ItemPriceList>
	</Message>
<!--	
<Message>    
<xsl:variable name="v_devise" select="Message/dw:pricebooks/dw:pricebook/dw:header/dw:currency"/>
<xsl:variable name="v_catalog_name" select="Message/dw:pricebooks/dw:pricebook/dw:header/dw:display-name"/>
<ItemPriceList>
<xsl:for-each select="Message/dw:pricebooks/dw:pricebook/dw:price-tables/dw:price-table">
<ItemPrice>
<PriceID>
<xsl:if test="contains($v_catalog_name, 'Discount')">
<xsl:value-of select="concat('2-', @product-id)"/>
</xsl:if>
<xsl:if test="contains($v_catalog_name, 'FullPrice')">
<xsl:value-of select="concat('3-', @product-id)"/>
</xsl:if>
</PriceID>
<Item>
<xsl:value-of select="@product-id"/>
</Item>
<Price>
<xsl:value-of select="dw:amount"/>
</Price>
<CurrencyCode>
<xsl:value-of select="$v_devise"/>
</CurrencyCode>
<Rank>
<xsl:if test="contains($v_catalog_name, 'Discount')">
<xsl:value-of select="2"/>
</xsl:if>
<xsl:if test="contains($v_catalog_name, 'FullPrice')">
<xsl:value-of select="3"/>
</xsl:if>  
</Rank>
<EffectiveDTTM>
<xsl:value-of select="concat(substring(dw:online-from,6,2),'/',substring(dw:online-from,9,2),'/',substring(dw:online-from,3,2),' ',substring(dw:online-from,12,5))"/>             
</EffectiveDTTM>
<ExpirationDTTM>
<xsl:value-of select="concat(substring(dw:online-to,6,2),'/',substring(dw:online-to,9,2),'/',substring(dw:online-to,3,2),' ',substring(dw:online-to,12,5))"/>                
</ExpirationDTTM>


<BehaviorAfterEndDate>
<xsl:text>Ignore</xsl:text>
</BehaviorAfterEndDate>
</ItemPrice>
</xsl:for-each>
</ItemPriceList>
</Message>
-->
</tXML>
</xsl:template>
</xsl:transform>
