<?xml version="1.0" encoding="utf-8"?>
<xsl:transform version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:my="my:my">
	<xsl:output method="text" indent="yes"/>
	<xsl:template match="@* | node()">
	<!-- VARIABLES ORDER -->
		<xsl:variable name="OrderNumber" select="/tXML/Message/Order/OrderNumber"/>
		<xsl:variable name="CustomerId" select="/tXML/Message/Order/CustomerInfo/CustomerId"/>
		<xsl:variable name="ReferencesFields" select="/tXML/Message/Order/ReferenceFields"/>
		<xsl:for-each select="/tXML/Message/Order/Invoices/InvoiceDetail[InvoiceType='Shipment' and Published='false']">
			<!-- VARIABLES INVOICE -->
			<xsl:variable name="InvoiceCreationDTTM" select="InvoiceCreationDTTM"/>
			<xsl:variable name="ShipmentNbr" select="ShipmentNbr"/>
			
			<xsl:for-each select="InvoiceLines/InvoiceLineDetail">
				<!-- VARIABLES INVOICE LINE-->
				<xsl:variable name="Parent" select="ParentOrderLineNbr "/>
				<xsl:variable name="InvoiceNbr" select="InvoiceNbr"/>
				
				<xsl:if test="exists(/tXML/Message/Order/OrderLines/OrderLine[ShippingInfo/ShipVia='CLNC' and LineNumber = $Parent])">
					<xsl:variable name="orderline" select="/tXML/Message/Order/OrderLines/OrderLine[ShippingInfo/ShipVia='CLNC' and LineNumber = $Parent][1]"/>
					<xsl:variable name="GP_DATEPIECE">
						<xsl:call-template name="convertDOMDateFormat1">
							<!-- <xsl:with-param name="input" select="../../InvoiceCreationDTTM"/> -->
							<xsl:with-param name="DOMDateTime" select="$InvoiceCreationDTTM"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="GP_REFINTERNE">
						<xsl:call-template name="convertDOMDateFormat1">
							<xsl:with-param name="DOMDateTime" select="$InvoiceCreationDTTM"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="GP_REFINTERNE_2" select="concat('S', $OrderNumber)"/>
					<xsl:variable name="GP_REFINTERNE_3" select="concat('IO', $InvoiceNbr)"/>
					<xsl:variable name="CODEBARRE" select="ShippedItem"/>
					<xsl:variable name="QTESTOCK" select="InvoicedQuantity"/>
					<xsl:variable name="PRIXVTETOTAL">
						<xsl:choose>
							<xsl:when test="$orderline/PriceInfo/IsPriceOverridden = 'false'">
								<xsl:value-of select="format-number($orderline/PriceInfo/Price * $orderline/Quantity/OrderedQty,'0.##')"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="format-number($orderline/PriceInfo/OriginalPrice * $orderline/Quantity/OrderedQty,'0.##')"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:variable name="PRIXVTETOTREM" select="LineTotal"/>
					<xsl:variable name="MONTANTTAXE1" select="0"/>
					<xsl:variable name="TYPEREMISE" select="$orderline/DiscountDetails/DiscountDetail/ExtDiscountDetailId"/>
					<xsl:variable name="CDEECOMETAB">
						<xsl:choose>
							<xsl:when test="$orderline/OrderLineStatus = 'Shipped' ">
								<xsl:value-of select="$orderline/ShippingInfo/ShippingAddress[1]/ShipToFax"/>
							</xsl:when>
						</xsl:choose>
					</xsl:variable>
					<xsl:variable name="GP_REFSUIVI">
						<xsl:value-of select="/tXML/Message/Order/ShipmentDetails/ShipmentDetail/Cartons/Carton[ShipmentNbr =$ShipmentNbr]/TrackingNbr "/>
					</xsl:variable>
					<xsl:value-of select="concat('CDLC1_',';',$CustomerId,';',$GP_DATEPIECE,';',concat($GP_REFINTERNE, $GP_REFINTERNE_2, $GP_REFINTERNE_3),';',$OrderNumber,';',$CODEBARRE,';',$QTESTOCK,';',$PRIXVTETOTAL,';',$PRIXVTETOTREM,';',$MONTANTTAXE1,';',$ReferencesFields/ReferenceField5,';',$TYPEREMISE,';',$CDEECOMETAB,';',$GP_REFSUIVI,';')"/>
					<xsl:text>&#13;</xsl:text>
				</xsl:if>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="convertDOMDateFormat1">
		<xsl:param name="DOMDateTime"/>
		<xsl:if test="string-length($DOMDateTime) &gt; 15">
			<xsl:variable name="varDate">
				<xsl:value-of select="substring-before($DOMDateTime, ' ')"/>
			</xsl:variable>
			<xsl:variable name="varMonth">
				<xsl:if test="string-length(substring-before($varDate, '/')) &lt; 2">
					<xsl:value-of select="concat('0',substring-before($varDate, '/'))"/>
				</xsl:if>
				<xsl:if test="string-length(substring-before($varDate, '/')) = 2">
					<xsl:value-of select="substring-before($varDate, '/')"/>
				</xsl:if>
			</xsl:variable>
			<xsl:variable name="varDayYear">
				<xsl:value-of select="substring-after($varDate,'/')"/>
			</xsl:variable>
			<xsl:variable name="varDay">
				<xsl:if test="string-length(substring-before($varDayYear, '/')) &lt; 2">
					<xsl:value-of select="concat('0',substring-before($varDayYear, '/') )"/>
				</xsl:if>
				<xsl:if test="string-length(substring-before($varDayYear, '/')) = 2">
					<xsl:value-of select="substring-before($varDayYear, '/')"/>
				</xsl:if>
			</xsl:variable>
			<xsl:variable name="varYear">
				<xsl:choose>
					<xsl:when test="string-length(substring-after($varDayYear, '/')) = 2">
						<xsl:value-of select="concat('20',substring-after($varDayYear, '/'))"/>
					</xsl:when>
					<xsl:when test="string-length(substring-after($varDayYear, '/')) = 4">
						<xsl:value-of select="substring-after($varDayYear, '/')"/>
					</xsl:when>
				</xsl:choose>
			</xsl:variable>
			<xsl:value-of select="concat($varYear,$varMonth,$varDay)"/>
		</xsl:if>
	</xsl:template>
</xsl:transform>
