<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/">
		<tXML>
			<!-- Header -->
			<Header>
				<xsl:copy-of select="/tXML/Header/Source"/>
				<xsl:copy-of select="/tXML/Header/Action_Type"/>
				<xsl:copy-of select="/tXML/Header/Batch_id"/>
				<Message_Type>BRL30_PICKTICKET</Message_Type>
				<xsl:copy-of select="/tXML/Header/Company_ID"/>
			</Header>
			<!-- Message -->
			<Message>
				<!-- Transformations for Message node -->
				<!-- Get the date and time into SG&G format -->
				<xsl:variable name="myOrderedDate">
					<xsl:call-template name="convertDOMDateFormat_ToDate">
						<xsl:with-param name="input" select="/tXML/Message/DistributionOrder/OrderedDttm"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="myOrderedTime">
					<xsl:call-template name="convertDOMDateFormat_ToTime">
						<xsl:with-param name="input" select="/tXML/Message/DistributionOrder/OrderedDttm"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name = "PaymentMethod" select = "DistributionOrder/DsgShipVia" />
				<xsl:copy-of select="/tXML/Message/DistributionOrder/DistributionOrderId"/>
				<xsl:copy-of select="/tXML/Message/DistributionOrder/OrderType"/>				
				<OrderedDttm>
					<xsl:value-of select="concat($myOrderedDate, ' ', $myOrderedTime)"/>
				</OrderedDttm>
				<xsl:copy-of select="/tXML/Message/DistributionOrder/MonetaryValue"/>
				<xsl:copy-of select="/tXML/Message/DistributionOrder/SalesOrderNbr"/>
				<CustomerName>
					<xsl:value-of select="/tXML/Message/DistributionOrder/DestinationContactName"/>
				</CustomerName>
				<DestinationAddressStreetNumber></DestinationAddressStreetNumber>
				<!--<DestinationAddressStreetNumber><xsl:value-of select="substring-before(/tXML/Message/DistributionOrder/DestinationAddressLine1,',')"/></DestinationAddressStreetNumber>
				<DestinationAddressLine1><xsl:value-of select="substring-after(/tXML/Message/DistributionOrder/DestinationAddressLine1,',')"/></DestinationAddressLine1>-->
				<xsl:copy-of select="/tXML/Message/DistributionOrder/DestinationAddressLine1"/>
				<xsl:copy-of select="/tXML/Message/DistributionOrder/DestinationAddressLine2"/>
				<xsl:copy-of select="/tXML/Message/DistributionOrder/DestinationCounty"/>
				<xsl:copy-of select="/tXML/Message/DistributionOrder/DestinationCity"/>
				<xsl:copy-of select="/tXML/Message/DistributionOrder/DestinationCountry"/>
				<xsl:copy-of select="/tXML/Message/DistributionOrder/DestinationStateOrProvince"/>
				<xsl:copy-of select="/tXML/Message/DistributionOrder/DestinationPostalCode"/>
				<xsl:copy-of select="/tXML/Message/DistributionOrder/DestinationContactTelephoneNbr"/>
				<xsl:copy-of select="/tXML/Message/DistributionOrder/DestinationContactEmailAddress"/>				
				<xsl:copy-of select="/tXML/Message/DistributionOrder/BillToName"/>
				<BillToAddressStreetNumber></BillToAddressStreetNumber>
				<!--<BillToAddressStreetNumber><xsl:value-of select="substring-before(/tXML/Message/DistributionOrder/BillToAddressLine1,',')"/></BillToAddressStreetNumber>
				<BillToAddressLine1><xsl:value-of select="substring-after(/tXML/Message/DistributionOrder/BillToAddressLine1,',')"/></BillToAddressLine1>-->
				<xsl:copy-of select="/tXML/Message/DistributionOrder/BillToAddressLine1"/>
				<xsl:copy-of select="/tXML/Message/DistributionOrder/BillToAddressLine2"/>
				<xsl:copy-of select="/tXML/Message/DistributionOrder/BillToCounty"/>
				<xsl:copy-of select="/tXML/Message/DistributionOrder/BillToCity"/>
				<xsl:copy-of select="/tXML/Message/DistributionOrder/BillToCountryCode"/>
				<xsl:copy-of select="/tXML/Message/DistributionOrder/BillToStateOrProv"/>
				<xsl:copy-of select="/tXML/Message/DistributionOrder/BillToPostalCode"/>				
				<BillToCPF><xsl:value-of select="/tXML/Message/DistributionOrder/ProcessInfo/RefTextField6"/></BillToCPF>											
				<xsl:copy-of select="/tXML/Message/DistributionOrder/OriginFacilityAliasId"/>
				<xsl:copy-of select="/tXML/Message/DistributionOrder/OriginFacilityName"/>
				<xsl:copy-of select="/tXML/Message/DistributionOrder/DsgShipVia"/>
				<xsl:for-each select="/tXML/Message/DistributionOrder/Comment">
					<xsl:call-template name="convertVAS">
						<xsl:with-param name="CommentText" select="CommentText"/>
					</xsl:call-template>
				</xsl:for-each>
				<xsl:for-each select="/tXML/Message/DistributionOrder/LineItem">
					<LineItem>
						<xsl:copy-of select="DoLineNbr"/>
						<xsl:copy-of select="AllocationSourceType"/>
						<ItemName>
							<!--<xsl:value-of select="concat(substring-before(ItemName,'-'), '-', InventoryAttributes/ItemAttribute1, substring-after(ItemName,'-'))"/>-->
							<xsl:value-of select="ItemName"/>
						</ItemName>
						<xsl:copy-of select="Description"/>
						<xsl:copy-of select="TotalMonetaryValue"/>						
						<Quantity>
							<xsl:value-of select="Quantity/OrderQty"/>
						</Quantity>
					</LineItem>
				</xsl:for-each>
			</Message>
		</tXML>
	</xsl:template>
	<!-- TEMPLATE - Get a YYYY-MM-DD date -->
	<xsl:template name="convertDOMDateFormat_ToDate">
		<xsl:param name="input"/>
		<xsl:analyze-string select="$input" regex="^(\d+)/(\d+)/(\d+) (.*?)$">
			<xsl:matching-substring>
				<xsl:variable name="yearReg" select="xs:string(regex-group(3))"/>
				<xsl:variable name="year">
					<xsl:choose>
						<xsl:when test="string-length($yearReg) = 2">
							<xsl:value-of select="concat('20', $yearReg)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$yearReg"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="month_Reg" select="xs:string(regex-group(1))"/>
				<xsl:variable name="month">
					<xsl:choose>
						<xsl:when test="string-length($month_Reg) = 1">
							<xsl:value-of select="concat('0', $month_Reg)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$month_Reg"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="dayReg" select="xs:string(regex-group(2))"/>
				<xsl:variable name="day">
					<xsl:choose>
						<xsl:when test="string-length($dayReg) = 1">
							<xsl:value-of select="concat('0', $dayReg)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$dayReg"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="dateFormatted" select="concat($year, '-', $month, '-', $day)"/>
				<xsl:value-of select="$dateFormatted"/>
			</xsl:matching-substring>
			<xsl:non-matching-substring>
				<xsl:value-of select="1900-01-01"/>
			</xsl:non-matching-substring>
		</xsl:analyze-string>
	</xsl:template>
	<!-- TEMPLATE - Get a HH24:MI time -->
	<xsl:template name="convertDOMDateFormat_ToTime">
		<xsl:param name="input"/>
		<xsl:analyze-string select="$input" regex="^(.+) (\d):(\d)(.*?)$">
			<xsl:matching-substring>
				<xsl:variable name="hoursReg" select="xs:string(regex-group(1))"/>
				<xsl:variable name="hours">
					<xsl:choose>
						<xsl:when test="string-length($hoursReg) = 1">
							<xsl:value-of select="concat('0', $hoursReg)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$hoursReg"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="minutesReg" select="xs:string(regex-group(2))"/>
				<xsl:variable name="minutes">
					<xsl:choose>
						<xsl:when test="string-length($hoursReg) = 1">
							<xsl:value-of select="concat('0', $hoursReg)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$hoursReg"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="timeFormatted" select="concat($hours, ':', $minutes)"/>
				<xsl:value-of select="$timeFormatted"/>
			</xsl:matching-substring>
			<xsl:non-matching-substring>
				<xsl:value-of select="'00:00'"/>
			</xsl:non-matching-substring>
		</xsl:analyze-string>
	</xsl:template>
	<xsl:template name="convertVAS">
		<xsl:param name="CommentText"/>
		<xsl:choose>
			<xsl:when test="starts-with($CommentText,'(Header) VAS Gift Message:')">
				<Comment>
					<NoteType>GB</NoteType>
					<CommentText>
						<xsl:value-of select="'Gift Box'"/>
					</CommentText>
				</Comment>
				<xsl:if test="substring-after($CommentText,'(Header) VAS Gift Message:')!='G1b2AzERTy'">
					<Comment>
						<NoteType>GM</NoteType>
						<CommentText>
							<xsl:value-of select="substring-after($CommentText,'(Header) VAS Gift Message:')"/>
						</CommentText>
					</Comment>
				</xsl:if>
			</xsl:when>
			<xsl:when test="starts-with($CommentText,'Loyalty Status:')">
				<Comment>
					<NoteType>LY</NoteType>
					<CommentText>
						<xsl:value-of select="substring-after($CommentText,'Loyalty Status:')"/>
					</CommentText>
				</Comment>
			</xsl:when>
			<xsl:when test="starts-with($CommentText,'Delivery Instrns.')">
				<Comment>
					<NoteType>SI</NoteType>
					<CommentText>
						<xsl:value-of select="substring-after($CommentText,'Delivery Instrns.')"/>
					</CommentText>
				</Comment>
			</xsl:when>
			<xsl:when test="starts-with($CommentText,'Gift Wrap')">
				<Comment>
					<NoteType>GB</NoteType>
					<CommentText>
						<xsl:value-of select="'Gift Box'"/>
					</CommentText>
				</Comment>
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
