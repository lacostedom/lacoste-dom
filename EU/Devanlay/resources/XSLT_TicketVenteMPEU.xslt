<?xml version="1.0" encoding="UTF-8"?>
<!--
Fichier de transformation C21 - Sales Posting Europe

Auteur      Date      	Commentaire
LDN         02/2016   	Création
QMT        08/2016		Modification (ajout de GP_ETABLISSEMENT et GP_LIBREPIECE3 ainsi qu'une nouvelle partie Intégration des annonces de livraison point relais
-->
<xsl:transform version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:my="my:my">
	<xsl:output method="text" indent="yes"/>
	<xsl:param name="separateur" select="'/'"/>
	<xsl:template match="/">
		<xsl:for-each select="tXML/Message/Order">
			<!--  **************************************************
            ************** Génération CLIENT *****************
            ************************************************** -->
		<!-- -->
			<!--   ******************************************************************************
            ************** Génération FFO (vente, exped, retour)   *****************
            ****************************************************************************** -->
			<xsl:variable name="ReferenceField3" select="ReferenceFields/ReferenceField3"/>
			<!-- GP_REGIMETAXE -->
			<xsl:variable name="ReferenceField5" select="ReferenceFields/ReferenceField5"/>
			<!-- $$_DEVISE -->
			<xsl:variable name="CustomerId">
				<xsl:choose>
					<xsl:when test="OrderType = 'MKP'">
						<xsl:choose>
							<xsl:when test="/tXML/Message/Order/OrderLines/OrderLine[1]/ShippingInfo/ShippingAddress/ShipToCountry='FR'">
								<xsl:value-of select="'GestFR'"/>
							</xsl:when>
							<xsl:when test="/tXML/Message/Order/OrderLines/OrderLine[1]/ShippingInfo/ShippingAddress/ShipToCountry='DE'">
								<xsl:value-of select="'GestDE'"/>
							</xsl:when>
							<xsl:when test="/tXML/Message/Order/OrderLines/OrderLine[1]/ShippingInfo/ShippingAddress/ShipToCountry='GB'">
								<xsl:value-of select="'GestGB'"/>
							</xsl:when>
							<xsl:when test="/tXML/Message/Order/OrderLines/OrderLine[1]/ShippingInfo/ShippingAddress/ShipToCountry='CH'">
								<xsl:value-of select="'GestCH'"/>
							</xsl:when>
							<xsl:when test="/tXML/Message/Order/OrderLines/OrderLine[1]/ShippingInfo/ShippingAddress/ShipToCountry='IT'">
								<xsl:value-of select="'GestIT'"/>
							</xsl:when>
							<xsl:when test="/tXML/Message/Order/OrderLines/OrderLine[1]/ShippingInfo/ShippingAddress/ShipToCountry='ES'">
								<xsl:value-of select="'GestES'"/>
							</xsl:when>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="CustomerInfo/CustomerId"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<!-- GP_TIERS (vente) -->
			<xsl:variable name="OrderNumber" select="OrderNumber"/>
			<!-- GP_REFEXTERNE (vente)-->
			<xsl:variable name="EnteredLocation" select="../Order[EntryType='S2W']/EnteredLocation"/>
			<!-- GP_LIBREPIECE1-->
			<xsl:variable name="EnteredBy" select="../Order[EntryType='S2W']/EnteredBy"/>
			<!-- $$_REPRESENTANT-->
			<xsl:variable name="GP_ETABLISSEMENT">
				<xsl:choose>
					<xsl:when test="OrderType = 'SFS' ">
						<xsl:value-of select="OrderLines/OrderLine[OrderLineStatus='Shipped'][1]/AllocationDetails/AllocationDetail[1]/OriginFacilityAliasID"/>
					</xsl:when>
					<xsl:when test="OrderType = 'R1H'">
						<xsl:value-of select="OrderLines/OrderLine[OrderLineStatus='Picked Up'][1]/AllocationDetails/AllocationDetail[1]/OriginFacilityAliasID"/>
					</xsl:when>
				<!-- Boutique Marketplace : THA 03/2017 -->
					<xsl:when test="OrderType = 'MKP'">
						<xsl:value-of select="/tXML/Message/Order/PaymentDetails/PaymentDetail[1]/ReferenceFields/ReferenceField4"/>
					</xsl:when>
				<!-- -->
					<xsl:otherwise>
						<xsl:value-of select="708"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="GP_LIBREPIECE3">
				<xsl:choose>
					<xsl:when test="EntryType = 'S2W'">
						<xsl:value-of select="OrderLines/OrderLine[OrderLineStatus='Shipped'][1]/ShippingInfo/ShippingAddress[1]/ShipToFax"/>
					</xsl:when>
					<xsl:otherwise/>
				</xsl:choose>
			</xsl:variable>
			<!--  *******************************************************************
            ************** Génération FFO vente et Expédition *********
            ******************************************************************* -->
			<xsl:for-each select="Invoices/InvoiceDetail[InvoiceType='Shipment' and Published='false']">
				<!--and ParentNbr=$OrderNumber]"-->
				<xsl:variable name="InvoiceNbr" select="InvoiceNbr"/>
				<xsl:variable name="DatePiece">
					<!--GP_DATEPIECE -->
					<xsl:call-template name="convertDOMDateFormat1">
						<xsl:with-param name="DOMDateTime" select="InvoiceCreationDTTM"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="RefInterne" select="concat($DatePiece, ' S', $OrderNumber, ' IO', $InvoiceNbr)"/>
				<xsl:for-each select="InvoiceLines/InvoiceLineDetail">
					<xsl:variable name="ShippedItem" select="ShippedItem"/>
					<!--$$_CODEBARRE FFO Vente-->
					<xsl:variable name="InvoicedQuantity" select="InvoicedQuantity"/>
					<!--$$_QTESTOCK -->
					<xsl:variable name="InvoicedQuantityAbs">
						<!--Pilotage de Qté négative (par ex pour les retours)-->
						<xsl:if test="contains($InvoicedQuantity,'-')">
							<xsl:value-of select="$InvoicedQuantity*-1"/>
						</xsl:if>
						<xsl:if test="not(contains($InvoicedQuantity,'-'))">
							<xsl:value-of select="$InvoicedQuantity"/>
						</xsl:if>
					</xsl:variable>
					<xsl:variable name="LineTotal" select="LineTotal"/>
					<xsl:variable name="ParentOrderLineNbr" select="ParentOrderLineNbr"/>
					<!-- Champ permettant la jointure entre Invoice et OrderLine -->
					<xsl:for-each select="../../../../OrderLines/OrderLine[LineNumber=$ParentOrderLineNbr]">
						<!--le XML renvoie une chaine true ou false mais au format string, donc l'affectation de la variable se fait en testant la chaine 
                avec l'égalité true ou false, ainsi on pourra tester un boolean-->
						<!--<xsl:variable name="IsPriceOverridden" select="/tXML/Message/Order/OrderLines/OrderLine[LineNumber=$ParentOrderLineNbr]/PriceInfo/IsPriceOverridden='true'"/>-->
						<xsl:variable name="IsPriceOverridden" select="PriceInfo/IsPriceOverridden='true'"/>
						<xsl:variable name="Price">
							<!--$$-PRIXVTETOTALTTC FFO Vente-->
							<xsl:if test="not($IsPriceOverridden)">
								<xsl:value-of select="PriceInfo/Price*$InvoicedQuantityAbs"/>
							</xsl:if>
							<xsl:if test="$IsPriceOverridden">
								<xsl:value-of select="PriceInfo/OriginalPrice*$InvoicedQuantityAbs"/>
							</xsl:if>
						</xsl:variable>
						<xsl:variable name="Discount" select="/tXML/Message/Order/OrderLines/OrderLine[LineNumber=$ParentOrderLineNbr]/DiscountDetails[1]/DiscountDetail[1]/ExtDiscountDetailId"/>
						<!--$$_TYPEREMISE FFO vente-->
						<!--Ecriture d'1 ligne de détail FFO Vente-->
						<xsl:value-of select="translate(concat('ECSC1_',';',$ReferenceField3,';',$CustomerId,';',$OrderNumber,';',$RefInterne,';',$DatePiece,';',$ShippedItem,';',$InvoicedQuantityAbs,';',$Price,';',$Discount,';',$ReferenceField5,';',$LineTotal,';',$EnteredLocation,';',$EnteredBy,';', $GP_ETABLISSEMENT, ';', $GP_LIBREPIECE3, ';'),'&amp;',',')"/>
						<xsl:text>&#13;</xsl:text>
					</xsl:for-each>
				</xsl:for-each>
				<!-- InvoiceLines/InvoiceLineDetail -->
				<!--  **************************************************************************
					*********** Génération FFO appeasement (before shipping) ******
					**************************************************************************	-->
				
				<xsl:if test="exists(DiscountDetails/DiscountDetail[DiscountType='Appeasement'])">
					<xsl:variable name="AppeasementValue" select="sum(DiscountDetails/DiscountDetail[DiscountType='Appeasement']/DiscountAmount)*-1"/>
					<xsl:variable name="AppeasementName">
						<xsl:call-template name="AppeasementCode">
							<xsl:with-param name="Appeas" select="DiscountDetails/DiscountDetail[DiscountType='Appeasement']/DiscountType"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:value-of select="	concat('ECSC1_',';',$ReferenceField3,';',$CustomerId,';',$OrderNumber,';',$RefInterne,';',$DatePiece,';',$AppeasementName,';',-1,';',$AppeasementValue,';','',';',$ReferenceField5,';',$AppeasementValue,';',$EnteredLocation,';',$EnteredBy,';')"/>
					<xsl:text>&#13;</xsl:text>
				</xsl:if>
				
				<!-- ******************************************************************
                  ********** Génération FFO Frais d'expédition ***************
                  ****************************************************************** 
                  *** Initialement le flux FFO Exped était piloté APRES les FFO ventes (comme tous les flux SalesPosting, mais l'import CBR impose d'avoir chaque ligne d'exped 
                      après l'invoice concerné, donc obligation d'insérer le flux Exped après chaque ligne de vente en jointant sur l'InvoiceNbr ***
              -->
				<!--Ecriture d'1 ligne de détail FFO frais d'expédition-->
				
				<!-- Frais d'expedition Marketplace : THA 03/2017 -->
				<xsl:if test="substring(ParentNbr,1,1) = 'A'">
				
					<xsl:variable name="ParentOrderLineNbr" select="InvoiceLines/InvoiceLineDetail[1]/ParentOrderLineNbr"/>
					<!--$$_CODEBARRE FFO frais expédition-->
					<!--Intégration des frais d'expédition: Récupération d'un UPC particulier en fct du ShipVia envoyé-->
					<xsl:variable name="UPCSpecialExped">
						<xsl:call-template name="UPCSpecial">
							<xsl:with-param name="ShipVia" select="/tXML/Message/Order/OrderLines/OrderLine[LineNumber=$ParentOrderLineNbr]/ShippingInfo/ShipVia"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:variable name="UPCSpecialExped" select="concat('FP',/tXML/Message/Order/OrderLines/OrderLine[LineNumber=$ParentOrderLineNbr]/ShippingInfo/ShipVia)"/>
					<!--$$_PRIXVTETOTALTTC frais d'expédition-->
					<xsl:variable name="PriceExped" select="format-number(sum(/tXML/Message/Order/ChargeDetails/ChargeDetail[ChargeCategory='Shipping']/UnitCharge),'0.##')"/>
					<!--$$_TYPEREMISE frais expédition -->
					<xsl:variable name="DiscountExped" select="/tXML/Message/Order/DiscountDetails/DiscountDetail[starts-with(ExtDiscountDetailId,'S')]/ExtDiscountDetailId"/>
					<!--$$_PRIXVTETOTTTCREM frais d'expédition-->
					<xsl:variable name="LineSubTotalExped" select="format-number(sum(ChargeDetails/ChargeDetail[ChargeCategory='Shipping']/ChargeAmount),'0.##')"/>
					<xsl:value-of select="translate(concat('ECSC1_',';',$ReferenceField3,';',$CustomerId,';',$OrderNumber,';',$RefInterne,';',$DatePiece,';',$UPCSpecialExped,';',1,';',$PriceExped,';',$DiscountExped,';',$ReferenceField5,';',$LineSubTotalExped,';',$EnteredLocation,';',$EnteredBy,';', $GP_ETABLISSEMENT, ';', $GP_LIBREPIECE3,  ';'),'&amp;',',')"/>
					<xsl:text>&#13;</xsl:text>
				
				</xsl:if>
				
			</xsl:for-each>
			<!-- Invoices/InvoiceDetail -->
			<!--   *****************************************************
            ************** Fin FFO vente *********************
            ***************************************************** -->
			<!--  *****************************************************
            ************** Génération FFO retour ***********
            ***************************************************** -->
			<xsl:for-each select="Invoices/InvoiceDetail[InvoiceType='Return' and Published='false']">
				<xsl:variable name="OrderNumberR" select="ReferenceNbr"/>
				<!-- GP_REFEXTERNE (return) -->
				<xsl:variable name="InvoiceNbrR" select="InvoiceNbr"/>
				<xsl:variable name="ParentNbr" select="ParentNbr"/>
				<xsl:variable name="InvoiceCreationDTTMR">
					<!--GP_DATEPIECE -->
					<xsl:call-template name="convertDOMDateFormat1">
						<xsl:with-param name="DOMDateTime" select="InvoiceCreationDTTM"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="Demarque" select="''"/>
				<xsl:variable name="RefInterneR" select="concat($InvoiceCreationDTTMR,' R', $OrderNumberR,' IO', $InvoiceNbrR)"/>
				<!-- GP_REFINTERNE -->
				<xsl:for-each select="InvoiceLines/InvoiceLineDetail">
					<xsl:variable name="ReturnDiscount">
						<xsl:if test="DiscountDetails/DiscountDetail/ExtDiscountDetailId='CS'">
							<discount code="{DiscountDetails/DiscountDetail[ExtDiscountDetailId='CS']/ExtDiscountDetailId}" value="{format-number(LineTotal,'#0.00')}"/>
						</xsl:if>
					</xsl:variable>
					<xsl:value-of select="$ReturnDiscount"/>
					<xsl:variable name="ShippedItem" select="ShippedItem"/>
					<!--$$_CODEBARRE -->
					<xsl:variable name="InvoicedQuantity" select="InvoicedQuantity"/>
					<!--$$_QTESTOCK -->
					<!--FFO retour : Pilotage de Qté négative (On doit envoyer une valeur négative sytématiquement pour les retours)-->
					<xsl:variable name="InvoicedQuantityNeg">
						<xsl:if test="contains($InvoicedQuantity,'-')">
							<xsl:value-of select="$InvoicedQuantity"/>
						</xsl:if>
						<xsl:if test="not(contains($InvoicedQuantity,'-'))">
							<xsl:value-of select="$InvoicedQuantity*-1"/>
						</xsl:if>
					</xsl:variable>
					<xsl:variable name="LineSubTotal" select="LineSubTotal"/>
					<xsl:variable name="LineSubTotalNeg">
						<xsl:if test="contains($LineSubTotal,'-')">
							<xsl:value-of select="$LineSubTotal"/>
						</xsl:if>
						<xsl:if test="not(contains($LineSubTotal,'-'))">
							<xsl:value-of select="$LineSubTotal*-1"/>
						</xsl:if>
					</xsl:variable>
					<!--Ecriture d'1 ligne de détail FFO Retour-->
					<xsl:value-of select="concat('ECSC1_',';',$ReferenceField3,';',$CustomerId,';',$ParentNbr,';',$RefInterneR,';',$InvoiceCreationDTTMR,';',OrderedItem,';',$InvoicedQuantityNeg,';',format-number($LineSubTotalNeg,'#0.00'),';',$Demarque,';',$ReferenceField5,';',format-number(LineTotal,'#0.00'),';',$EnteredLocation,';',$EnteredBy,';',$GP_ETABLISSEMENT)"/>
					<xsl:text>&#13;</xsl:text>
				</xsl:for-each>
				<!-- InvoiceLines/InvoiceLineDetail -->
			</xsl:for-each>
			<!--PILOTAGE FFO retour-->
			<!--   *****************************************************
            ************** Fin FFO Retour *******************
            ***************************************************** -->
			<!--  ******************************************************
            ************** Génération Appeasement *********
            ****************************************************** -->
			
			<xsl:for-each select="Invoices/InvoiceDetail[InvoiceType='Adjustment' and Published='false' and string-length(translate(substring(ParentNbr,1,3),'ABCDEFGHIJKLMNOPQRSTUVWXYZ', '')) > 0]">
				<!--and ParentNbr=$OrderNumber]"-->
				<xsl:variable name="InvoiceNbr" select="InvoiceNbr"/>
				<!--<xsl:variable name="RefInterne" select="concat($dateCreation, ' OD', $OrderNumber, ' IO', $InvoiceNbr)"/>-->
				<xsl:variable name="InvoiceCreationDTTM">
					<xsl:call-template name="convertDOMDateFormat1">
						<xsl:with-param name="DOMDateTime" select="InvoiceCreationDTTM"/>
						<!--GP_DATEPIECE -->
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="RefInterne" select="concat($InvoiceCreationDTTM, ' A', $OrderNumber, ' IO', $InvoiceNbr)"/>
				<xsl:variable name="HeaderDiscounts" select="HeaderDiscounts"/>
				<!--$$_PRIXVTETOTALTTC et $$_PRIXVTETOTTTCREM -->
				<!--FFO appeasement : Pilotage de Qté négative (On doit envoyer une valeur négative sytématiquement pour les appeasements)-->
				<xsl:variable name="HeaderDiscountsNeg">
					<xsl:if test="contains($HeaderDiscounts,'-')">
						<xsl:value-of select="$HeaderDiscounts"/>
					</xsl:if>
					<xsl:if test="not(contains($HeaderDiscounts,'-'))">
						<xsl:value-of select="$HeaderDiscounts*-1"/>
					</xsl:if>
				</xsl:variable>
				<!--Ecriture ligne d'entête-->
				<xsl:for-each select="DiscountDetails/DiscountDetail[DiscountType='Appeasement' or DiscountType='Shipping appeasement' or DiscountType='RMA Fees']">
					<xsl:variable name="AppeasementName">
						<!--$$_CODEBARRE -->
						<xsl:call-template name="AppeasementCode">
							<xsl:with-param name="Appeas" select="DiscountType"/>
						</xsl:call-template>
					</xsl:variable>
					<xsl:value-of select="concat('ECSC1_',';',$ReferenceField3,';',$CustomerId,';',$OrderNumber,';',$RefInterne,';',$InvoiceCreationDTTM,';',$AppeasementName,';',-1,';',$HeaderDiscountsNeg,';','',';',$ReferenceField5,';',$HeaderDiscountsNeg,';',$EnteredLocation,';',$EnteredBy,';', $GP_ETABLISSEMENT,';')"/>
					<xsl:text>&#13;</xsl:text>
				</xsl:for-each>

				<!--Ecriture ligne de détail-->
				<xsl:for-each select="InvoiceLines/InvoiceLineDetail">
					<xsl:for-each select="DiscountDetails/DiscountDetail[DiscountType='Appeasement' or DiscountType='Shipping appeasement' or DiscountType='RMA Fees']">
						<xsl:variable name="AppeasementName">
							<xsl:call-template name="AppeasementCode">
								<xsl:with-param name="Appeas" select="DiscountType"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:value-of select="concat('ECSC1_',';',$ReferenceField3,';',$CustomerId,';',$OrderNumber,';',$RefInterne,';',$InvoiceCreationDTTM,';',$AppeasementName,';',-1,';',$HeaderDiscountsNeg,';','',';',$ReferenceField5,';',$HeaderDiscountsNeg,';',$EnteredLocation,';',$EnteredBy,';', $GP_ETABLISSEMENT, ';')"/>
						<xsl:text>&#13;</xsl:text>
					</xsl:for-each>
				</xsl:for-each>
			</xsl:for-each>
			
			<!-- Invoices/InvoiceDetail -->
			<!--   ******************************************************
            ************** FIN Génération Appeasement ****
            ****************************************************** -->
			<!--  *******************************************************************************
            ************** Génération Détail de paiement (par les Invoice) *********
            ******************************************************************************* -->
			
			<!-- pas d'appeasement avant shipping pour les commandes marketplace pour ne pas déclencher de remboursement en cas d'annulation de ligne à cause des commandes prepaid -->
			
			<xsl:for-each select="Invoices/InvoiceDetail[Published='false' 
												and (InvoiceType='Shipment' or (InvoiceType='Adjustment' and string-length(translate(substring(ParentNbr,1,3),'ABCDEFGHIJKLMNOPQRSTUVWXYZ', '')) > 0) or InvoiceType='Return')]">
				<!--and ParentNbr=/tXML/Message/Order/OrderNumber -->
				<xsl:variable name="DateEche">
					<xsl:call-template name="convertDOMDateFormat1">
						<!--GPE_DATEECHE (On ramène ici la date de la facture, obtenue dans la balise "InvoiceNumber du noeud courant")-->
						<xsl:with-param name="DOMDateTime" select="InvoiceCreationDTTM"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="InvoiceNbr" select="InvoiceNbr"/>
				<xsl:variable name="OrderNumberEche">
					<xsl:if test="InvoiceType='Shipment'">
						<xsl:value-of select="ParentNbr"/>
					</xsl:if>
					<xsl:if test="InvoiceType='Adjustment'">
						<xsl:value-of select="ParentNbr"/>
					</xsl:if>
					<xsl:if test="InvoiceType='Return'">
						<xsl:value-of select="ReferenceNbr"/>
					</xsl:if>
				</xsl:variable>
				<xsl:variable name="TypeRefInterne">
					<xsl:if test="InvoiceType='Shipment'">
						<xsl:value-of select="' S'"/>
					</xsl:if>
					<xsl:if test="InvoiceType='Adjustment'">
						<xsl:value-of select="' A'"/>
					</xsl:if>
					<xsl:if test="InvoiceType='Return'">
						<xsl:value-of select="' R'"/>
					</xsl:if>
				</xsl:variable>
				<xsl:variable name="signe">
					<!--pour piloter si le montant doit être positif (hors return) ou négatif (sur return)-->
					<xsl:if test="InvoiceType='Return'">
						<xsl:value-of select="'-'"/>
					</xsl:if>
					<xsl:if test="InvoiceType!='Return'">
						<xsl:value-of select="''"/>
					</xsl:if>
				</xsl:variable>
				<!-- On ne filtre plus sur les TransactionDecision='Success', mais on prend tout et on concatenera le type de carte avec le statut : FSE 25/02-->
				<!--				<xsl:for-each select="/tXML/Message/Order/PaymentDetails/PaymentDetail[PaymentTransactionDetails/PaymentTransactionDetail[TransactionDecision='Success']/InvoiceList/Invoice/InvoiceNumber=$InvoiceNbr or PaymentTransactionDetails/PaymentTransactionDetail[TransactionDecision='Success']/InvoiceNumber=$InvoiceNbr]">-->
				<xsl:for-each select="/tXML/Message/Order/PaymentDetails/PaymentDetail[PaymentTransactionDetails/PaymentTransactionDetail/InvoiceList/Invoice/InvoiceNumber=$InvoiceNbr or PaymentTransactionDetails/PaymentTransactionDetail/InvoiceNumber=$InvoiceNbr]">
					<xsl:variable name="posit" select="position()"/>
					<xsl:variable name="PaymentMethodCBRdirect" select="PaymentMethod"/>
					<xsl:variable name="CardTypeCBRdirect">
						<xsl:if test="PaymentMethod != 'Others'">
							<xsl:value-of select="substring(CardType,1,3)"/>
						</xsl:if>
					</xsl:variable>
				<!-- Moyen de paiement Marketplace : THA 03/2017 Set ZAL to every MKP payment-->
					<xsl:variable name="PaiementMarketplace">
						<xsl:choose>
							<xsl:when test="/tXML/Message/Order/ReferenceFields/ReferenceField3 = '829' or /tXML/Message/Order/ReferenceFields/ReferenceField3 = '820' or /tXML/Message/Order/ReferenceFields/ReferenceField3 = '821' or /tXML/Message/Order/ReferenceFields/ReferenceField3 = '822'">
								<xsl:value-of select="'ZAL'"/>
							</xsl:when>
							<xsl:when test="/tXML/Message/Order/ReferenceFields/ReferenceField3 = '823' or /tXML/Message/Order/ReferenceFields/ReferenceField3 = '824' or /tXML/Message/Order/ReferenceFields/ReferenceField3 = '825' or /tXML/Message/Order/ReferenceFields/ReferenceField3 = '826' or /tXML/Message/Order/ReferenceFields/ReferenceField3 = '827'">
								<xsl:value-of select="'ZAL'"/>
							</xsl:when>
							<xsl:when test="/tXML/Message/Order/ReferenceFields/ReferenceField3 = '828'">
								<xsl:value-of select="'ZAL'"/>
							</xsl:when>
						</xsl:choose>
					</xsl:variable>
				<!-- -->
					<xsl:variable name="RefInterne" select="concat($DateEche,$TypeRefInterne, $OrderNumberEche, ' IO', $InvoiceNbr)"/>
					<xsl:variable name="NameAsOnCard" select="upper-case(NameAsOnCard)"/>
					<xsl:variable name="NumCard" select="substring(AccountDisplayNumber,string-length(AccountDisplayNumber)-3)"/>
					<xsl:variable name="MonthExpiration" select="CardExpiryMonth"/>
					<xsl:variable name="YearExpiration" select="CardExpiryYear"/>
					<xsl:variable name="ExpirationDate" select="concat($MonthExpiration,'/',$YearExpiration)"/>
					<!--					<xsl:for-each select="PaymentTransactionDetails/PaymentTransactionDetail[TransactionDecision='Success']/InvoiceList/Invoice[InvoiceNumber=$InvoiceNbr]">-->
					<xsl:for-each select="PaymentTransactionDetails/PaymentTransactionDetail/InvoiceList/Invoice[InvoiceNumber=$InvoiceNbr]">
						<xsl:variable name="TokenId" select="../../RequestId"/>
						<xsl:variable name="NameOnCard" select="concat(upper-case($NameAsOnCard), ' -- ',$TokenId)"/>
						<xsl:variable name="ProcessedAmount" select="format-number(InvoiceTransactionAmount,'0.##')"/>
						<xsl:variable name="TransactionDecision" select="../../TransactionDecision"/>
						<xsl:variable name="GPE_MODEPAIE">
							<xsl:choose>
								<xsl:when test="/tXML/Message/Order/OrderType = 'R1H' or /tXML/Message/Order/OrderType = 'SFS' ">
									<!--<xsl:value-of select="'Paiement en attente'"/>-->
									<xsl:value-of select="concat('W2S',$PaymentMethodCBRdirect,$TransactionDecision)"/>
								</xsl:when>
							<!-- Moyen de paiement Marketplace : THA 03/2017 -->
								<xsl:when test="/tXML/Message/Order/OrderType = 'MKP'">
									<xsl:value-of select="$PaiementMarketplace"/>
								</xsl:when>
							<!-- -->
								<xsl:otherwise>
									<xsl:value-of select="concat($PaymentMethodCBRdirect,$TransactionDecision)"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="GPE_PERCU">
							<xsl:choose>
								<xsl:when test="/tXML/Message/Order/OrderType = 'R1H' or /tXML/Message/Order/OrderType = 'SFS' ">
									<!--<xsl:value-of select="'Paiement en attente'"/>-->
									<xsl:value-of select="''"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="'N'"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:value-of select="concat('ECPC1_',';',$DateEche,';',$GPE_MODEPAIE,';',$ReferenceField5,';',$CardTypeCBRdirect,';',$RefInterne,';',$ProcessedAmount,';',$posit,';',$NameOnCard,';',$ExpirationDate,';',$NumCard,';',$GPE_PERCU,';')"/>
						<xsl:text>&#13;</xsl:text>
					</xsl:for-each>
					<xsl:for-each select="PaymentTransactionDetails/PaymentTransactionDetail[InvoiceNumber=$InvoiceNbr]">
						<xsl:variable name="TokenId" select="RequestId"/>
						<xsl:variable name="NameOnCard" select="concat(upper-case($NameAsOnCard), ' -- ',$TokenId)"/>
						<xsl:variable name="ProcessedAmount" select="format-number(ProcessedAmount,'0.##')"/>
						<xsl:variable name="TransactionDecision" select="TransactionDecision"/>
						<xsl:variable name="GPE_MODEPAIE">
							<xsl:choose>
								<xsl:when test="OrderType = 'R1H' or OrderType = 'SFS' ">
									<!--Paiement en attente-->
									<xsl:value-of select="concat('W2S',$PaymentMethodCBRdirect,$TransactionDecision)"/>
								</xsl:when>
								<!-- Moyen de paiement Marketplace : THA 03/2017 -->
								<xsl:when test="/tXML/Message/Order/OrderType = 'MKP'">
									<xsl:value-of select="$PaiementMarketplace"/>
								</xsl:when>
								<!-- -->
								<xsl:otherwise>
									<xsl:value-of select="concat($PaymentMethodCBRdirect,$TransactionDecision)"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="GPE_PERCU">
							<xsl:choose>
								<xsl:when test="OrderType = 'R1H' or OrderType = 'SFS' ">
									<!--<xsl:value-of select="'Paiement en attente'"/>-->
									<xsl:value-of select="''"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="'N'"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:value-of select="concat('ECPC1_',';',$DateEche,';',$GPE_MODEPAIE,';',$ReferenceField5,';',concat($CardTypeCBRdirect,$TransactionDecision),';',$RefInterne,';',$ProcessedAmount,';',$posit,';',$NameOnCard,';',$ExpirationDate,';',$NumCard,';',$GPE_PERCU,';')"/>
						<xsl:text>&#13;</xsl:text>
					</xsl:for-each>
				</xsl:for-each>
			</xsl:for-each>
			<!--   *******************************************************************************
            ********** FIN Génération Détail de paiement (par les Invoice) *********
            ******************************************************************************* -->
			<!--   *******************************************************************************
	********** Intégration des annonces de livraison point relais *********
	******************************************************************************* -->
			<xsl:for-each select="Invoices/InvoiceDetail[InvoiceType='Shipment' and Published='false']">
				<xsl:for-each select="InvoiceLines/InvoiceLineDetail">
					<xsl:variable name="Parent" select="ParentOrderLineNbr "/>
					<xsl:if test="exists(../../../../OrderLines/OrderLine[ShippingInfo/ShipVia='CLNC' and LineNumber = $Parent])">
						<xsl:variable name="orderline" select="../../../../OrderLines/OrderLine[ShippingInfo/ShipVia='CLNC' and LineNumber = $Parent][1]"/>
						<xsl:variable name="GP_DATEPIECE">
							<xsl:call-template name="convertDOMDateFormat1">
								<!-- <xsl:with-param name="input" select="../../InvoiceCreationDTTM"/> -->
								<xsl:with-param name="DOMDateTime" select="../../InvoiceCreationDTTM"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="GP_REFINTERNE">
							<xsl:call-template name="convertDOMDateFormat1">
								<xsl:with-param name="DOMDateTime" select="../../InvoiceCreationDTTM"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="GP_REFINTERNE_2" select="concat('S', $OrderNumber)"/>
						<xsl:variable name="GP_REFINTERNE_3" select="concat('IO', ../../InvoiceNbr)"/>
						<xsl:variable name="CODEBARRE" select="ShippedItem"/>
						<xsl:variable name="QTESTOCK" select="InvoicedQuantity"/>
						<xsl:variable name="PRIXVTETOTAL">
							<xsl:choose>
								<xsl:when test="$orderline/PriceInfo/IsPriceOverridden = 'false'">
									<xsl:value-of select="format-number($orderline/PriceInfo/Price * $orderline/Quantity/OrderedQty,'0.##')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="format-number($orderline/PriceInfo/OriginalPrice * $orderline/Quantity/OrderedQty,'0.##')"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="PRIXVTETOTREM" select="LineTotal"/>
						<xsl:variable name="MONTANTTAXE1" select="0"/>
						<xsl:variable name="TYPEREMISE" select="$orderline/DiscountDetails/DiscountDetail/ExtDiscountDetailId"/>
						<xsl:variable name="CDEECOMETAB">
							<xsl:choose>
								<xsl:when test="$orderline/OrderLineStatus = 'Shipped' ">
									<xsl:value-of select="$orderline/ShippingInfo/ShippingAddress[1]/ShipToFax"/>
								</xsl:when>
							</xsl:choose>
						</xsl:variable>
						<xsl:variable name="GP_REFSUIVI">
							<xsl:choose>
								<xsl:when test="../../../../ShipmentDetails/ShipmentDetail/Cartons/Carton/ShipmentNbr = ../../../../Invoices/InvoiceDetail/ShipmentNbr ">
									<xsl:value-of select="../../../../ShipmentDetails/ShipmentDetail/Cartons/Carton[ShipmentNbr = ../../../../Invoices/InvoiceDetail/ShipmentNbr]/TrackingNbr "/>
								</xsl:when>
							</xsl:choose>
						</xsl:variable>
						<xsl:value-of select="concat('CDLC1_',';',$CustomerId,';',$GP_DATEPIECE,';',concat($GP_REFINTERNE, $GP_REFINTERNE_2, $GP_REFINTERNE_3),';',$OrderNumber,';',$CODEBARRE,';',$QTESTOCK,';',$PRIXVTETOTAL,';',$PRIXVTETOTREM,';',$MONTANTTAXE1,';',$ReferenceField5,';',$TYPEREMISE,';',$CDEECOMETAB,';',$GP_REFSUIVI,';')"/>
						<xsl:text>&#13;</xsl:text>
					</xsl:if>
				</xsl:for-each>
			</xsl:for-each>
			<!--   *******************************************************************************
            ********** FIN Intégration des annonces de livraison point relais *********
            ******************************************************************************* -->
		</xsl:for-each>
		<!--  *******************************************************
          ************ Section Template Function **********
          *******************************************************-->
	</xsl:template>
	<!-- This function transforms the date format from '9/7/07 13:13'  OR 9/9/2009 09:00  7/10/2008 10:27 to YYYYMMDD -->
	<xsl:template name="convertDOMDateFormat1">
		<xsl:param name="DOMDateTime"/>
		<xsl:if test="string-length($DOMDateTime) &gt; 15">
			<xsl:variable name="varDate">
				<xsl:value-of select="substring-before($DOMDateTime, ' ')"/>
			</xsl:variable>
			<xsl:variable name="varMonth">
				<xsl:if test="string-length(substring-before($varDate, '/')) &lt; 2">
					<xsl:value-of select="concat('0',substring-before($varDate, '/'))"/>
				</xsl:if>
				<xsl:if test="string-length(substring-before($varDate, '/')) = 2">
					<xsl:value-of select="substring-before($varDate, '/')"/>
				</xsl:if>
			</xsl:variable>
			<xsl:variable name="varDayYear">
				<xsl:value-of select="substring-after($varDate,'/')"/>
			</xsl:variable>
			<xsl:variable name="varDay">
				<xsl:if test="string-length(substring-before($varDayYear, '/')) &lt; 2">
					<xsl:value-of select="concat('0',substring-before($varDayYear, '/') )"/>
				</xsl:if>
				<xsl:if test="string-length(substring-before($varDayYear, '/')) = 2">
					<xsl:value-of select="substring-before($varDayYear, '/')"/>
				</xsl:if>
			</xsl:variable>
			<xsl:variable name="varYear">
				<xsl:choose>
					<xsl:when test="string-length(substring-after($varDayYear, '/')) = 2">
						<xsl:value-of select="concat('20',substring-after($varDayYear, '/'))"/>
					</xsl:when>
					<xsl:when test="string-length(substring-after($varDayYear, '/')) = 4">
						<xsl:value-of select="substring-after($varDayYear, '/')"/>
					</xsl:when>
				</xsl:choose>
			</xsl:variable>
			<xsl:value-of select="concat($varYear,$varMonth,$varDay)"/>
		</xsl:if>
	</xsl:template>
	<xsl:template name="convertDOMDateFormat2">
		<xsl:param name="input"/>
		<xsl:analyze-string select="$input" regex="^(\d+)/(\d+)/(\d+) (.*?)$">
			<xsl:matching-substring>
				<xsl:variable name="yearReg" select="xs:string(regex-group(3))"/>
				<xsl:variable name="year">
					<xsl:choose>
						<xsl:when test="string-length($yearReg) = 2">
							<xsl:value-of select="concat('20', $yearReg)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$yearReg"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="month_Reg" select="xs:string(regex-group(1))"/>
				<xsl:variable name="month">
					<xsl:choose>
						<xsl:when test="string-length($month_Reg) = 1">
							<xsl:value-of select="concat('0', $month_Reg)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$month_Reg"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="dayReg" select="xs:string(regex-group(2))"/>
				<xsl:variable name="day">
					<xsl:choose>
						<xsl:when test="string-length($dayReg) = 1">
							<xsl:value-of select="concat('0', $dayReg)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$dayReg"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="dateFormatted" select="concat($month, '/', $day, '/', $year)"/>
				<xsl:value-of select="$dateFormatted"/>
			</xsl:matching-substring>
			<xsl:non-matching-substring>
				<xsl:value-of select="'01/01/1900'"/>
			</xsl:non-matching-substring>
		</xsl:analyze-string>
	</xsl:template>
	<!-- Fonction de recherche du code barre en fonction du ShipVia -->
	<xsl:template name="UPCSpecial">
		<xsl:param name="ShipVia"/>
		<xsl:choose>
			<xsl:when test="contains($ShipVia,'STND')">
				<!--Livraison standard-->
				<xsl:value-of select="'9990000007325'"/>
			</xsl:when>
			<xsl:when test="contains($ShipVia,'EXPR')">
				<!--Livraison express-->
				<xsl:value-of select="'9990000007226'"/>
			</xsl:when>
			<xsl:when test="contains($ShipVia,'CLNC')">
				<!--Click and collect-->
				<xsl:value-of select="'9990000007219'"/>
			</xsl:when>
			<xsl:when test="contains($ShipVia,'PTRS')">
				<!--Point relais-->
				<xsl:value-of select="'9990000007220'"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="'NoExped'"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<!-- Fonction de recherche du code Appeasement dans CBR-->
	<xsl:template name="AppeasementCode">
		<xsl:param name="Appeas"/>
		<xsl:choose>
			<xsl:when test="$Appeas='Appeasement'">
				<xsl:value-of select="'ZECOM1'"/>
			</xsl:when>
			<xsl:when test="$Appeas='Shipping appeasement'">
				<xsl:value-of select="'ZECOM3'"/>
			</xsl:when>
			<xsl:when test="$Appeas='RMA Fees'">
				<xsl:value-of select="'A remplir'"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$Appeas"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:transform>
