<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions">

	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	
	<xsl:template match="/">
		<soapenv:Envelope 
			xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" 
			xmlns:ns="http://cbrspec.lacoste.com/ConnectV3/1.0">
		
			<soapenv:Header/>
			
			<soapenv:Body>
				
				<!-- Body/CancelOrder -->
				<ns:CancelOrder>
				
					<!-- Body/CancelOrder/ExternalReference -->
					<!-- Value : CONNECT + tXML/Message/Order/OrderNumber -->
					<ns:ExternalReference>
						<xsl:variable name="ExternalReferenceText" select="tXML/Message/Order/OrderNumber" />
						<xsl:variable name="ExternalReferenceValue" select="fn:concat('CONNECT', $ExternalReferenceText)" />
						<xsl:value-of select="$ExternalReferenceValue" />
					</ns:ExternalReference>
					
					<!-- Body/AddCustomerOrder/DataOrder/CreateRequest/Header/IsSplittedOrder -->
					<!-- if tXML/Message/Order/ReferenceFields/ReferenceField7 contains "Split" then "true" else "false -->
					<ns:IsSplittedOrder>
						<xsl:variable name="OrderNumber" select="/tXML/Message/Order/OrderNumber" />
						<xsl:variable name="OrderNumberLastChar" select="fn:upper-case( fn:substring($OrderNumber, fn:string-length($OrderNumber))) " />
						<xsl:choose>
							<xsl:when test="$OrderNumberLastChar = 'A' or $OrderNumberLastChar = 'B'">
								<xsl:value-of select=" 'true' " />
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select=" 'false' " />
							</xsl:otherwise>
						</xsl:choose>
					</ns:IsSplittedOrder>
				</ns:CancelOrder>
			
			</soapenv:Body>
		
		</soapenv:Envelope>
	</xsl:template>

</xsl:stylesheet>
